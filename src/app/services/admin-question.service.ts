import { Injectable } from '@angular/core';
import { server } from "../../assets/config";
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})

export class AdminQuestionService {
  constructor(private http: HttpService) { }

  /**
   * Create Question
   */
  public CreateQuestion(bodyToSend): Promise<Object> {
    return this.http.post(`${server}/api/question/create`, bodyToSend);
  }

  /**
   * Get questions by module
   */
  public GetQuestions(module): Promise<Object> {
    return this.http.get(`${server}/api/question/all/${module}`);
  }

  /**
   * Update question
   */
  public UpdateQuestion(bodyToSend): Promise<Object> {
    return this.http.patch(`${server}/api/question/update`, bodyToSend);
  }

  /**
   * Delete question
   */
  public DeleteQuestion(id): Promise<Object> {
    return this.http.delete(`${server}/api/question/delete/${id}`);
  }
}