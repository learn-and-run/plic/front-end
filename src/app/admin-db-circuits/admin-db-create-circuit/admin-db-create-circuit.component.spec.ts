import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbCreateCircuitComponent } from './admin-db-create-circuit.component';

describe('AdminDbCreateCircuitComponent', () => {
  let component: AdminDbCreateCircuitComponent;
  let fixture: ComponentFixture<AdminDbCreateCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbCreateCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbCreateCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
