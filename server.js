const express = require('express');

const app = express();

app.use(express.static('./dist/WebSite'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'dist/WebSite/'}),
);

app.listen(process.env.PORT || 4200);
