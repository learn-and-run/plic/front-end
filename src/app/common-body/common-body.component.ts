import { Component, OnInit, AfterViewInit, ViewChild, Input, HostListener } from '@angular/core';
import { Globals } from 'src/assets/Globals';
@Component({
  selector: 'app-common-body',
  templateUrl: './common-body.component.html',
  styleUrls: ['./common-body.component.sass']
})
export class CommonBodyComponent implements OnInit {

  constructor(public globals: Globals) { }
  @Input() display: Boolean;
  isMobile = false
  images = ["rpg1.png", "rpg2.png", "rpg3.png"];

  ngOnInit() {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      this.isMobile = true;
    }
  }


  getImage(name: string)
  {
    return '../assets/images/Carrousel/'+ name;
  }
}
