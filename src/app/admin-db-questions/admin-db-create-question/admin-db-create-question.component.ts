import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { adminLevels, adminModules } from 'src/app/app-constant';
import { AdminQuestionService } from 'src/app/services/admin-question.service';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'create-question',
  templateUrl: './admin-db-create-question.component.html',
  styleUrls: ['./admin-db-create-question.component.sass']
})
export class AdminDbCreateQuestionComponent implements OnInit {


  constructor(private questionService: AdminQuestionService, private location: Location, private router: Router, public globals: Globals) { }

  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("SubmitButton") SubmitButton: ElementRef;

  counter = 0;
  clue1 = false;
  clue2 = false;
  clue3 = false;

  isLoading$ = new BehaviorSubject(false);
  canDisplay$ = new BehaviorSubject(true);
  modules = adminModules;
  levels = adminLevels;
  difficulty = [0, 1, 2, 3, 4, 5]

  adminForm  = new FormGroup({
    "name": new FormControl(null, Validators.required),
    "question": new FormControl(null, Validators.required),
    "answer": new FormControl(null, Validators.required),
    "badAnswer": new FormControl(null, Validators.required),
    "level": new FormControl(null, Validators.required),
    "module": new FormControl(null, Validators.required),
    "clue1": new FormControl(""),
    "clue2": new FormControl(""),
    "clue3": new FormControl(""),
    "difficulty": new FormControl(0),
    "explanation": new FormControl("", Validators.required)
  });
  display = false;
  ngOnInit() {
  }

  addClue() {
    if (this.counter < 3) {
      this.counter++;
      if (this.counter === 1)
        this.clue1 = true;
      if (this.counter === 2)
        this.clue2 = true;
      if (this.counter === 3)
       { this.clue3 = true; 
      this.display = true;}

    }
  }
  back() {
    this.location.back();
  }
  removeClue() {
    if (this.counter > 0) {
      if (this.counter === 3)
      {
        this.adminForm.patchValue({
          "clue3": ""
        });
        this.clue3 =false;}
      if (this.counter === 2){
        this.adminForm.patchValue({
          "clue2": ""
        });
        this.clue2 = false;}
      if (this.counter === 1){
        this.adminForm.patchValue({
          "clue1": ""
        });
        this.clue1 = false;
      }
      this.counter--;
      this.display =false;
    }
  }
  failure = false;
  submitForm() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const success = () => {
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      this.location.back();
    };

    const failure = () => {
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      this.failure= true;
      setTimeout(() => this.failure = false, 500);
      clearTimeout();
    };
    const body = {
      "name": this.adminForm.get("name").value,
      "question": this.adminForm.get("question").value,
      "response": this.adminForm.get("answer").value,
      "badAnswer": this.adminForm.get("badAnswer").value,
      "level": this.adminForm.get("level").value,
      "module": this.adminForm.get("module").value,
      "clue1": this.adminForm.get("clue1").value,
      "clue2": this.adminForm.get("clue2").value,
      "clue3": this.adminForm.get("clue3").value,
      "difficulty": this.adminForm.get("difficulty").value,
      "explanation": this.adminForm.get("explanation").value
    };
    this.questionService.CreateQuestion(body).then(success).catch(failure);
  }
  
  fail() {
    this.failure = false;
  }
}
