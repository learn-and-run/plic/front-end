import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObservableLike } from 'rxjs';
import { server } from "../../assets/config";
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})

export class AdminCircuitService {
  constructor(private http: HttpService) { }

  /** 
   * Create circuit
  */
  public CreateCircuit(bodyToSend): Promise<Object> {
    return this.http.post(`${server}/api/circuit/create`, bodyToSend);
  }

    /** 
   * UpdateCircuit
  */
 public UpdateCircuit(bodyToSend): Promise<Object> {
    return this.http.patch(`${server}/api/circuit/update`, bodyToSend);
  }

  public DeleteCircuit(id): Promise<Object> {
    // return new Promise((resolve, reject) => {
    //   this.http.delete(`${server}/api/circuit/delete/${id}`, {
    //     observe: 'response',
    //     withCredentials: true
    //   }).subscribe({
    //     next: data => resolve(data),
    //     error: data => reject(data)
    //   });
    // });
    return this.http.patch(`${server}/api/circuit/delete/${id}`);
  }

  public GetCircuits(module): Promise<Object> {
    return this.http.get(`${server}/api/circuit/all/${module}`);
  }

  public updateRooms(bodyToSend): Promise<Object> {
    return this.http.patch(`${server}/api/circuit/update/rooms`, bodyToSend);
  }

  /**
   * Get Scenes type
   */
  public getScenesTypes(): Promise<Object> {
    return this.http.get(`${server}/api/scene/all`);
  }

  /**
   * Create Scene type
   */
  public createSceneType(bodyToSend): Promise<Object> {
    return this.http.post(`${server}/api/scene/create`, bodyToSend);
  }

  /**
   * Modify Scene type
   */
  public updateSceneType(bodyToSend): Promise<Object> {
    return this.http.patch(`${server}/api/scene/update`, bodyToSend);
  }

  /**
   * Delete Scene type
   */
  public deleteSceneType(uuid): Promise<Object> {
    return this.http.delete(`${server}/api/scene/delete/${uuid}`);
  }
}