import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StatsService } from 'src/app/services/stats.service';

@NgModule({
    imports:
    [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
    ],
    declarations:
    [
    ],
    providers: [StatsService]
})
export class AdminDBUpdateRoomModule {}