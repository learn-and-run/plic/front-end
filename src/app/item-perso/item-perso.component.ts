import { Component, OnInit, Input, ViewChild, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'perso',
  templateUrl: './item-perso.component.html',
  styleUrls: ['./item-perso.component.sass']
})
export class ItemPersoComponent implements OnInit {

  @Input() persoImg: string;
  @Input() description: string;
  @Input() background: string;
  @Input() name: string;
  isMobile = false;
  @ViewChild("Perso", {static: true}) perso: ElementRef<HTMLDivElement>;

  constructor() { }

  ngOnInit() {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
      this.perso.nativeElement.classList.add("persos-mobile");
    } else {
      this.isMobile = false;
      this.perso.nativeElement.classList.add("persos-desktop");
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.perso.nativeElement.classList.contains("persos-mobile")) {
      this.isMobile = false;
      this.perso.nativeElement.classList.replace("persos-mobile", "persos-desktop");
    } else if (window.innerWidth < 1024 && this.perso.nativeElement.classList.contains("persos-desktop")) {
      this.isMobile = true;
      this.perso.nativeElement.classList.replace("persos-desktop", "persos-mobile");
    }
  }
}
