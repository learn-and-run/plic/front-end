import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonBodyComponent } from './common-body/common-body.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { AdminDBQuestionsComponent } from './admin-db-questions/admin-db-questions.component';
import { PersonnagesComponent } from './personnages/personnages.component';
import { SettingsComponent } from './settings/settings.component';
import { AdminDbCircuitsComponent } from './admin-db-circuits/admin-db-circuits.component';
import { AdminDbRoomsComponent } from './admin-db-rooms/admin-db-rooms.component';
import { AdminDbCreateQuestionComponent } from './admin-db-questions/admin-db-create-question/admin-db-create-question.component';
import { AdminDbUpdateQuestionComponent } from './admin-db-questions/admin-db-update-question/admin-db-update-question.component';
import { AdminDbCreateRoomComponent } from './admin-db-rooms/admin-db-create-room/admin-db-create-room.component';
import { AdminDbUpdateRoomComponent } from './admin-db-rooms/admin-db-update-room/admin-db-update-room.component';
import { AdminDbCreateCircuitComponent } from './admin-db-circuits/admin-db-create-circuit/admin-db-create-circuit.component';
import { AdminDbUpdateCircuitComponent } from './admin-db-circuits/admin-db-update-circuit/admin-db-update-circuit.component';
import { ChildComponent } from './child/child.component';
import { FriendsComponent } from './friends/friends.component';
import { ParentComponent } from './parent/parent.component';
import { ValidateEmailComponent } from './validate-email/validate-email.component';
import { RankingComponent } from './ranking/ranking.component';
import { AuthGuardService } from './auth-guard.service';
import { UserGuardService } from './user-guard.service';
import { AdminGuardService } from './admin-guard.service';
import { AdminDbObjectsComponent } from './admin-db-objects/admin-db-objects.component';
import { AdminDbRoomTypeComponent } from './admin-db-room-type/admin-db-room-type.component';
import { AdminDbScenesTypeComponent } from './admin-db-scenes-type/admin-db-scenes-type.component';


const routes: Routes = [
  {
    path: "", redirectTo: "/accueil", pathMatch:"full"
  },
  { path: "accueil", component: CommonBodyComponent },
  { path: "creation-compte", component: CreateAccountComponent },
  { path: "mot-de-passe/:id", component: ForgotPasswordComponent},
  { path: "statistiques", component: StatisticsComponent, canActivate : [AuthGuardService, UserGuardService]},
  { path: "statistiques/:pseudo", component: StatisticsComponent, canActivate : [AuthGuardService, UserGuardService]},
  { path: "activer-compte/:id", component: ActivateAccountComponent},
  {
    path: "validation-email/:id", component: ValidateEmailComponent
  },
  { path: "non-trouvé", component: NotFoundComponent},
  { 
    path: "admin/questions", 
    component: AdminDBQuestionsComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/questions/création",
    component: AdminDbCreateQuestionComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/questions/modification",
    component: AdminDbUpdateQuestionComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  { path: "admin/circuits", component: AdminDbCircuitsComponent,
  canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/circuits/création", component: AdminDbCreateCircuitComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/circuits/modification", component: AdminDbUpdateCircuitComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  { path: "admin/salles", component: AdminDbRoomsComponent,
  canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/salles/création", component: AdminDbCreateRoomComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/salles/modification", component: AdminDbUpdateRoomComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  { path: "admin/objets", component: AdminDbObjectsComponent,
  canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/types-salles", component: AdminDbRoomTypeComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  {
    path: "admin/types-scènes", component: AdminDbScenesTypeComponent,
    canActivate: [AuthGuardService, AdminGuardService]
  },
  { path: "personnages", component: PersonnagesComponent},
  { path: 'paramètres', component: SettingsComponent, canActivate : [AuthGuardService]},
  {
    path: "enfants", component: ChildComponent, canActivate : [AuthGuardService, UserGuardService]
  },
  {
    path: "parents", component: ParentComponent, canActivate : [AuthGuardService, UserGuardService]
  },
  {
    path: "amis", component: FriendsComponent, canActivate : [AuthGuardService, UserGuardService]
  },
  {
    path: "classement", component: RankingComponent, canActivate : [AuthGuardService, UserGuardService]
  },
  { path: '**', redirectTo: "/non-trouvé"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
