import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.sass']
})
export class NotFoundComponent implements OnInit {

  constructor(public globals: Globals) { }

  ngOnInit() {
  }

}
