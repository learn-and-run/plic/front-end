import { Component, OnInit, ElementRef, ViewChild, HostListener} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AdminQuestionService } from '../services/admin-question.service';
import { adminModules } from '../app-constant';
import { runInThisContext } from 'vm';
import { element } from 'protractor';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-admin-db-questions',
  templateUrl: './admin-db-questions.component.html',
  styleUrls: ['./admin-db-questions.component.sass']
})
export class AdminDBQuestionsComponent implements OnInit {

  listQuestions = [];
  isMobileDisplay = false;
  listQuestionsDisplayed = this.listQuestions.sort();
  @ViewChild('Hola', {static: false}) hola: ElementRef<HTMLDivElement>;
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  currentModule = 0;
  textColor = "#b63a6b";
  borderColor = "#b63a6b";
  testModules = [
    {
      item: "Tous",
      itemValue: "TOUS",
      textColor: "#b63a6b",
      current: "#b63a6b"
    },
    {
      item: "Mathématiques",
      itemValue: "MATHS",
      textColor: "#51c47f",
      current: "#dbdbdb"
    },
    {
      item: "Français",
      itemValue: "FRANCAIS",
      textColor: "#1c7cac",
      current: "#dbdbdb",
    },
    {
      item: "Physique",
      itemValue: "PHYSIQUE",
      textColor: "#d37f50",
      current: "#dbdbdb"

    },
    {
      item: "Chimie",
      itemValue: "CHIMIE",
      textColor: "#db983d",
      current: "#dbdbdb"

    },
    {
      item: "Anglais",
      itemValue: "ANGLAIS",
      textColor: "#cd444b",
      current: "#dbdbdb"

    },
    {
      item: "Histoire",
      itemValue: "HISTOIRE",
      textColor: "#ce80b2",
      current: "#dbdbdb"

    },
    {
      item: "Géographie",
      itemValue: "GEOGRAPHIE",
      textColor: "#a5619e",
      current: "#dbdbdb"
    }
  ];
  modules = adminModules;
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  constructor(private questionsService: AdminQuestionService, public globals: Globals) {
    this.getQuestions();
  }
  isMobile = false;
  currentIndex =0;
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
  }

  getQuestions() {
    this.questionsService.GetQuestions("TOUS").then(response => {
      response["body"]["questions"].forEach(question => {
        this.listQuestions.push(
          {
            question,
            "confirm": false,
            "cancel": false,
            "error": false,
            "isDisplayMobile": false
          });
      });
    }).then(() => {
      this.loading = false;
      this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);});
  }
  currentColor = "#b63a6b";
  change(element, id) {
    if (this.currentModule !== id) {
      this.testModules[this.currentModule].current = "#dbdbdb";
      this.currentColor = this.testModules[id].textColor;
      this.currentModule = id;
      this.testModules[id].current = this.testModules[id].textColor;
      this.refresh();
    }
  }

  refresh() {
      const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
      // check if the tabs clicked is TOUS
      if (this.testModules[this.currentModule].itemValue === "TOUS") {
        // Simple search
        this.listQuestionsDisplayed= this.listQuestions.filter(question => {
          const questionLowerCase = question.question.name.toLowerCase();
          return questionLowerCase.includes(wordToLowerCase);
        });
      } else {
        // filtering question by the module clicked and the search word
        this.listQuestionsDisplayed = this.listQuestions.filter(question => {
          if (question.question.module === this.testModules[this.currentModule].itemValue) {
            const questionLowerCase = question.question.name.toLowerCase();
            return questionLowerCase.includes(wordToLowerCase);
          }
        });
      }
  }

  goModify(question) {
    localStorage.setItem("Question", JSON.stringify(question.question));
  }

  ask = false;
  askDelete(id)
  {
    this.currentIndex = id;
    this.ask = true;
    if (this.globals.isMobile) {
      this.listQuestionsDisplayed[id].isDisplayMobile = true;
    }
    this.listQuestionsDisplayed[id].confirm =true;
    this.listQuestionsDisplayed[id].error = false;
    this.listQuestionsDisplayed[id].cancel = true;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.ask)
        this.listQuestionsDisplayed[this.currentIndex].isDisplayMobile = false;
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.ask)
        this.listQuestionsDisplayed[this.currentIndex].isDisplayMobile = true;
      this.isMobile = true;
    }
  }
  delete(id) {
    this.ask = false;
    this.listQuestionsDisplayed[id].isDisplayMobile = false;
    const questionToRemvove = this.listQuestionsDisplayed[id];
    this.questionsService.DeleteQuestion(questionToRemvove.question.id).then(() => {
      this.listQuestions = this.listQuestions.filter(question => question.question.id !== questionToRemvove.question.id);
      this.refresh();
    }).catch(() => {
      this.listQuestionsDisplayed[id].error = true;
      this.listQuestionsDisplayed[id].confirm = false;
      this.listQuestionsDisplayed[id].isDisplayMobile = true;
      this.listQuestionsDisplayed[id].cancel = false;
    });
  }

  cancel(id) {
    this.ask = false;
    this.listQuestionsDisplayed[id].isDisplayMobile = false;
    this.listQuestionsDisplayed[id].confirm = false;
    this.listQuestionsDisplayed[id].cancel = false;
  }

  display = false;
  test() {
    this.display = !this.display;
    if (this.display) {
      this.hola.nativeElement.classList.add('is-active');
    } else {
      this.hola.nativeElement.classList.remove('is-active');
    }
  }

  fail() {
    this.error$.next(false);
  }
}
