import {Component, HostListener} from '@angular/core';
import {Globals} from '../assets/Globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'WebSite';

  constructor(public globals: Globals) {
    globals.isMobile = window.innerWidth < 1024;
  }

  @HostListener('window:resize')
  onResize(): void {
    if (window.innerWidth >= 1024 && this.globals.isMobile) {
      this.globals.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.globals.isMobile) {
      this.globals.isMobile = true;
    }
  }
}
