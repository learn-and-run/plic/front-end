import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbUpdateCircuitComponent } from './admin-db-update-circuit.component';

describe('AdminDbUpdateCircuitComponent', () => {
  let component: AdminDbUpdateCircuitComponent;
  let fixture: ComponentFixture<AdminDbUpdateCircuitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbUpdateCircuitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbUpdateCircuitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
