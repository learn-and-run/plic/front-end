import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../services/user.service';
import { server } from "../../assets/config";
import {Globals} from '../../assets/Globals';

@Component({
  selector: 'app-personnages',
  templateUrl: './personnages.component.html',
  styleUrls: ['./personnages.component.sass']
})
export class PersonnagesComponent implements OnInit {

  constructor(private userService: UserService, public globals: Globals) { }
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;

  persos = [];
  displayPerso = [];
  ngOnInit() {
    this.userService.getCharacters().then(response => {
      response["body"]["characters"].forEach(character => {
        this.persos.push({
          "name": character.name,
          "img": `${server}` + character.smallImage,
          "description": character.description,
          "background": character.background
        });
        this.displayPerso.push({
          "name": character.name,
          "img": `${server}` + character.smallImage,
          "description": character.description,
          "background": character.background
        });
      });
    });
  }

  refresh() {
    const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
    this.displayPerso = this.persos.filter(perso => {
      console.log(perso);
      const persoLowerCase = perso.name.toLowerCase();
      return persoLowerCase.includes(wordToLowerCase);
    });
  }
}
