import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../services/user.service';
import { TextIconComponent } from '../text-icon/text-icon.component';

@NgModule({
    imports:
    [
        FormsModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [
    ],
    declarations:
    [
        TextIconComponent
    ],
    providers: [UserService]
})
export class HeaderModule {}