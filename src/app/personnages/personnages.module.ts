import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemPersoComponent } from '../item-perso/item-perso.component';
import { TextIconComponent } from '../text-icon/text-icon.component';

@NgModule({
    imports:
    [
        FormsModule,
    ],
    exports: [
    ],
    declarations:
    [
        TextIconComponent,
        ItemPersoComponent
    ]
})
export class HeaderModule {}