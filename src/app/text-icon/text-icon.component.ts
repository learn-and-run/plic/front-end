import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'TextIcon',
  templateUrl: './text-icon.component.html',
  styleUrls: ['./text-icon.component.sass']
})
export class TextIconComponent implements OnInit {

  @Input() styleClass: string;
  @Input() styleClassImg: string;
  @Input() comment: string;
  @Input() name: string;
  @Input() text: string;
  
  constructor() { }

  ngOnInit() {
  }

}
