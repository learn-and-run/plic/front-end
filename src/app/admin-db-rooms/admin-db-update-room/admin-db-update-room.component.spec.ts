import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbUpdateRoomComponent } from './admin-db-update-room.component';

describe('AdminDbUpdateRoomComponent', () => {
  let component: AdminDbUpdateRoomComponent;
  let fixture: ComponentFixture<AdminDbUpdateRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbUpdateRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbUpdateRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
