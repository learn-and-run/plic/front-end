import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemPersoComponent } from './item-perso.component';

describe('ItemPersoComponent', () => {
  let component: ItemPersoComponent;
  let fixture: ComponentFixture<ItemPersoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemPersoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemPersoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
