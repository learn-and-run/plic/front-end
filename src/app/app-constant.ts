export const modules = [
    {
      name: "général.png",
      text: "Général",
      color: "#292f33",
      colorD: "#606B73",
      opacity: 1,
      db: "GENERAL",
      uncheck: "round_T.png",
      check: "roundCheck_T.png",
      checkW: "roundCheck_TW.png",
      scoreIcon: "medaille_T.png",
      timeIcon: "time_T.png"
    },
    {
      name: "maths.png",
      text: "Mathématiques",
      color: "#51c47f",
      colorD: "#369E60",
      opacity: 0.5,
      db: "MATHS",
      uncheck: "round_M.png",
      check: "roundCheck_M.png",
      checkW: "roundCheck_MW.png",
      scoreIcon: "medaille_M.png",
      timeIcon: "time_M.png"
    }, {
      name: "francais.png",
      text: "Français",
      color: "#1c7cac",
      colorD: "#135170",
      opacity: 0.5,
      db: "FRANCAIS",
      uncheck: "round_F.png",
      check: "roundCheck_F.png",
      checkW: "roundCheck_FW.png",
      scoreIcon: "medaille_F.png",
      timeIcon:"time_F.png"
    },
   {
      name: "chimie.png",
      text: "Chimie",
      color: "#db983d",
      colorD: "#AC6B12",
      opacity: 0.5,
      db: "CHIMIE",
      uncheck: "round_C.png",
      check: "roundCheck_C.png",
      checkW: "roundCheck_CW.png",
      scoreIcon: "medaille_C.png",
      timeIcon: "time_C.png"
    },
    {
      name: "physique.png",
      text: "Physique",
      color: "#d37f50",
      colorD: "#AB6842",
      opacity: 0.5,
      db: "PHYSIQUE",
      uncheck: "round_P.png",
      check: "roundCheck_P.png",
      checkW: "roundCheck_PW.png",
      scoreIcon: "medaille_P.png",
      timeIcon: "time_P.png"

    },
    {
      name: "anglais.png",
      text: "Anglais",
      color: "#cd444b",
      colorD: "#AC393F",
      opacity: 0.5,
      db: "ANGLAIS",
      uncheck: "round_A.png",
      check: "roundCheck_A.png",
      checkW: "roundCheck_AW.png",
      scoreIcon: "medaille_A.png",
      timeIcon: "time_A.png"
    },
    {
      name: "géo.png",
      text: "Géographie",
      color: "#a5619e",
      colorD: "#72456D",
      opacity: 0.5,
      db: "GEOGRAPHIE",
      uncheck: "round_G.png",
      check: "roundCheck_G.png",
      checkW: "roundCheck_GW.png",
      scoreIcon: "medaille_G.png",
      timeIcon: "time_G.png"
    },
    {
      name: "histoire.png",
      text: "Histoire",
      color: "#ce80b2",
      colorD: "#8B5D7B",
      opacity: 0.5,
      db: "HISTOIRE",
      uncheck: "round_H.png",
      check: "roundCheck_H.png",
      checkW: "roundCheck_HW.png",
      scoreIcon: "medaille_H.png",
      timeIcon: "time_H.png"
    }, 
  ];

export const classItems = [{
    value: "SIX",
    text: 6,
    activeColor: "#ffffff",
    color: "rgb(191,46,140)"
  },
  {
    value: "FIVE",
    text: 5,
    activeColor: "#ffffff",
    color: "rgb(255,203,6)"
  },
  {
    value: "FOUR",
    text: 4,
    activeColor: "#ffffff",
    color: "rgb(106,40,130)"
  },
  {
    value: "THREE",
    text: 3,
    activeColor: "#ffffff",
    color: "rgb(251,117,18)"
  }
];

export const adminModules = [
  {
    text: "Mathématiques",
    value: "MATHS"
  },
  {
    text: "Français",
    value: "FRANCAIS"
  },
  {
    text: "Anglais",
    value: "ANGLAIS"
  },
  {
    text: "Histoire",
    value: "HISTOIRE"
  },
  {
    text: "Géographie",
    value: "GEOGRAPHIE"
  },
  {
    text: "Physique",
    value: "PHYSIQUE"
  },
  {
    text: "Chimie",
    value: "CHIMIE"
  }
];

export const adminLevels = [
  {
    text: "Sixième",
    value: "SIXIEME",
    intValue: "SIX"
  },
  {
    text: "Cinquième",
    value: "CINQUIEME",
    intValue: "FIVE"
  },
  {
    text: "Quatrième",
    value: "QUATRIEME",
    intValue: "FOUR"
  },
  {
    text: "Troisième",
    value: "TROISIEME",
    intValue: "THREE"
  }
];

export const   imgs = [{
  "matiere": "MATHS",
  "imgMatiere": "mathsC.png",
  "imgUpdate": "maths.png",
  "imgQuestion": "qM.png",
  "imgTime": "timeM.png",
  "imgDelete": "deleteM.png"
},
{
  "matiere": "FRANCAIS",
  "imgMatiere": "francaisC.png",
  "imgUpdate": "francais.png",
  "imgQuestion": "qF.png",
  "imgTime": "timeF.png",
  "imgDelete": "deleteF.png"
},
{
  "matiere": "ANGLAIS",
  "imgMatiere": "anglaisC.png",
  "imgUpdate": "anglais.png",
  "imgQuestion": "qA.png",
  "imgTime": "timeA.png",
  "imgDelete": "deleteA.png"
},
{
  "matiere": "GENERAL",
  "imgMatiere": "généralC.png",
  "imgUpdate": "général.png",
  "imgQuestion": "qT.png",
  "imgTime": "timeT.png",
  "imgDelete": "deleteT.png"
},
{
  "matiere": "PHYSIQUE",
  "imgMatiere": "physiqueC.png",
  "imgUpdate": "physique.png",
  "imgQuestion": "qP.png",
  "imgTime": "timeP.png",
  "imgDelete": "deleteP.png"
},
{
  "matiere": "CHIMIE",
  "imgMatiere": "chimieC.png",
  "imgUpdate": "chimie.png",
  "imgQuestion": "qC.png",
  "imgTime": "timeC.png",
  "imgDelete": "deleteC.png"
},  {
  "matiere": "GEOGRAPHIE",
  "imgMatiere": "géoC.png",
  "imgUpdate": "géo.png",
  "imgQuestion": "qG.png",
  "imgTime": "timeG.png",
  "imgDelete": "deleteG.png"
},
{
  "matiere": "HISTOIRE",
  "imgMatiere": "histoireC.png",
  "imgUpdate": "histoire.png",
  "imgQuestion": "qH.png",
  "imgTime": "timeH.png",
  "imgDelete": "deleteH.png"
}
];

export const AdminItems = [
  {
    "text": "Questions",
    "path": "/admin/questions",
    "img": "questions.png"
  },
  {
    "text": "Salles",
    "path": "/admin/salles",
    "img": "pieces.png"
  },
  {
    "text": "Circuits",
    "path": "/admin/circuits",
    "img": "circuits.png"
  },
  {
    "text": "Objets",
    "path": "/admin/objets",
    "img": "objets.png"
  }
  , 
  {
    "text": "Types Salles",
    "path": "/admin/types-salles",
    "img": "type.png"
  },
  {
    "text": "Types Scènes",
    "path": "/admin/types-scènes",
    "img": "scene.png"
  }
];