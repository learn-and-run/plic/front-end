import { Component, ElementRef, HostListener, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { type } from 'os';
import { BehaviorSubject } from 'rxjs';
import { Globals } from 'src/assets/Globals';
import { AdminCircuitService } from '../services/admin-circuit.service';
import { AdminRoomService } from '../services/admin-room.service';

@Component({
  selector: 'app-admin-db-scenes-type',
  templateUrl: './admin-db-scenes-type.component.html',
  styleUrls: ['./admin-db-scenes-type.component.sass']
})
export class AdminDbScenesTypeComponent implements OnInit {
  @ViewChild("Delete", {static: false}) deleteModal: ElementRef<HTMLDivElement>;
  @ViewChild("Modify", {static: false}) modifyModal: ElementRef<HTMLDivElement>;
  @ViewChild("Add", {static: false}) addModal: ElementRef<HTMLDivElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  @ViewChild("SubmitButton") modifyButton: ElementRef<HTMLButtonElement>;
  @ViewChild("SubmitButton2") addButton: ElementRef<HTMLButtonElement>;
  listSceneTypes = [];
  listSceneTypesDisplayed = this.listSceneTypes;
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  failure = false;
  createForm = new FormGroup({
    "name": new FormControl(null, Validators.required)
  });

  modifyForm = new FormGroup({
    "uuid": new FormControl(null),
    "name": new FormControl(null, Validators.required)
  });
  constructor(public globals: Globals, private circuitService: AdminCircuitService) { }

  isMobile = false;
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
    this.getTypes();
  }

  fail() {
    this.error$.next(false);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.ask)
        this.deleteModal.nativeElement.classList.remove("is-active");
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.ask)
        this.deleteModal.nativeElement.classList.add("is-active");      
      this.isMobile = true;
    }
  }

  getTypes() {
    this.circuitService.getScenesTypes().then(response => {
      response["body"]["scenes"].forEach(elt => {this.listSceneTypes.push(elt)});
      this.loading = false;
      this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);
    });
  }

  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
      this.oldIndexA = i;
    } else {
      this.oldIndexA = -1;
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }
  text = "";
  openModal() {
    this.addModal.nativeElement.classList.add("is-active");
  }
  goModify(elt) {
    this.text = elt.name;
    this.modifyForm.patchValue({
      "uuid": elt.uuid,
      "name": elt.name
    });
    this.modifyModal.nativeElement.classList.add("is-active");
  }
  refresh() {
  const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
     this.listSceneTypesDisplayed=  this.listSceneTypes.filter(elt => {
      const name = elt.name.toLowerCase();
      return name.includes(wordToLowerCase);
    });
  }

  ask = false;
  currentIndex = 0;
  askDelete(id)
  {
    this.ask = true;
    this.currentIndex = id;
    if (this.globals.isMobile)
      this.deleteModal.nativeElement.classList.add("is-active");
    this.text = this.listSceneTypesDisplayed[id].name;
    this.listSceneTypesDisplayed[id].confirm = true;
    this.listSceneTypesDisplayed[id].error = false;
    this.listSceneTypesDisplayed[id].cancel = true;
  }

  close(modal) {
    if (modal === "delete") {
      this.deleteModal.nativeElement.classList.remove("is-active");
       this.listSceneTypesDisplayed[this.currentIndex].confirm = false;
       this.listSceneTypesDisplayed[this.currentIndex].cancel = false;
    } else if (modal === "add") {
      this.addModal.nativeElement.classList.remove("is-active");
      this.createForm.patchValue({
        "name": null
      });
    } else {
      this.modifyModal.nativeElement.classList.remove("is-active");
      this.modifyForm.patchValue({
        "uuid": null,
        "name": null
      });
    }
  }

  delete(id) {
    this.ask = false;
    const typeToRemove = this.listSceneTypesDisplayed[id];
    this.circuitService.deleteSceneType(typeToRemove.uuid).then(() => {
      this.listSceneTypes = this.listSceneTypes.filter(name => name.uuid !== typeToRemove.uuid);
      this.deleteModal.nativeElement.classList.remove("is-active");
      this.refresh();
    }).catch(() => {
      this.listSceneTypesDisplayed[id].error = true;
      this.deleteModal.nativeElement.classList.remove("is-active");
      this.listSceneTypesDisplayed[id].confirm = false;
      this.listSceneTypesDisplayed[id].cancel = false;
    });
  }

  cancel(id) {
    this.ask = false;
    if (this.globals.isMobile) {
      this.deleteModal.nativeElement.classList.remove("is-active");
    }
    this.listSceneTypesDisplayed[id].confirm = false;
    this.listSceneTypesDisplayed[id].cancel = false;
  }
  private regex = '^([A-Z]+[a-z]+_*)+$';
  isValid(modal) {
    if (modal === "create") {
      const name = this.createForm.get("name").value;
      if (name !== null)
        return name.search(this.regex) !== -1 ? true : false;
      return false;
    } else {
      const name = this.modifyForm.get("name").value;
      if (name !== null)
        return name.search(this.regex) !== -1 ? true : false;
      return false;
    }
  }

  submitAddForm() {
    this.addButton.nativeElement.classList.add("is-loading");
      const body = {
        "name": this.createForm.get("name").value
      };
      this.circuitService.createSceneType(body).then(response => {
        this.listSceneTypes.push(response["body"]);
        this.listSceneTypesDisplayed = this.listSceneTypes;
        this.createForm.patchValue({
          "name": null
        });
        this.addModal.nativeElement.classList.remove("is-active");
        this.addButton.nativeElement.classList.remove("is-loading");
      }).catch(() => {this.failure = true; 
        this.addButton.nativeElement.classList.remove("is-loading");
        setTimeout(() => this.failure = false, 500);
        clearTimeout();
      });
  }

  submitUpdateForm() {
    this.modifyButton.nativeElement.classList.add("is-loading");
    const body = {
      "uuid": this.modifyForm.get("uuid").value,
      "name": this.modifyForm.get("name").value
    };
    this.circuitService.updateSceneType(body).then(response => {
      const index = this.listSceneTypes.findIndex(elt => elt.uuid === response["body"].uuid);
      this.listSceneTypes[index].name = response["body"].name;
      this.modifyForm.patchValue({
        "uuid": null,
        "name": null
      });
      this.modifyModal.nativeElement.classList.remove("is-active");
      this.modifyButton.nativeElement.classList.remove("is-loading");
    }).catch(() => {this.failure = true; 
      this.modifyButton.nativeElement.classList.remove("is-loading");
      setTimeout(() => this.failure = false, 500);
      clearTimeout();
    });
  }
}
