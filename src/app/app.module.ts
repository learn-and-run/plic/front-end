import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { AdminDbCircuitsComponent } from './admin-db-circuits/admin-db-circuits.component';
import { AdminDbCreateCircuitComponent } from './admin-db-circuits/admin-db-create-circuit/admin-db-create-circuit.component';
import { AdminDbUpdateCircuitComponent } from './admin-db-circuits/admin-db-update-circuit/admin-db-update-circuit.component';
import { AdminDbCreateQuestionComponent } from './admin-db-questions/admin-db-create-question/admin-db-create-question.component';
import { AdminDBQuestionsComponent } from './admin-db-questions/admin-db-questions.component';
import { AdminDbUpdateQuestionComponent } from './admin-db-questions/admin-db-update-question/admin-db-update-question.component';
import { AdminDbCreateRoomComponent } from './admin-db-rooms/admin-db-create-room/admin-db-create-room.component';
import { AdminDbRoomsComponent } from './admin-db-rooms/admin-db-rooms.component';
import { AdminDbUpdateRoomComponent } from './admin-db-rooms/admin-db-update-room/admin-db-update-room.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonBodyComponent } from './common-body/common-body.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { DifferentAComponent } from './different-a/different-a.component';
import { FooterComponent } from './footer/footer.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HeaderComponent } from './header/header.component';
import { ItemPersoComponent } from './item-perso/item-perso.component';
import { LoaderComponent } from './loader/loader.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PersonnagesComponent } from './personnages/personnages.component';
import { SettingsComponent } from './settings/settings.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { TabItemComponent } from './tab-item/tab-item.component';
import { TextIconComponent } from './text-icon/text-icon.component';
import { ChildComponent } from './child/child.component';
import { FriendsComponent } from './friends/friends.component';
import { ParentComponent } from './parent/parent.component';
import { ValidateEmailComponent } from './validate-email/validate-email.component';
import { PasswordComponent } from './password/password.component';
import { RankingComponent } from './ranking/ranking.component';
import {Globals} from '../assets/Globals';
import { TitleComponent } from './title/title.component';
import { DropItemComponent } from './drop-item/drop-item.component';
import { AuthGuardService } from './auth-guard.service';
import { UserGuardService } from './user-guard.service';
import { AdminGuardService } from './admin-guard.service';
import { AdminDbObjectsComponent } from './admin-db-objects/admin-db-objects.component';
import { AdminDbRoomTypeComponent } from './admin-db-room-type/admin-db-room-type.component';
import { AdminDbScenesTypeComponent } from './admin-db-scenes-type/admin-db-scenes-type.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TextIconComponent,
    CommonBodyComponent,
    CreateAccountComponent,
    ForgotPasswordComponent,
    StatisticsComponent,
    TabItemComponent,
    ActivateAccountComponent,
    NotFoundComponent,
    LoaderComponent,
    AdminDBQuestionsComponent,
    PersonnagesComponent,
    ItemPersoComponent,
    SettingsComponent,
    AdminDbRoomsComponent,
    AdminDbCircuitsComponent,
    AdminDbCreateRoomComponent,
    AdminDbUpdateRoomComponent,
    AdminDbCreateCircuitComponent,
    AdminDbUpdateCircuitComponent,
    DifferentAComponent,
    AdminDbCreateQuestionComponent,
    AdminDbUpdateQuestionComponent,
    ChildComponent,
    FriendsComponent,
    ParentComponent,
    ValidateEmailComponent,
    PasswordComponent,
    RankingComponent,
    TitleComponent,
    DropItemComponent,
    AdminDbObjectsComponent,
    AdminDbRoomTypeComponent,
    AdminDbScenesTypeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    Globals,
    AuthGuardService,
    UserGuardService,
    AdminGuardService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
