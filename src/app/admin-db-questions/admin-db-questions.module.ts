import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AdminDBQuestionsComponent } from './admin-db-questions.component';
import { AdminDbCreateQuestionComponent } from './admin-db-create-question/admin-db-create-question.component';
import { AdminDbUpdateQuestionComponent } from './admin-db-update-question/admin-db-update-question.component';
import { AdminQuestionService } from '../services/admin-question.service';

@NgModule({
    imports:
    [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
       AdminDBQuestionsComponent
    ],
    declarations:
    [
        AdminDBQuestionsComponent,
        AdminDbCreateQuestionComponent,
        AdminDbUpdateQuestionComponent,
    ],
    providers: [AdminQuestionService]
})
export class AdminDBQuestionsModule {}