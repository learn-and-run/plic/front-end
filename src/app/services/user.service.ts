import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { server } from "../../assets/config";
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})

export class UserService {
  constructor(private httpService: HttpService, private http: HttpClient) { }

  urlEncodedHeader = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

  /**
   * Login the user on the backend.
   * @param emailOrPseudo The pseudo or the email useful to login
   * @param password The password of the user
   * @param rememberMe Boolean that indicate if we have to remember the user
   */
  public login(emailOrPseudo: string, password: string, rememberMe: Boolean): Promise<Object> { 
    return new Promise((resolve, reject) => {

      let body = new URLSearchParams();
      body.set('username', emailOrPseudo);
      body.set('password', password);
      body.set('remember-me', rememberMe.toString());

      this.http.post(`${server}/api/login`, body.toString(), {
        observe: 'response',
        headers: this.urlEncodedHeader,
        withCredentials: true
      }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)        
      });
    });
  }

  /**
   * Determine if the current user is authenticated or not.
   */
  public isAuthenticated(): Promise<object> {
    return this.httpService.get(`${server}/api/user/is_auth`);
  }

  /**
   * Determine if the current user is fully authenticated or not.
   */
  public isFullyAuthenticated(): Promise<Object> {
    return this.httpService.get(`${server}/api/user/is_fully_auth`);
  }

  /**
   * Get informations about the current user.
   */
  public getUserInfo(): Promise<Object> {
    return this.httpService.get(`${server}/api/user/info`);
  }

  /**
   * Create a user in database
   * @param user An object that countain element, please check the api before pass it elements.
   * @param userType Specified the type of user to create (can be 'professor', 'parent' or 'student')
   */
  public createUser(user: Object, userType: String) : Promise<Object> {
    return this.httpService.post(`${server}/api/user`, userType);
  }

  /**
   * Logout the user from the backend.
   */
  public logout() : Promise<Object> {
    return this.httpService.delete(`${server}/api/logout`);
  }

  /**
   * Send an email with a link to reset the password.
   * @param email The email on which the link have to be sent.
   */
  public askResetPassword(email: String): Promise<Object> {
    return this.httpService.post(`${server}/api/user/reset`, {"email": email});
  }

  /**
   * Reset the password associated with the specific email address by the link
   * @param id The id that correspond to the current link
   * @param password The new password
   */
  public resetPassword(id: String, password: String): Promise<Object> {
    return this.httpService.patch(`${server}/api/user/reset/${id}`, {"password": password});
  }

  /**
   * Send an email to the current user with a link to change the email to prevent security issues.
   */
  public sendUpdateEmail(): Promise<Object> {
    return this.httpService.post(`${server}/api/user/update/email`);
  }

  /**
   * Update the email of the user with the specific link id to the new email.
   * @param id The id that correspond to the current link
   * @param email The new email
   */
  public updateEmail(id: String, email: String): Promise<Object> {
    return this.httpService.post(`${server}/api/user/update/email/${id}`, { 'email': email });
  }

  /**
   * Update the password of the current user.
   * @param password The new password
   */
  public updatePassword(password: String): Promise<Object> {
    return this.httpService.patch(`${server}/api/user/update/password`, { "password": password});
  }

  /**
   * Delete the current account from the database. This action is irreversible.
   */
  public deleteAccount(): Promise<Object> {
    return this.httpService.delete(`${server}/api/user/delete`);
  }

  public updateAccount(userType, body): Promise<Object> {
    return this.httpService.patch(`${server}/api/user/update/${userType}`, body);
  }

  public getRelation(type): Promise<Object> {
    return this.httpService.get(`${server}/api/user/relation/all?userType=${type}`);
  }

  public sendInvitation(body): Promise<Object> {
    return this.httpService.post(`${server}/api/user/relation/add`, body);
  }

  public pendingInvitation(): Promise<Object> {
    return this.httpService.get(`${server}/api/user/relation/pending`);
  }

  public acceptInvitation(id: Object): Promise<Object> {
    return this.httpService.post(`${server}/api/user/relation/validate`, id);
  }

  public removeRelation(id): Promise<Object> {
    return this.httpService.delete(`${server}/api/user/relation/delete/${id}`);
  }

  public getCharacters(): Promise<Object> {
    return this.httpService.get(`${server}/api/character/all`);
  }

  public validateEmail(id): Promise<Object> {
    return this.httpService.get(`${server}/api/user/update/email/${id}`);
  }

    /**
   * Determine if the current user is admin or not.
   */
  public isAdmin(): Promise<Object> {
    return this.httpService.get(`${server}/api/user/is_admin`);
  }

  public declineInvitation(invitationId: string): Promise<Object>
  {
    return this.httpService.delete(`${server}/api/user/relation/decline/${invitationId}`);
  }

  public activateAccount(accountId: string): Promise<Object>{
    return this.httpService.get(`${server}/api/user/activate/${accountId}`);
  }
}