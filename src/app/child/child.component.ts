import { Component, OnInit, ViewChild, ElementRef, HostListener, ViewChildren } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.sass']
})
export class ChildComponent implements OnInit {

  constructor(private userService: UserService) { }
  listChildren = [];
  hasChildren = false;
  hasNoChildren = false;
  isMobile = false;
  @ViewChild("Add") add: ElementRef<HTMLElement>;
  @ViewChild("Success") success: ElementRef<HTMLElement>;
  @ViewChild("Failure") failure: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton") submitButton: ElementRef<HTMLElement>;
  @ViewChild("Number") number: ElementRef<HTMLSpanElement>;
  @ViewChild("SubmitButton2") submitButton2: ElementRef<HTMLElement>;
  @ViewChild("Bell") bell: ElementRef<HTMLImageElement>;
  @ViewChild("Warning") warning: ElementRef<HTMLElement>;
  @ViewChildren("Tpt") drop: ElementRef<HTMLDivElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  listAsks = [];
  childToDelete = { 
    "relationId": 0,
    "name": ""
  };
  listTests = [
    {
      character: "toto",
      name: "titi",
    },
    {
      character: "toto",
      name: "titi",
    },
    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },
  ];
  addForm = new FormGroup({
    "username": new FormControl(null, Validators.required)
  });
  numberNotif = 0;
  ngOnInit() {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
    this.userService.getRelation("STUDENT").then(response => {
      console.log(response);
      response["body"]["relations"].forEach(child => {
        this.listChildren.push({
          "character": child["user"]["character"].name + ".png",
          "name": child["user"].firstname,
          "pseudo": child["user"].pseudo,
          "relationId": child.id
        });
      });
    }).then(() =>  {
      this.userService.pendingInvitation().then(response => {
        response["body"]["pendingInvitations"].forEach(demand => {
          if (demand.userType !== "PARENT") {
            this.listAsks.push({
              "relationId": demand.id,
              "name": demand["user"].firstname,
              "character": demand["user"]["character"].name + ".png",
              "visibility": "visible",
            });
          }
        });
        if (this.listAsks.length !== 0) {
          this.numberNotif = this.listAsks.length;
          if (this.isMobile) {
            this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile has-animation-bell";
            this.number.nativeElement.className = "notif has-left-margin notif-size-mobile has-animation";
          } else {
            this.bell.nativeElement.className = "has-left-margin is-bell has-animation-bell";
            this.number.nativeElement.className = "notif notif-size-desktop has-left-margin has-animation";
          }
          this.number.nativeElement.style.visibility = "visible";
        } 
        else {
          if(this.isMobile)  {
            this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          } else {
            this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed";
          }
          this.number.nativeElement.style.visibility = "hidden";
        }
      })
      if (this.listChildren.length === 0) {
        this.hasNoChildren = true;
      } else {
        this.hasChildren = true;
      }
    });
  }

  goPerso(perso){
    return `assets/images/HeaderImages/${perso}`;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.numberNotif !== 0 && this.notif) {
        this.bell.nativeElement.className = "has-left-margin is-bell has-animation-bell";
        this.number.nativeElement.className = "notif notif-size-desktop has-left-margin has-animation";
      }
      if (!this.notif) {
        this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed";
        this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if(!this.notif) {
        this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
      }
      if(this.numberNotif !== 0 && this.notif) {
        this.number.nativeElement.className = "notif has-left-margin notif-size-mobile has-animation";
        this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile has-animation-bell";
      }
      this.isMobile = true;
    }
  }

  notif = false;
  display = true;
  img = "assets/images/bell.png";
  canDisplay = true;
  openNotif() {
    if (this.listAsks.length !== 0) {
      if (this.numberNotif !== 0) {
        if(!this.isMobile) {
          this.canDisplay = true;
        }
        if (this.display) {
          this.notif = true;
          if (this.isMobile)
            this.canDisplay = false;
          this.bell.nativeElement.classList.remove("has-animation-bell");
          this.number.nativeElement.classList.remove("has-animation");
          this.img = "assets/images/croix2.png";
          this.number.nativeElement.style.visibility = "hidden";
          this.display = false;
        } else {
          this.notif = false;
          if (this.isMobile)
            this.canDisplay = true;
          this.bell.nativeElement.classList.add("has-animation-bell");
          this.number.nativeElement.classList.add("has-animation");
          this.img = "assets/images/bell.png";
          this.number.nativeElement.style.visibility = "visible";
          this.display = true;
        }
      }
    }
  }

  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else
    if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
     this.oldIndexA = i;
    } else {
      this.oldIndexA = -1;
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }

  cancel(child: string) {
    if(child === "add") {
      this.add.nativeElement.classList.remove("is-active");
    } else if (child === "success")
      this.success.nativeElement.classList.remove("is-active");
    else if (child === "warning")
      this.warning.nativeElement.classList.remove("is-active");
    else
      this.failure.nativeElement.classList.remove("is-active");
  }

  delete() {
    this.submitButton2.nativeElement.classList.add("is-loading");
    this.userService.removeRelation(this.childToDelete.relationId).then(() => {
      this.submitButton2.nativeElement.classList.remove()
      this.listChildren = this.listChildren.filter(friend => friend.relationId !== this.childToDelete.relationId);
      this.warning.nativeElement.classList.remove("is-active");
      if (this.listChildren.length === 0) {
        this.hasNoChildren= true;
        this.hasChildren = false;
      }
    }).catch(() => {
      this.submitButton2.nativeElement.classList.remove("is-loading");
      this.warning.nativeElement.classList.remove("is-active");
      this.failure.nativeElement.classList.add("is-active");
    });
  }
  openAddChild() {
    this.add.nativeElement.classList.add("is-active");
  }

  acceptChild(child, index) {
    const body = {
      "id": child.relationId
    };
    this.userService.acceptInvitation(body).then(() => {
      this.listChildren.push({
        "character": child.character,
        "name": child.name,
        "relationId": child.relationId
      });
      this.listAsks = this.listAsks.filter(ask => ask !== child);
      if (this.listAsks.length === 0) {
        this.img = "assets/images/bell.png";
        this.notif = false;
        if(this.isMobile)  {
          this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        } else {
          this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
          this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed opacity";
        }
      } else {
        this.numberNotif--;
      }
    });
  }
  username = "";
  addChild() {
    this.submitButton.nativeElement.classList.add("is-loading");
    this.userService.sendInvitation(this.addForm.value).then(() => {
      this.submitButton.nativeElement.classList.remove("is-loading");
      this.add.nativeElement.classList.remove("is-active");
      this.username = this.addForm.get("username").value;
      this.success.nativeElement.classList.add("is-active");
    }).catch(() => {
      this.submitButton.nativeElement.classList.remove("is-loading");
      this.failure.nativeElement.classList.add("is-active");
    });
  }

  askDelete(event) {
    this.childToDelete = {
      "relationId": event.relationId,
      "name": event.name
    }
    this.warning.nativeElement.classList.add("is-active");
  }
  redirect() {
    document.location.reload(true);
  }

  Submit() {
    this.addForm.patchValue({
      "username": null
    });
    this.failure.nativeElement.classList.remove("is-active");
  }
  oldIndex = -1;
  open(i){
    if (this.oldIndex === -1) {
      this.oldIndex = i;
      this.drop["_results"][i].nativeElement.classList.add("is-active");
    } else
    if (i !== this.oldIndex) {
      this.drop["_results"][this.oldIndex].nativeElement.classList.remove("is-active");
      this.drop["_results"][i].nativeElement.classList.add("is-active");
      this.oldIndex = i;
    } else {
      this.oldIndexA = -1;
      this.drop["_results"][i].nativeElement.classList.remove("is-active");
    }
  }

  goStatschild(name) {
    return `/statistiques/${name}`;
  }

  declineChild(parent) {
    this.userService.declineInvitation(parent.relationId).then(() => {
      if (this.hasNoChildren) {
        this.hasChildren = true;
        this.hasNoChildren = false;
      }
      this.listAsks = this.listAsks.filter(ask => ask !== parent);
      if (this.listAsks.length === 0) {
        this.img = "assets/images/bell.png";
        this.notif =false;
        if(this.isMobile)  {
          this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        } else {
          this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
          this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed opacity";
        }
      } else {
        this.numberNotif--;
      }
    });
  }
}
