import { Component, ElementRef, HostListener, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { type } from 'os';
import { BehaviorSubject } from 'rxjs';
import { Globals } from 'src/assets/Globals';
import { AdminRoomService } from '../services/admin-room.service';

@Component({
  selector: 'app-admin-db-room-type',
  templateUrl: './admin-db-room-type.component.html',
  styleUrls: ['./admin-db-room-type.component.sass']
})
export class AdminDbRoomTypeComponent implements OnInit {
  @ViewChild("Delete", {static: false}) deleteModal: ElementRef<HTMLDivElement>;
  @ViewChild("Modify", {static: false}) modifyModal: ElementRef<HTMLDivElement>;
  @ViewChild("Add", {static: false}) addModal: ElementRef<HTMLDivElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  @ViewChild("SubmitButton") modifyButton: ElementRef<HTMLButtonElement>;
  @ViewChild("SubmitButton2") addButton: ElementRef<HTMLButtonElement>;
  listRoomTypes = [];
  listRoomTypesDisplayed = this.listRoomTypes;
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  failure = false;
  createForm = new FormGroup({
    "type": new FormControl(null, Validators.required)
  });

  modifyForm = new FormGroup({
    "uuid": new FormControl(null),
    "type": new FormControl(null, Validators.required)
  });
  constructor(public globals: Globals, private roomService: AdminRoomService) { }

  isMobile = false;
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
    this.getTypes();
  }

  fail() {
    this.error$.next(false);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.ask)
        this.deleteModal.nativeElement.classList.remove("is-active");
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.ask)
        this.deleteModal.nativeElement.classList.add("is-active");      
      this.isMobile = true;
    }
  }

  getTypes() {
    this.roomService.getRoomsTypes().then(response => {
      response["body"]["types"].forEach(elt => {this.listRoomTypes.push(elt)});
      this.loading = false;
      this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);
    });
  }

  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
      this.oldIndexA = i;
    } else {
      this.oldIndexA = -1;
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }
  text = "";
  openModal() {
    this.addModal.nativeElement.classList.add("is-active");
  }
  goModify(elt) {
    this.text = elt.type;
    this.modifyForm.patchValue({
      "uuid": elt.uuid,
      "type": elt.type
    });
    this.modifyModal.nativeElement.classList.add("is-active");
  }
  refresh() {
  const wordToUpperCase = this.Search.nativeElement.value.toUpperCase();
     this.listRoomTypesDisplayed=  this.listRoomTypes.filter(elt => {
      const type = elt.type;
      return type.includes(wordToUpperCase);
    });
  }

  ask = false;
  currentIndex = 0;
  askDelete(id)
  {
    this.ask = true;
    this.currentIndex = id;
    if (this.globals.isMobile)
      this.deleteModal.nativeElement.classList.add("is-active");
    this.text = this.listRoomTypesDisplayed[id].type;
    this.listRoomTypesDisplayed[id].confirm = true;
    this.listRoomTypesDisplayed[id].error = false;
    this.listRoomTypesDisplayed[id].cancel = true;
  }

  close(modal) {
    if (modal === "delete") {
      this.deleteModal.nativeElement.classList.remove("is-active");
       this.listRoomTypesDisplayed[this.currentIndex].confirm = false;
       this.listRoomTypesDisplayed[this.currentIndex].cancel = false;
    } else if (modal === "add") {
      this.addModal.nativeElement.classList.remove("is-active");
      this.createForm.patchValue({
        "type": null
      });
    } else {
      this.modifyModal.nativeElement.classList.remove("is-active");
      this.modifyForm.patchValue({
        "uuid": null,
        "type": null
      });
    }
  }

  delete(id) {
    this.ask = false;
    const typeToRemove = this.listRoomTypesDisplayed[id];
    this.roomService.deleteRoomType(typeToRemove.uuid).then(() => {
      this.listRoomTypes = this.listRoomTypes.filter(type => type.uuid !== typeToRemove.uuid);
      this.deleteModal.nativeElement.classList.remove("is-active");
      this.refresh();
    }).catch(() => {
      this.listRoomTypesDisplayed[id].error = true;
      this.deleteModal.nativeElement.classList.remove("is-active");
      this.listRoomTypesDisplayed[id].confirm = false;
      this.listRoomTypesDisplayed[id].cancel = false;
    });
  }

  cancel(id) {
    this.ask = false;
    if (this.globals.isMobile) {
      this.deleteModal.nativeElement.classList.remove("is-active");
    }
    this.listRoomTypesDisplayed[id].confirm = false;
    this.listRoomTypesDisplayed[id].cancel = false;
  }
  private regex = '^([A-Z]+_*)+$';
  isValid(modal) {
    if (modal === "create") {
      const type = this.createForm.get("type").value;
      if (type !== null)
        return type.search(this.regex) !== -1 ? true : false;
      return false;
    } else {
      const type = this.modifyForm.get("type").value;
      if (type !== null)
        return type.search(this.regex) !== -1 ? true : false;
      return false;
    }
  }
  submitAddForm() {
    this.addButton.nativeElement.classList.add("is-loading");
      const body = {
        "type": this.createForm.get("type").value
      };
      this.roomService.createRoomType(body).then(response => {
        this.listRoomTypes.push(response["body"]);
        this.listRoomTypesDisplayed = this.listRoomTypes;
        this.createForm.patchValue({
          "type": null
        });
        this.addModal.nativeElement.classList.remove("is-active");
        this.addButton.nativeElement.classList.remove("is-loading");
      }).catch(() => {this.failure = true; 
        this.addButton.nativeElement.classList.remove("is-loading");
        setTimeout(() => this.failure = false, 500);
        clearTimeout();
      });
  }
  submitUpdateForm() {
    this.modifyButton.nativeElement.classList.add("is-loading");
    const body = {
      "uuid": this.modifyForm.get("uuid").value,
      "type": this.modifyForm.get("type").value
    };
    this.roomService.updateRoomType(body).then(response => {
      const index = this.listRoomTypes.findIndex(elt => elt.uuid === response["body"].uuid);
      this.listRoomTypes[index].type = response["body"].type;
      this.modifyForm.patchValue({
        "uuid": null,
        "type": null
      });
      this.modifyButton.nativeElement.classList.remove("is-loading");
      this.modifyModal.nativeElement.classList.remove("is-active");
    }).catch(() => {this.failure = true; 
      this.modifyModal.nativeElement.classList.remove("is-active");
      setTimeout(() => this.failure = false, 500);
      clearTimeout();
    });
  }
}
