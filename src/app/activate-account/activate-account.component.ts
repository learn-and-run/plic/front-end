import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from 'src/assets/Globals';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.sass']
})
export class ActivateAccountComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, public globals: Globals) { }
  userId: string;
  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("ActiveButton") ActiveButton: ElementRef<HTMLElement>;

  ngOnInit() {
    this.userId = this.route.snapshot.params['id'];
  }

  activateAccount() {
    this.ActiveButton.nativeElement.classList.add("is-loading");
    const onSuccess = () => {
      this.ActiveButton.nativeElement.classList.remove("is-loading");
      this.Success.nativeElement.classList.add("is-active");
    };

    const onFailed = () => {
      this.Failure.nativeElement.classList.add("is-active");
      this.ActiveButton.nativeElement.classList.remove("is-loading");
    };

    this.userService.activateAccount(this.userId).then(onSuccess).catch(onFailed);
  }

  cancel(child: string) {
    if (child === "success")
      this.Success.nativeElement.classList.remove("is-active");
    else
      this.Failure.nativeElement.classList.remove("is-active");
  }

  redirect() {
    this.router.navigate(['/accueil']).then(() => document.location.reload());
  }
}
