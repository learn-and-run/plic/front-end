import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { server } from "../../assets/config";
import { Injectable, resolveForwardRef } from '@angular/core';
import { promise } from 'protractor';
import { nextTick } from 'process';

@Injectable({
  providedIn: 'root',
})

export class HttpService {
  constructor(private http: HttpClient) { }

  urlEncodedHeader = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
  jsonHeader = new HttpHeaders({'Content-Type': 'application/json'});

  public get(path: string): Promise<Object> {
    return new Promise((resolve, reject) => {
        this.http.get(path, {
          observe: 'response', 
          withCredentials: true
        }).subscribe({
          next: response => resolve(response),
          error: err => reject(err)
        });
    });
  }

  public post(path: string, body?: Object) : Promise<Object> {
    return new Promise((resolve, reject) => {
      this.http.post(path, body, {
        observe: 'response',
        headers: this.jsonHeader, 
        withCredentials: true
      }).subscribe({
        next: response => resolve(response),
        error: err => reject(err) 
      })
    });
  }

  public delete(path: string) : Promise<Object> {
    return new Promise((resolve, reject) => {
      this.http.delete(path, {
        observe: 'response',
        headers: this.urlEncodedHeader,
        withCredentials: true
      }).subscribe({
        next: response => resolve(response),
        error: err => reject(err)
      });
    });
  }

  public patch(path: string, body?: object): Promise<Object> {
    return new Promise((resolve, reject) => {

      this.http.patch(path, body, {
        observe: 'response',
        headers: this.jsonHeader,
        withCredentials: true
      }).subscribe({
        next: response => resolve(response),
        error: err => reject(err)
      });
    });
  }
}