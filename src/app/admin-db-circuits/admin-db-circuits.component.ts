import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Globals } from 'src/assets/Globals';
import { AdminCircuitService } from '../services/admin-circuit.service';

@Component({
  selector: 'app-admin-db-circuits',
  templateUrl: './admin-db-circuits.component.html',
  styleUrls: ['./admin-db-circuits.component.sass']
})
export class AdminDbCircuitsComponent implements OnInit {
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  @ViewChild('Hola', {static: false}) hola: ElementRef<HTMLDivElement>;

  currentModule = 0;
  textColor = "#b63a6b";
  borderColor = "#b63a6b";
  testModules = [
    {
      item: "Tous",
      itemValue: "TOUS",
      textColor: "#b63a6b",
      current: "#b63a6b"
    },
    {
      item: "Général",
      itemValue: "GENERAL",
      textColor: "#292f33",
      current: "#dbdbdb"
    },
    {
      item: "Mathématiques",
      itemValue: "MATHS",
      textColor: "#51c47f",
      current: "#dbdbdb"
    },
    {
      item: "Français",
      itemValue: "FRANCAIS",
      textColor: "#1c7cac",
      current: "#dbdbdb",
    },
    {
      item: "Physique",
      itemValue: "PHYSIQUE",
      textColor: "#d37f50",
      current: "#dbdbdb"

    },
    {
      item: "Chimie",
      itemValue: "CHIMIE",
      textColor: "#db983d",
      current: "#dbdbdb"

    },
    {
      item: "Anglais",
      itemValue: "ANGLAIS",
      textColor: "#cd444b",
      current: "#dbdbdb"

    },
    {
      item: "Histoire",
      itemValue: "HISTOIRE",
      textColor: "#ce80b2",
      current: "#dbdbdb"

    },
    {
      item: "Géographie",
      itemValue: "GEOGRAPHIE",
      textColor: "#a5619e",
      current: "#dbdbdb"
    }
  ];
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  currentIndex = 0;
  listCircuits = [];
  listCircuitsDisplayed = this.listCircuits.sort();
  constructor(private circuitService: AdminCircuitService, public globals: Globals) { 
    this.getCircuits();
  }
  getCircuits() {
    this.circuitService.GetCircuits("TOUS").then(response => {
      response["body"]["circuits"].forEach(circuit => 
        this.listCircuits.push({
          circuit,
          "confirm": false,
          "cancel": false,
          "error": false,
          "isDisplayMobile": false
        })
      )
    }).then(() =>{
      this.loading = false;
      this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);
    });
  }
  isMobile = false;
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
  }
  currentColor = "#b63a6b";

  change(item, id) {
    if (this.currentModule !== id) {
      this.testModules[this.currentModule].current = "#dbdbdb";
      this.currentModule = id;
      this.currentColor = this.testModules[id].textColor;
      this.testModules[id].current = this.testModules[id].textColor;
      this.refresh();
    }
  }
  ask = false;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.ask)
        this.listCircuitsDisplayed[this.currentIndex].isDisplayMobile = false;
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.ask)
        this.listCircuitsDisplayed[this.currentIndex].isDisplayMobile = true;
      this.isMobile = true;
    }
  }

  refresh() {
      const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
      // check if the tabs clicked is TOUS
      if (this.testModules[this.currentModule].itemValue === "TOUS") {
        // Simple search
        this.listCircuitsDisplayed= this.listCircuits.filter(circuit => {
          const circuitLowerCase = circuit.circuit.name.toLowerCase();
          return circuitLowerCase.includes(wordToLowerCase);
        });
      } else {
        // filtering circuit by the module clicked and the search word
        this.listCircuitsDisplayed = this.listCircuits.filter(circuit => {
          if (circuit.circuit.module === this.testModules[this.currentModule].itemValue) {
            const circuitLowerCase = circuit.circuit.name.toLowerCase();
            return circuitLowerCase.includes(wordToLowerCase);
          }
        });
      }
  }

  goModify(circuit) {
    localStorage.setItem("Circuit", JSON.stringify(circuit.circuit));
  }

  askDelete(id)
  {
    this.ask = true;
    this.currentIndex = id;
    if (this.globals.isMobile) {
      this.listCircuitsDisplayed[id].isDisplayMobile = true;
    }
   this.listCircuitsDisplayed[id].confirm = true;
   this.listCircuitsDisplayed[id].error = false;
   this.listCircuitsDisplayed[id].cancel = true;
  }

  delete(id) {
    this.ask = false;
    this.listCircuitsDisplayed[id].isDisplayMobile = false;
    const circuitToRemove = this.listCircuitsDisplayed[id];
    this.circuitService.DeleteCircuit(circuitToRemove.circuit.id).then(() => {
      this.listCircuits = this.listCircuits.filter(circuit => circuit.circuit.id !== circuitToRemove.circuit.id);
      this.refresh();
    }).catch(() => {
      this.listCircuitsDisplayed[id].error = true;
      this.listCircuitsDisplayed[id].isDisplayMobile = true;
      this.listCircuitsDisplayed[id].confirm = false;
      this.listCircuitsDisplayed[id].cancel = false;
    });
  }

  cancel(id) {
    this.ask = false;
    this.listCircuitsDisplayed[id].isDisplayMobile = false;
    this.listCircuitsDisplayed[id].confirm = false;
    this.listCircuitsDisplayed[id].cancel = false;
  }

  display = false;
  test() {
    this.display = !this.display;
    if (this.display) {
      this.hola.nativeElement.classList.add('is-active');
    } else {
      this.hola.nativeElement.classList.remove('is-active');
    }
  }

  fail() {
    this.error$.next(false);
  }
}
