import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { adminModules } from 'src/app/app-constant';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminRoomService } from 'src/app/services/admin-room.service';
import { AdminQuestionService } from 'src/app/services/admin-question.service';
import { Location } from '@angular/common';
import { AdminCircuitService } from 'src/app/services/admin-circuit.service';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'create-circuit',
  templateUrl: './admin-db-create-circuit.component.html',
  styleUrls: ['./admin-db-create-circuit.component.sass']
})
export class AdminDbCreateCircuitComponent implements OnInit {
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  listRooms = [];
  listRoomsDisplayed = [];
  scenes = [];
  errorText = "Chargement des questions impossible pour le moment."
  modules = adminModules;
  display$ = new BehaviorSubject(false);
  @ViewChild("Rooms", {static: false}) rooms: ElementRef<HTMLDivElement>;
  error$ = new BehaviorSubject(false);
  loading = true;
  createForm = new FormGroup({
    "name": new FormControl("", Validators.required),
    "pictureName": new FormControl("", Validators.required),
    "module": new FormControl("GENERAL", Validators.required),
    "scene_uuid": new FormControl(null, Validators.required)
  });
  @ViewChild("SubmitButton") SubmitButton: ElementRef;
  preview = "";
  nameImg = "";
  failure = false;
  textError = "Quelque chose s'est mal passé.";
  constructor(private roomService: AdminRoomService, private circuitService: AdminCircuitService, private questionService: AdminQuestionService, private location: Location, public globals: Globals) { 
    this.modules.push({
      text: "Général",
      value: "GENERAL"
    }
    );
  }
  isMobile = false;

  questions = [];
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
  
    this.getQuestions();
  }

  getQuestions() {
    this.questionService.GetQuestions("TOUS").then(response => {
      response["body"]["questions"].forEach(question => {
        this.questions.push({
          "id": question.id,
          "name": question.name,
        });
      });
    }).then(() =>  {this.roomService.getRooms("TOUS").then(response => {
      response["body"]["rooms"].forEach(room => {
        const question = this.questions.find(q => q.id === room.questionId);
        this.listRooms.push(
          {
            "roomId": room.id,
            "time": this.getFormatedTime(room.expectedTime),
            "module": room.module,
            "name": question.name,
            "background": "",
            "text": "black",
            "isCheck": false
          });
        this.listRoomsDisplayed = this.listRooms;
      });
    }).then(() => {
      this.circuitService.getScenesTypes().then(response => {
        response["body"]["scenes"].forEach(elt => {
          this.scenes.push({
            "uuid": elt.uuid,
            "name": elt.name
          });
        });
      }).catch(() => {
        this.textError = "Chargement des scènes impossible pour le moment.";
        this.error$.next(true);
      });
    }).then(() => {
      this.loading = false;
      this.display$.next(true);
    }).catch(() => {
      this.loading = false;
      this.errorText = "Chargement des salles impossible pour le moment."
      this.error$.next(true);
    });
  }).catch(() => 
    {
      this.loading = false;
      this.textError = "Chargement des questions impossible pour le moment."
      this.error$.next(true)
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.isOpen){
        this.isOpen = false;
        this.rooms.nativeElement.classList.remove("is-active");       
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      this.isMobile = true;
    }
  }

  convertTime(time: number) {
    let timer = time / 1000;
    let minutes = Math.trunc(timer / 60);
    let seconds = Math.trunc(timer % 60);
    return {"seconds": seconds, "minutes": minutes};
  }

  getFormatedTime(timeMiliseconds) {
    const time = this.convertTime(timeMiliseconds);
    let formatedTime = "";
    if (time.minutes < 10)
      formatedTime += "0" + time.minutes;
    else
      formatedTime += time.minutes;
    formatedTime += ":";
    if (time.seconds < 10) {
      formatedTime += "0" + time.seconds;
    } else {
      formatedTime += time.seconds;
    }
    return formatedTime;
  }

  upload(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.preview = reader.result as string;
      };
    }
    this.nameImg = this.createForm.get("pictureName").value.substring(12);
  }

  chooseQuestion(id) {
    // Re init default color 
    if (this.listRoomsDisplayed[id].text !== "black") {
      this.listRoomsDisplayed[id].text = "black";
      this.listRoomsDisplayed[id].background = "";
      this.listRoomsDisplayed[id].isCheck = false;
    } else {
      this.listRoomsDisplayed[id].text = "white";
      this.listRoomsDisplayed[id].background = "#5a5a5a";
      this.listRoomsDisplayed[id].isCheck = true;
    }
  }

  refresh() {
    const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
    // check if the tabs clicked is null
    if (this.createForm.get("module").value === "GENERAL") {
      // Simple search
      this.listRoomsDisplayed= this.listRooms.filter(question=> {
        const questionLowerCase = question.name.toLowerCase();
        return questionLowerCase.includes(wordToLowerCase);
      });
    } else {
      // filtering room by the module clicked and the search word
      this.listRoomsDisplayed = this.listRooms.filter(question => {
        if (question.module === this.createForm.get("module").value) {
          const roomLowerCase = question.name.toLowerCase();
          return roomLowerCase.includes(wordToLowerCase);
        }
      });
    }
  }


  refactoList() {

    this.listRoomsDisplayed.forEach(room => {
      room.background = "";
      room.text = "black";
      room.isCheck = false;
    });
    if (this.createForm.get("module").value === "GENERAL") {
      this.listRoomsDisplayed = this.listRooms;
    } else {
      this.listRoomsDisplayed = this.listRooms.filter(question => {
        return (question.module === this.createForm.get("module").value);
      });
    }
    this.refresh();
  }

  createCircuit() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const picture =this.createForm.get("pictureName").value.substring(12);
    const namePicture = picture.split('.');

    const body = {
      "name": this.createForm.get("name").value,
      "module": this.createForm.get("module").value,
      "pictureName": namePicture[0],
      "scene_uuid": this.createForm.get("scene_uuid").value
    };
    
    const checked = this.listRoomsDisplayed.filter(r => r.isCheck);
    const listIds = [];
    checked.forEach(check => {
      listIds.push(check.roomId);
    });
    this.circuitService.CreateCircuit(body).then(response => {
      const body = {
        "circuitId": response["body"].id,
        "roomIds": listIds
      };
      this.circuitService.updateRooms(body).then(() => {
        this.location.back();
      }).catch(() => {
        this.SubmitButton.nativeElement.classList.remove("is-loading");
        this.failure = true;
        setTimeout(() => this.failure = false, 500);
        clearTimeout();
      });
    }).catch(error => {
      if (error.error.status === 409) {
        this.textError = "Un circuit avec le nom \"" + body.name + "\" existe déjà";
      }
      else {
        this.textError = "Quelque chose s'est mal passé.";
      }
      this.failure = true;
      this.createForm.patchValue({
        "name": "",
        "pictureName": "",
        "module": "GENERAL",
        "scene_uuid": null
      });
      setTimeout(() => this.failure = false, 500);
      clearTimeout();
    this.nameImg = "";
      this.preview = "";
      this.SubmitButton.nativeElement.classList.remove("is-loading");
    });
      // checked.forEach(room => {
      //   if(room.isCheck) {
          // this.circuitService.LinkRoom({
          //   "circuitId": response["body"].id,
          //   "roomId": room.roomId
          // });
        // }
      // });
  }

  fail() {
    this.failure = false;
  }
  back() {
    this.location.back();
  }
  isOpen = false;
  openModal() {
    this.isOpen = true;
    this.rooms.nativeElement.classList.add("is-active");
  }

  close() {
    this.isOpen = false;
    this.rooms.nativeElement.classList.remove("is-active");
  }

  error() {
    this.error$.next(false);
  }
}
