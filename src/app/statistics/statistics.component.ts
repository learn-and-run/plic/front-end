import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef, ViewChildren} from '@angular/core';
import {modules} from '../app-constant';
import { UserService } from '../services/user.service';
import { BehaviorSubject } from 'rxjs';
import { StatsService } from '../services/stats.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AdminRoomService } from '../services/admin-room.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.sass']
})

export class StatisticsComponent implements OnInit {

  isLoading$ = new BehaviorSubject(false);
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  Display$ = new BehaviorSubject(false);
  @ViewChild('Hola', {static: false}) hola: ElementRef<HTMLDivElement>;
  @ViewChild('Stats', {static: false}) mobileStats: ElementRef<HTMLDivElement>;
  @ViewChildren('TR') tr: ElementRef<HTMLElement>;
  circuits = [];
  isLink$ = new BehaviorSubject(false);
  pseudo = '';
  title = 'Tes statistiques';
  isMobile = false;
  currentModule = 0;
  currentColor = 'rgb(41, 47, 51, 1)';
  timeString = '';
  modules = modules;
  className = "";
  scoreImg = '';
  timeImg = '';
  constructor(private userService: UserService, private statsService: StatsService, private roomService: AdminRoomService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    if (this.route.snapshot.params.pseudo !== undefined) {
      this.pseudo = this.route.snapshot.params.pseudo;
      this.isLink$.next(true);
      this.CallBack();
      this.Display$.next(true);
      this.title = 'Statistiques de ' + this.pseudo;
    } else {
      this.userService.getUserInfo().then(response => {
        this.pseudo = response["body"].user.pseudo;
      }).then(() => this.CallBack()).then(() => this.Display$.next(true));
    }
    if (window.innerWidth < 1024) {
      this.isMobile = true;
      this.className = "is-flex is-justify-center";
    } else {
      this.isMobile = false;
      if (this.isLink$.value) {
        this.className = "is-flex is-justify-center";
      } else {
        this.className = "is-flex is-space-around";
      }
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if(this.mobileStats.nativeElement.classList.contains("is-active")) {
        this.mobileStats.nativeElement.classList.remove("is-active");
        this.enigme = true;
      } else {
        this.enigme = false;
      }
      if (this.isLink$.value) {
        this.className = "is-flex is-justify-center";
      } else {
        this.className = "is-flex is-space-around";
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.enigme){
        this.mobileStats.nativeElement.classList.add("is-active");
        this.enigme = false;
      }
      this.className = "is-flex is-justify-center";
      this.isMobile = true;
    }
  }

  getLetter() {
    if (this.modules[this.currentModule].db === 'GENERAL') {
      return 'T';
    } else {
      return this.modules[this.currentModule].db.substring(0, 1);
    }
  }

  path(roundImage, isSelected) {
    if (isSelected) {
      return roundImage.includes("Check") ? "../assets/images/CircuitsIcon/" + this.modules[this.currentModule].checkW : "../assets/images/CircuitsIcon/round.png";
    }
    return "../assets/images/CircuitsIcon/" + roundImage;
  }

  display = false;
  test() {
    this.display = !this.display;
    if (this.display) {
      this.hola.nativeElement.classList.add('is-active');
    } else {
      this.hola.nativeElement.classList.remove('is-active');
    }
  }


  
  getImgColor() {
    const letter = this.getLetter();
    return '../assets/images/score_time/time_' + letter + '.png';
  }
  getImgScore() {
    const letter = this.getLetter();
    return '../assets/images/score_time/medaille_' + letter + '.png';
  }


  getPath(name) {
    return `../assets/images/matières/${name}`;
  }
  CallBack() {
    this.isLoading$.next(true);
    this.canDisplay$.next(false);
    this.circuits = [];
    this.enigme = false;
    this.listRooms = [];
    this.notYet = false;
    const onSuccess = response => {
        response.body.scores.forEach(element => {
          this.circuits.push({
            id: element.circuitId,
            name: element.circuitName,
            score: element.score,
            time: this.SecondsToTime(element.spendTime),
            roundImage: element.score === 100 ? this.modules[this.currentModule].check : this.modules[this.currentModule].uncheck,
            isSelected: false
          });
        });
        this.scoreImg = this.modules[this.currentModule].scoreIcon;
        this.timeImg = this.modules[this.currentModule].timeIcon;
        setTimeout(() => {
          this.canDisplay$.next(true);
          this.isLoading$.next(false);
        }, 1000);
        clearTimeout();
      };
    const onFailure = () => {
        this.isLoading$.next(false);
        this.error$.next(true);
      };
    const body = {
        pseudo: this.pseudo,
        module: this.modules[this.currentModule].db
      };
    this.statsService.getStats(body).then(onSuccess).catch(onFailure);
  }

  onClick(id) {
    if (this.currentModule !== id) {
      this.modules[this.currentModule].opacity = 0.5;
      this.currentModule = id;
      this.currentColor = this.modules[id].color;
      this.modules[id].opacity = 1;
      this.CallBack();
    }
  }

  back() {
    this.location.back();
  }
  close() {
    this.enigme = false;
  }

  Cancel() {
    this.mobileStats.nativeElement.classList.remove('is-active');
  }
  enigme = false;
  notYet = false;
  text = "";
  listRooms = [];
  currentIndex = -1;
  openEnigma(id, score, i) {
    this.listRooms = [];
    if (!this.isLink$.value)
    {
      this.changeSelection(i);
      this.circuits[i].isSelected = true;
      if (score === 100) {
        this.notYet = false;
        this.roomService.associatedCircuit(id).then(response => {
          if (response["body"]["rooms"].length === 0) {
            this.enigme = false;
            this.text = "Désolé, ce circuit ne contient pas encore d'énigmes.";
          }
          else {
            response["body"]["rooms"].forEach(room => {
              this.listRooms.push({
                name: room.question.name
              });
            });
            if (this.isMobile){
              this.mobileStats.nativeElement.classList.add("is-active");
            } else {
              this.enigme =true;
            }
          }
        });
      } else {
        this.text = "Désolé, ce circuit n'est pas terminé, tu ne peux pas avoir accès aux énigmes avant de les avoir faites.";
        if (!this.isMobile) {
          this.enigme = false;
        } else {
          this.mobileStats.nativeElement.classList.add("is-active");
        }
        this.notYet = true;
      }
    }   
  }

  thirdCase = false;
  index = -1;
  changeSelection(i) {
    if (this.currentIndex === -1) {
      if (this.thirdCase) {
        this.circuits[this.index].isSelected = false;
        this.thirdCase = false;
      }
      this.currentIndex = i;
      this.circuits[i].isSelected = true;
    } else {
      if(this.currentIndex !== i) {
        this.circuits[this.currentIndex].isSelected = false;
        this.circuits[i].isSelected = true;
        this.currentIndex = i;
      } else {
        this.currentIndex = -1;
        this.thirdCase = true;
        this.index = i;
        this.circuits[i].isSelected = true;
      }
    }
  }

  SecondsToTime(given_seconds: number) {
    const dateObj = new Date(given_seconds * 1000);
    const hours = dateObj.getUTCHours();
    const minutes = dateObj.getUTCMinutes();
    const seconds = dateObj.getSeconds();
    let time = '';

    if (hours === 0 && minutes === 0 && seconds !== 0) {
      time = seconds.toString().padStart(2, '0') + ' s ';
    } else if (hours === 0 && minutes === 0 && seconds === 0) {
      time = '--';
    } else if (hours === 0 && minutes !== 0 && seconds === 0) {
          time = minutes.toString().padStart(2, '0') + ' min ';
    } else if (hours === 0 && minutes !== 0 && seconds !== 0) {
          time = minutes.toString().padStart(2, '0') + ' min ' + seconds.toString().padStart(2, '0') + ' s ';
    } else if (hours !== 0 && minutes === 0 && seconds === 0) {
          time = hours.toString().padStart(2, '0') + ' h ';
    } else if (hours !== 0 && minutes !== 0 && seconds === 0) {
          time = hours.toString().padStart(2, '0') + ' h ' + minutes.toString().padStart(2, '0') + ' min ';
    } else {
          time = hours.toString().padStart(2, '0') + ' h ' +
          minutes.toString().padStart(2, '0') + ' min ' +
          seconds.toString().padStart(2, '0') + ' s ';
    }
    return time;
  }
}
