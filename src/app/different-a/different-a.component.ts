import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'different-a',
  templateUrl: './different-a.component.html',
  styleUrls: ['./different-a.component.sass']
})
export class DifferentAComponent implements OnInit {

  @Input() textColor: string;
  @Input() borderColor: string;
  @Input() item: string;

  constructor() { }

  ngOnInit() {
  }

}
