import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.sass']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, public globals: Globals) { }

  id = "";
  Password = new FormGroup({
    "newPassword": new FormControl("", Validators.required),
  });

  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("SubmitButton") SubmitButton: ElementRef<HTMLElement>;

  private readonly imgPath = "../assets/images/";
  eyeImg = `${this.imgPath}eye.png`;
  inputType = "password";

  display = false;
  Display() {
    this.display = !this.display;
    if (this.display) {
      this.eyeImg = `${this.imgPath}eyeClose.png`;
      this.inputType = "text";
    } else {
      this.inputType = "password";
      this.eyeImg = `${this.imgPath}eye.png`;
    }
  }
  
  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
  }

  cancel(child: string) {
    if (child === "success")
      this.Success.nativeElement.classList.remove("is-active");
    else
      this.Failure.nativeElement.classList.remove("is-active");
  }

  redirect() {
    this.router.navigate(['/accueil']).then(() => document.location.reload());
  }

  Submit() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const onSuccess = () => {
      this.Success.nativeElement.classList.add("is-active");
      this.SubmitButton.nativeElement.classList.remove("is-loading");
    };

    const onFailed = () => {
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      this.Failure.nativeElement.classList.add("is-active");
    };
    
    this.userService.resetPassword(this.id, this.Password.get("newPassword").value).then(onSuccess).catch(onFailed); 
  }


  checkRegex(regex, password: string) {
    return password.search(regex);
  }

  isValid() {
    const password = this.Password.get("newPassword").value;
    var regexp =/[@[ \]^_!"#$%&'()*+,-./:;{}<>=|~?]+/g;
    if (this.checkRegex('[0-9]', password) !== -1
      && this.checkRegex('[a-z]', password) !== -1
      && this.checkRegex('[A-Z]', password) !== -1
      && this.checkRegex(regexp, password) !== -1
      && password.length >= 5 && password.length <= 72) {
        return true;
      }
    return false;
  }
}
