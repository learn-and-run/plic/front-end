import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { server } from "../assets/config";

@Injectable()
export class UserGuardService implements CanActivate {

    constructor(private router: Router, private http: HttpClient) {
    }
   
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | UrlTree {
        return Promise.resolve(this.http.get(`${server}/api/user/info`, {
            observe: 'response', 
            withCredentials: true
          }).toPromise()).then(res => { 
            if (res["body"]["user"]["userType"] === "STUDENT" && route.url.find(u => u.path.includes("enfants")))
            {
                alert("Tu n'es pas un parent. Tu n'as pas l'autorisation de voir cette page.");
                this.router.navigate(["/accueil"]).then(() => document.location.reload());
                return false;
            }
            else if (res["body"]["user"]["userType"] === "PARENT" && route.url.find(u => u.path.includes("statistiques/"))) {
              return true;
            }
            else if (res["body"]["user"]["userType"] === "PARENT" && route.url.find(u => u.path.includes("amis") || u.path.includes("classements") || u.path.includes("parents"))) {
                alert("Vous n'êtes pas un enfant. Vous n'avez pas l'autorisation de voir cette page.");
                this.router.navigate(["/accueil"]).then(() => document.location.reload());
                return false;
            }
            else {
                return true;
            }
          }).catch(() => {
            this.router.navigate(["/accueil"]).then(() => document.location.reload());
            return false;
          });
    }
}