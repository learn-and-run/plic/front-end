import { Component, OnInit, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { UserService } from '../services/user.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AdminItems } from '../app-constant';
enum UserMask {
  Lambda = 1,
  Student = 2,
  Parent = 4
}


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  
  @Input() user: string;
  @ViewChild('NavBarBurger', {static: true} as any) burger: ElementRef<HTMLElement>;
  @ViewChild('NavBar', {static: true} as any) navBar: ElementRef<HTMLElement>;
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("FailureFP", {static: true}) FailureFP: ElementRef;
  @ViewChild("ConnectPopUp") ConnectPopUp: ElementRef<HTMLElement>;
  @ViewChild("ForgotPassword") ForgotPassword: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton") SubmitButton: ElementRef<HTMLElement>;
  @ViewChild("SubmitButtonFP") SubmitButtonFP: ElementRef<HTMLElement>;
  @ViewChild("Account") account: ElementRef<HTMLElement>;
  textError = "";
  character = "Parent.png";
  protected readonly path = "../assets/images/";
  eyeImg = `${this.path}eye.png`;
  inputType = "password";
  isAuto = false;
  checkUncheck = `${this.path}case.png`;
  userInfo = {
    user: {
      pseudo: '',
    }
  };
  listAdmins = AdminItems;

  Connect = new FormGroup({
    "EmailOrPseudo": new FormControl("", Validators.required),
    "MDP": new FormControl("", Validators.required)
  });
  
  Forgot = new FormGroup({
    "Email": new FormControl("", Validators.required)
  });

  public isMobile: boolean = false;
  
  Icons = [
    // {
    //   name: "Home.png",
    //   text: "Accueil",
    //   title: "Accueil",
    //   path: "/",
    //   userType: UserMask.Lambda | UserMask.Student | UserMask.Parent
    // },
    {
      name: "perso.png",
      text: "Personnages",
      title: "Personnages",
      path: '/personnages',
      userType: UserMask.Lambda | UserMask.Student | UserMask.Parent
    }
  ];

  isAdmin$ = new BehaviorSubject(false);
  /**
   * Constructor of the header component
   * @param userService The user service given by dependency injection
   */
  constructor(private userService: UserService, private router: Router) {
    this.isMobile = window.innerWidth < 1024;

    this.userService.isAuthenticated().then(response => 
      {
        this.isAuto = response["body"]["isAuthenticated"];
        if (this.isAuto) {
          // If the user info fail, we disconnect the current user
          this.userService.getUserInfo()
                          .then(response => {
                            // console.log(response)
            this.userInfo = response["body"]["user"];
            this.character = response["body"]["user"]["character"].name + "Grande.png";
            this.addIconsHeader(this.userInfo["userType"]);
            
          }).then(() => {
            this.userService.isAdmin().then(response => {
              if (response["body"]["isAdmin"]) {
                this.isAdmin$.next(true);
              } else {
                this.isAdmin$.next(false);
              }
            });
          }).catch(() => this.isAuto = false);
        }
      }
    );
  }
 
  ngOnInit() {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // if (window.innerWidth >= 1024 && this.account.nativeElement.classList.contains("is-column")) {
    //   this.account.nativeElement.classList.remove("is-column");
    // } else if (window.innerWidth < 1024) {
    //   this.account.nativeElement.classList.add("is-column");
    // }
    if (window.innerWidth >= 1024 && this.isMobile) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      this.isMobile = true;
    }
  }

  click = false;
  openHeader() {
    this.click = !this.click;
    if (this.click) {
      this.burger.nativeElement.classList.add("is-active");
      this.navBar.nativeElement.classList.add("is-active");
    } else {
      this.navBar.nativeElement.classList.remove("is-active");
      this.burger.nativeElement.classList.remove("is-active");
    }
  }

  closeHeader() {
    if (this.burger.nativeElement.classList.contains("is-active")) {
      this.navBar.nativeElement.classList.remove("is-active");
      this.burger.nativeElement.classList.remove("is-active");
    }
  }
  
  ngAfterViewInit() {
    // const burger = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);//document.getElementsByClassName("navbar-burger");
    // if (burger.length > 0) {
    //   burger.forEach(element => {
    //     element.addEventListener('click', () => {

    //       // Get the target from the "data-target" attribute
    //       const target = element.dataset.target;
    //       const $target = document.getElementById(target);
  
    //       // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    //       element.classList.toggle('is-active');
    //       $target.classList.toggle('is-active');
  
    //     });
    //   });
    // }
  }

  // goToPath(path) {
  //   this.router.navigate([`${path}`]).then(() => {
  //     if (path === "/") {
  //       document.location.reload(true);
  //     }
  //   });
  // }
  
  GetImg(name: string) {
    if (name === "perso") {
      return "assets/images/HeaderImages/" + this.character;
    }
    return 'assets/images/HeaderImages/' + name;
  }

  ActiveModal(modal: string) {
    if(modal === "connect") {
      this.ConnectPopUp.nativeElement.classList.add("is-active");
    }
    if (modal === "forgotPassword") {
      this.ConnectPopUp.nativeElement.classList.remove("is-active");
      this.ForgotPassword.nativeElement.classList.add("is-active");
    }
  }

  /**
   * Close the modal 
   * @param modal The modal name
   */
  Cancel(modal: string) {
    if (modal === "C") {
      this.ConnectPopUp.nativeElement.classList.remove("is-active");
      // if(!this.SubmitButton.nativeElement.classList.contains("is-loading"))
      //   this.SubmitButton.nativeElement.classList.remove("is-loading");
      this.Connect.patchValue({
        "EmailOrPseudo": "",
        "MDP": ""
      });
    } else if (modal === "E") {
      // this.SubmitButton.nativeElement.classList.remove("is-loading");
      this.Connect.patchValue({
        "EmailOrPseudo": "",
        "MDP": ""
      });
      this.Failure.nativeElement.classList.remove("is-active");
    } else if (modal === "ER"){
      if (!this.ConnectPopUp.nativeElement.classList.contains("is-active")) {
        this.ConnectPopUp.nativeElement.classList.add("is-active");
      }
      // this.SubmitButtonFP.nativeElement.classList.remove("is-loading");
      this.Forgot.patchValue({
        "Email": ""
      });
      this.FailureFP.nativeElement.classList.remove("is-active");
    }
    else {
      this.ForgotPassword.nativeElement.classList.remove("is-active");
      this.ConnectPopUp.nativeElement.classList.add("is-active");
    }
  }

  rememberMe = false;
  Change() {
    if (!this.rememberMe) {
      this.checkUncheck = `${this.path}check.png`;
      this.rememberMe = true;
    } else {
      this.checkUncheck = `${this.path}case.png`;
      this.rememberMe = false;
    }
  }

  display = false;
  Display() {
    this.display = !this.display;
    if (this.display) {
      this.eyeImg = `${this.path}eyeClose.png`;
      this.inputType = "text";
    } else {
      this.inputType = "password";
      this.eyeImg = `${this.path}eye.png`;
    }
  }
  /**
   * Add Icons to the header by the specific userType
   * @param userType the userType of the user to add icons
   */
  private addIconsHeader(userType: string) {
    switch (userType) {
      case "STUDENT":
        this.Icons.push({
          name: "friends.png",
          text: "Amis",
          title: "Amis",
          path: "/amis",
          userType: UserMask.Student
        },
        {
          name: "parent.png",
          text: "Parents",
          title: "Parents",
          path: "/parents",
          userType: UserMask.Student
        },
        {
          name: "stats.png",
          text: "Statistiques",
          title: "Statistiques",
          path: "/statistiques",
          userType: UserMask.Student
        });
        break;
      case "PARENT":
        this.Icons.push({
          name: "child.png",
          text: "Enfants",
          title: "Enfants",
          path: "/enfants",
          userType: UserMask.Parent
        });
        break;
      default:
        console.log(`${userType} is not a valid userType`);
        break;
    }
  }

  /**
   * Call Login request
   */
  login() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const onSuccess = () => {
      this.isAuto = true;
      this.userService.getUserInfo()
                      .then(response => {
                        this.userInfo = response["body"]["user"];
                        this.character = response["body"]["user"]["character"].name + "Grande.png";

                        this.addIconsHeader(this.userInfo["userType"]);
                      }).then(() => {
                        this.SubmitButton.nativeElement.classList.remove("is-loading");
                        this.Cancel('C');
                      }).then(() => {
                        this.userService.isAdmin().then(response => {
                          if (response["body"]["isAdmin"]) {
                            this.isAdmin$.next(true);
                          } else {
                            this.isAdmin$.next(false);
                          }
                        });
                      }).catch(() => {
                        this.isAuto = false;
                      });
    };

    const onFailed = () => {
      this.ConnectPopUp.nativeElement.classList.remove("is-active");
      this.Failure.nativeElement.classList.add("is-active");
    };

    this.userService.login(this.Connect.get("EmailOrPseudo").value, this.Connect.get("MDP").value, this.rememberMe)
                    .then(onSuccess)
                    .catch(onFailed);
  }
  /**
   * Call the logout request
   */
  logout() {
    const onSuccess = () => {
      this.isAuto = false;
      // Keep only icons that contains the lambda mask
      this.Icons = this.Icons.filter(icon => (icon.userType & UserMask.Lambda) === UserMask.Lambda),
      this.Connect.reset({
        "EmailOrPseudo": "",
        "MDP": ""
      });
      this.router.navigate(['/accueil']).then(() => document.location.reload(true));
    };

    const onFailed = () => {
      alert("Erreur serveur");
    }

    this.userService.logout()
                    .then(onSuccess)
                    .catch(onFailed);
  }

  forgotPassword() {
    this.SubmitButtonFP.nativeElement.classList.add("is-loading");
    const onSuccess = () => {
      // this.Cancel("forgotPassword");
      this.ForgotPassword.nativeElement.classList.remove("is-active");
    };

    const onFailed = () => {
      // alert("L'email ne semble pas être correct. Réessayez.")
      this.SubmitButtonFP.nativeElement.classList.remove("is-active");
      this.FailureFP.nativeElement.classList.add("is-active");
    }

    this.userService.askResetPassword(this.Forgot.get("Email").value)
                    .then(onSuccess).then(() =>
                      {this.SubmitButtonFP.nativeElement.classList.remove("is-loading");}
                    ).catch(onFailed);    
  }

  tryAgain() {
    this.Failure.nativeElement.classList.remove("is-active");
    this.SubmitButton.nativeElement.classList.remove("is-loading");
    this.Connect.patchValue({
      "EmailOrPseudo": "",
      "MDP": ""
    });
    this.ConnectPopUp.nativeElement.classList.add("is-active");
  }
  tryAgainFP() {
    this.FailureFP.nativeElement.classList.remove("is-active");
    this.SubmitButtonFP.nativeElement.classList.remove("is-loading");
    this.Forgot.patchValue({
      "Email": ""
    });
    this.ForgotPassword.nativeElement.classList.add("is-active");
  }
}