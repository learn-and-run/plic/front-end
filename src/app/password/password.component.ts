import { Component, ElementRef, EventEmitter, forwardRef, HostListener, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.sass'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordComponent),
      multi: true
    }
  ]
})
export class PasswordComponent implements OnInit, ControlValueAccessor {

  @ViewChild("pass", {static: true}) pass: ElementRef<HTMLDivElement>;
  @ViewChild("Input", {static: true}) input: ElementRef<HTMLInputElement>;
  @ViewChild("eye", {static: true}) eye: ElementRef<HTMLImageElement>;
  @Input() inputModel: string;
  @Output() inputModelChange = new EventEmitter<string|number|boolean>();
  @Input() password: string;
  @Input() text: string;
  @Input() checkInput = false;
  @Input() isReadonly = false;
  private readonly imgPath = "../assets/images/";

  checks = [
    {
      img: `${this.imgPath}crossCase.png`,
      text: "Au moins un chiffre"
    },
    {
      img: `${this.imgPath}crossCase.png`,
      text: "Au moins une lettre"
    },
    {
      img: `${this.imgPath}crossCase.png`,
      text: "Au moins une majuscule"
    },
    {
      img: `${this.imgPath}crossCase.png`,
      text: "Au moins un caractère spécial"
    },
    {
      img: `${this.imgPath}crossCase.png`,
      text: "Une taille supérieur à 5 et inférieur à 72"
    }
  ];

  constructor() { }


  ngOnInit(): void {
    if (this.password === "hidden")
      this.inputType = "text";

    if (window.innerWidth < 1024) {
      this.pass.nativeElement.classList.add("passwordStyle-mobile");
      this.input.nativeElement.classList.add("inputSize-mobile");
      this.eye.nativeElement.classList.add("imageSize-mobile");
    } else {
      this.input.nativeElement.classList.add("inputSize-desktop");
      this.eye.nativeElement.classList.add("imageSize-desktop");
      this.pass.nativeElement.classList.add("passwordStyle-desktop");
    }
  }
  appear = false;
  pop() {
    if (this.checkInput) {
      this.appear = !this.appear;
    }
  }
  checkRegex(regex, password: string) {
    return password.search(regex);
  }

  checkPassword() {
    const img = `${this.imgPath}checkCase.png`;
    const img2 = `${this.imgPath}crossCase.png`;
    const password = this.input.nativeElement.value;

    this.checks[0].img = this.checkRegex('[0-9]', password) === -1 ? img2 : img;
    this.checks[1].img = this.checkRegex('[a-z]', password) === -1 ? img2 : img;
    this.checks[2].img = this.checkRegex('[A-Z]', password) === -1 ? img2 : img;
    this.checks[4].img = password.length >= 5 && password.length <= 72 ? img : img2;
    var regexp =/[@[ \]^_!"#$%&'()*+,-./:;{}<>=|~?]+/g;
    this.checks[3].img = this.checkRegex(regexp, password) === -1 ? img2 : img;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.pass.nativeElement.classList.contains("passwordStyle-mobile")) {
      this.input.nativeElement.classList.replace("inputSize-mobile", "inputSize-desktop");
      this.eye.nativeElement.classList.replace("imageSize-mobile", "imageSize-desktop");

      this.pass.nativeElement.classList.replace("passwordStyle-mobile", "passwordStyle-desktop");
    } else if (window.innerWidth < 1024 && this.pass.nativeElement.classList.contains("passwordStyle-desktop")) {
      this.input.nativeElement.classList.add("inputSize-desktop", "inputSize-mobile");
      this.eye.nativeElement.classList.replace("imageSize-desktop", "imageSize-mobile");
      this.pass.nativeElement.classList.replace("passwordStyle-desktop", "passwordStyle-mobile");
    }
  }

  eyeImg = `${this.imgPath}eye.png`;
  inputType = "password";

  display = false;
  Display() {
    this.display = !this.display;
    if (this.display) {
      this.eyeImg = `${this.imgPath}eyeClose.png`;
      this.inputType = "text";
    } else {
      this.inputType = "password";
      this.eyeImg = `${this.imgPath}eye.png`;
    }
  }

  changeValue(event): void {
    this.inputModel = event;
    this.inputModelChange.emit(this.inputModel);
    this.propagateChange(this.inputModel);
  }

  propagateChange: any = () => {};

  propagateTouched: any = () => {};

  writeValue(value): void {
    if (value !== undefined) {
      this.inputModel = value;
    }
  }

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn): void {
    this.propagateTouched = fn;
  }
}
