import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbRoomsComponent } from './admin-db-rooms.component';

describe('AdminDbRoomsComponent', () => {
  let component: AdminDbRoomsComponent;
  let fixture: ComponentFixture<AdminDbRoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbRoomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
