import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { UserService } from '../services/user.service';
@NgModule({
    imports:
    [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
    ],
    providers: [UserService]
})
export class ChildModule {}