import { Component, OnInit, ViewChild, ElementRef, HostListener, ViewChildren } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Globals } from 'src/assets/Globals';


@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.sass']
})
export class ParentComponent implements OnInit {
 
  constructor(private userService: UserService) { }
  listParent = [];
  hasParent = false;
  hasNoParent = false;
  isMobile = false;
  @ViewChild("Add") add: ElementRef<HTMLElement>;
  @ViewChild("Success") success: ElementRef<HTMLElement>;
  @ViewChild("Failure") failure: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton") submitButton: ElementRef<HTMLElement>;
  @ViewChild("Number") number: ElementRef<HTMLSpanElement>;
  @ViewChild("SubmitButton2") submitButton2: ElementRef<HTMLElement>;
  @ViewChild("Bell") bell: ElementRef<HTMLImageElement>;
  @ViewChild("Warning") warning: ElementRef<HTMLElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  listAsks = [];
  parentToDelete = { 
    "relationId": 0,
    "name": ""
  };
  listTests = [
    {
      character: "toto",
      name: "titi",
    },
    {
      character: "toto",
      name: "titi",
    },
    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },
  ];
  addForm = new FormGroup({
    "username": new FormControl(null, Validators.required)
  });
  numberNotif = 0;
  ngOnInit() {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
    this.userService.getRelation("PARENT").then(response => {
      response["body"]["relations"].forEach(parent => {
        this.listParent.push({
          "character": parent["user"]["character"].name + ".png",
          "name": parent["user"].firstname,
          "relationId": parent.id
        });
      });
    }).then(() =>  {
      this.userService.pendingInvitation().then(response => {
        response["body"]["pendingInvitations"].forEach(demand => {
          if (demand.userType === "PARENT") {
            this.listAsks.push({
              "relationId": demand.id,
              "name": demand["user"].firstname,
              "character": demand["user"]["character"].name + ".png",
              "visibility": "visible",
            });
          } 
        });
        if (this.listAsks.length !== 0) {
          this.numberNotif = this.listAsks.length;
          if (this.isMobile) {
            this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile has-animation-bell";
            this.number.nativeElement.className = "notif has-left-margin notif-size-mobile has-animation";
          } else {
            this.bell.nativeElement.className = "has-left-margin is-bell has-animation-bell";
            this.number.nativeElement.className = "notif notif-size-desktop has-left-margin has-animation";
          }
          this.number.nativeElement.style.visibility = "visible";
        } 
        else {
          if(this.isMobile)  {
            this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          } else {
            this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed";
          }
          this.number.nativeElement.style.visibility = "hidden";
        }
      });
      if (this.listParent.length === 0) {
        this.hasNoParent = true;
      } else {
        this.hasParent = true;
      }
    });
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.numberNotif !== 0 && this.notif) {
        this.bell.nativeElement.className = "has-left-margin is-bell has-animation-bell";
        this.number.nativeElement.className = "notif notif-size-desktop has-left-margin has-animation";
      }
      if (!this.notif) {
        this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed";
        this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if(!this.notif) {
        this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
      }
      if(this.numberNotif !== 0 && this.notif) {
        this.number.nativeElement.className = "notif has-left-margin notif-size-mobile has-animation";
        this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile has-animation-bell";
      }
      this.isMobile = true;
    }
  }
  notif = false;
  display = true;
  img = "assets/images/bell.png";
  canDisplay = true;
  openNotif() {
    if(this.listAsks.length !== 0) {
      if (this.numberNotif !== 0) {
        if(!this.isMobile) {
          this.canDisplay = true;
        }
        if (this.display) {
          this.notif = true;
          if (this.isMobile)
            this.canDisplay = false;
          this.bell.nativeElement.classList.remove("has-animation-bell");
          this.number.nativeElement.classList.remove("has-animation");
          this.img = "assets/images/croix2.png";
          this.number.nativeElement.style.visibility = "hidden";
          this.display = false;
        } else {
          this.notif = false;
          if (this.isMobile)
            this.canDisplay = true;
          this.bell.nativeElement.classList.add("has-animation-bell");
          this.number.nativeElement.classList.add("has-animation");
          this.img = "assets/images/bell.png";
          this.number.nativeElement.style.visibility = "visible";
          this.display = true;
        }
      }
    }
  }

  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else
    if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
     this.oldIndexA = i;
    } else {
      this.oldIndexA = -1;
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }

  goPerso(perso){
    return `assets/images/HeaderImages/${perso}`;
  }
  cancel(parent: string) {
    if(parent === "add") {
      this.add.nativeElement.classList.remove("is-active");
    } else if (parent === "success")
      this.success.nativeElement.classList.remove("is-active");
    else if (parent === "warning")
      this.warning.nativeElement.classList.remove("is-active");
    else
      this.failure.nativeElement.classList.remove("is-active");
  }

  delete() {
    this.submitButton2.nativeElement.classList.add("is-loading");
    this.userService.removeRelation(this.parentToDelete.relationId).then(() => {
      this.submitButton2.nativeElement.classList.remove()
      this.listParent = this.listParent.filter(parent => parent.relationId !== this.parentToDelete.relationId);
      this.warning.nativeElement.classList.remove("is-active");
      if (this.listParent.length === 0) {
        this.hasNoParent= true;
        this.hasParent = false;
      }
    }).catch(() => {
      this.submitButton2.nativeElement.classList.remove("is-loading");
      this.warning.nativeElement.classList.remove("is-active");
      this.failure.nativeElement.classList.add("is-active");
    });
  }
  openAddParent() {
    this.add.nativeElement.classList.add("is-active");
  }

  acceptParent(parent, index) {
    const body = {
      "id": parent.relationId
    };
    this.userService.acceptInvitation(body).then(() => {
      this.listParent.push({
        "character": parent.character,
        "name": parent.name,
        "relationId": parent.relationId
      });
      this.listAsks = this.listAsks.filter(ask => ask !== parent);
      if (this.listAsks.length === 0) {
        this.img = "assets/images/bell.png";
        this.notif = false;
        if(this.isMobile)  {
          this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        } else {
          this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
          this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed opacity";
        }
      } else {
        this.numberNotif--;
      }
    });
  }
  username = "";
  addParent() {
    this.submitButton.nativeElement.classList.add("is-loading");
    this.userService.sendInvitation(this.addForm.value).then(() => {
      this.submitButton.nativeElement.classList.remove("is-loading");
      this.add.nativeElement.classList.remove("is-active");
      this.username = this.addForm.get("username").value;
      this.success.nativeElement.classList.add("is-active");
    }).catch(() => {
      this.submitButton.nativeElement.classList.remove("is-loading");
      this.failure.nativeElement.classList.add("is-active");
    });
  }

  askDelete(event) {
    this.parentToDelete = {
      "relationId": event.relationId,
      "name": event.name
    }
    this.warning.nativeElement.classList.add("is-active");
  }
  redirect() {
    document.location.reload();
  }

  Submit() {
    this.addForm.patchValue({
      "username": null
    });
    this.failure.nativeElement.classList.remove("is-active");
  }

  declineParent(parent) {
    this.userService.declineInvitation(parent.relationId).then(() => {
      if (this.hasNoParent) {
        this.hasParent = true;
        this.hasNoParent = false;
      }
      this.listAsks = this.listAsks.filter(ask => ask !== parent);
      if (this.listAsks.length === 0) {
        this.img = "assets/images/bell.png";
        this.notif = false;
        if(this.isMobile)  {
          this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        } else {
          this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
          this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed opacity";
        }
      } else {
        this.numberNotif--;
      }
    });
  }
}
