import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbCircuitsComponent } from './admin-db-circuits.component';

describe('AdminDbCircuitsComponent', () => {
  let component: AdminDbCircuitsComponent;
  let fixture: ComponentFixture<AdminDbCircuitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbCircuitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbCircuitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
