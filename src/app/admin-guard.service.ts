import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { UserService } from './services/user.service';
import { server } from "../assets/config";

@Injectable()
export class AdminGuardService implements CanActivate {
    constructor(private router: Router, private http: HttpClient) {
    }
   
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | UrlTree {
      return Promise.resolve(
        this.http.get(`${server}/api/user/is_admin`, {
            observe: 'response',
            withCredentials: true
          }).toPromise()
      ).then(res => {
            if (res["body"]["isAdmin"])
                return true;
            else
            {
                alert("Vous n'êtes pas administrateur. Vous n'avez pas l'autorisation de voir cette page.");
                this.router.navigate(["/accueil"]).then(() => document.location.reload());
                return false;
            }  
        }).catch(() => {
          this.router.navigate(["/accueil"]).then(() => document.location.reload());
          return false;
      });
    }
}