import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { StatsService } from '../services/stats.service';
import { TabItemComponent } from '../tab-item/tab-item.component';
import { AdminDbRoomsComponent } from './admin-db-rooms.component';
import { AdminRoomService } from '../services/admin-room.service';

@NgModule({
    imports:
    [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        AdminDbRoomsComponent
    ],
    declarations:
    [
        AdminDbRoomsComponent,
        TabItemComponent,
    ],
    providers: [StatsService, AdminRoomService]
})
export class AdminDBRoomsModule {}