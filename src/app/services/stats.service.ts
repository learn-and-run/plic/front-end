import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { server } from "../../assets/config";

@Injectable({
  providedIn: 'root',
})

export class StatsService {
  constructor(private http: HttpClient) { }

  public getStats(bodyToSend): Promise<Object> {
    return new Promise((resolve, reject) => {
      this.http.get(`${server}/api/score/module`, {
        withCredentials: true,
        observe: 'response',
        params: {
          "pseudo": bodyToSend.pseudo,
          "moduleType": bodyToSend.module
        }
      }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)
      });
    });
  }

  public getRanking(module): Promise<Object> {
    return new Promise((resolve, reject) => {
      this.http.get(`${server}/api/score/friends`, {
        withCredentials: true,
        observe: 'response',
        params: {
          "moduleType": module
        }
      }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)
      });
    });
  }
}