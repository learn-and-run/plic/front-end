import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDBQuestionsComponent } from './admin-db-questions.component';

describe('AdminDBQuestionsComponent', () => {
  let component: AdminDBQuestionsComponent;
  let fixture: ComponentFixture<AdminDBQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDBQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDBQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
