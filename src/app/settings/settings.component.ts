import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../services/user.service';
import { adminLevels, classItems} from '../app-constant';
import { Router } from '@angular/router';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass']
})
export class SettingsComponent implements OnInit, AfterViewInit {

  constructor(private userService: UserService, private router: Router, public globals: Globals) { }

  // variable for display or not the content of the page
  canDisplay$ = new BehaviorSubject(false);

  // variable to know if the loader is loading or not
  isLoading$ = new BehaviorSubject(true);

  // variable to know if we are in edit mode or not
  change$ = new BehaviorSubject(false);

  // variable to know if the edit informations is displayed or not dependind if the edit password is display or not
  pass$ = new BehaviorSubject(true);

  // variable to change the color of the change password button
  change = "#292f33";

  // object with informations to display in the success popUp
  success = {
    text: "Tes informations ont bien été modifiées !",
    text2: "Paramètres changés",
    img: "../assets/images/settingsCheck.png"
  };

  // Variable to detect changes or modify the element 
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("ButtonPassword", {static: true}) passwordButton: ElementRef;
  @ViewChild("ButtonPassword2") passwordButton2: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton2") submitbutton2: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton4") submitbutton4: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton3") submitbutton3: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton5") submitbutton5: ElementRef<HTMLElement>;
  @ViewChild("PasswordAsking") passwordAsking: ElementRef<HTMLElement>;
  @ViewChild("NewPassword") newPass: ElementRef<HTMLElement>;
  @ViewChild("Email") email: ElementRef<HTMLElement>;
  @ViewChild("Warning") warning: ElementRef<HTMLElement>;
  
  // form for edit informations 
  account = new FormGroup({
    lastname: new FormControl(""),
    firstname: new FormControl(""),
    pseudo: new FormControl(""),
    chosenClass: new FormControl("")
  });

  // fom for edit password
  Password = new FormGroup({
    password: new FormControl(""),
  });

  EmailForm = new FormGroup({
    email: new FormControl(null, [Validators.email])
  });
  
  verify = new FormGroup({
    password: new FormControl(""),
  });


  userTypeList = [
    {
      "fr": "Enfant",
      "en": "student"      
    },
    {
      "fr": "Parent",
      "en": "parent"
    },
    {
      "fr": "Professeur",
      "en": "professor"
    }
  ];
  valid$ = new BehaviorSubject(false);

  
  @ViewChild("Input") input: ElementRef<HTMLInputElement>;

  // variable to know if we are in edit mode or  look mode
  isReadonly = true;

  // variable to have level
  classDb=  adminLevels;

  // variable to handle the displaying
  currentClass = -1;
  classItems = classItems;
  classLevel = -1;
  userType = "Enfant";
  color = "";
  visible = new BehaviorSubject(false);
  textButton = "Changer mon mot de passe";
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.userService.getUserInfo().then(response =>
       {
        const userClass = this.classDb.find(level => level.value === response["body"]["user"].levelType).intValue;
        this.currentClass = this.classItems.findIndex(c => c.value === userClass);
        this.classItems[this.currentClass].activeColor = this.classItems[this.currentClass].color;
        this.classLevel = this.classItems[this.currentClass].text;
        this.color = this.classItems[this.currentClass].color;
        this.userType = response["body"].userType === "STUDENT" ? "Enfant" :  response["body"].userType === "PARENT" ? "Parent" : "Professeur";

        this.account.patchValue({
          email: response["body"]["user"].email,
          lastname: response["body"]["user"].lastname,
          firstname: response["body"]["user"].firstname,
          pseudo: response["body"]["user"].pseudo,
          chosenClass: userClass
        });
       }
    ).then(() => {
      this.isLoading$.next(false);
      this.canDisplay$.next(true);
    });
  }

  getImgUserTupe() {
    if (this.userType === "Enfant") {
      return 'assets/images/child.png';
    } else if (this.userType === "Parent") {
      return "assets/images/parent.png";
    } else {
      return "assets/images/professor.png"
    }
  }

  ChangeClass(id) {
    if(this.currentClass !== -1) {
      this.classItems[this.currentClass].activeColor = "#FFFFFF";
    }
    this.currentClass = id;
    this.classItems[id].activeColor = this.classItems[id].color;
  }

  Enter(id) {
    if (id !== this.currentClass) {
      this.classItems[id].activeColor = this.classItems[id].color;
    }
  }
  
  appear = false;
  pop() {
    this.appear = !this.appear;
  }

  Leave(id) {
    if (id !== this.currentClass) {
      this.classItems[id].activeColor = "#FFFFFF";
    }
  }

  edit(){
    this.isReadonly = false;
  }

  editPassword() {
    // if(this.change$.value) {
    //   this.textButton = "Changer mon mot de passe";
    //   this.change$.next(false);
    //   this.pass$.next(true);
    //   this.change = "#292f33";
    //   this.Password.reset({
    //     "password": "",
    //     "oldPassword": ""
    //   });
    // } else {
    //   this.pass$.next(false);
    //   this.textButton = "Ne pas changer de mot de passe";
    //   this.change$.next(true);
    //   this.change = "#7B2816";
    // }
    this.newPass.nativeElement.classList.add("is-active");
  }
  editEmail() {
    this.email.nativeElement.classList.add("is-active");
  }
  Same = false;
  passwordChanging = false;
  changePassword(){
    if(this.Password.get("oldPassword").value === this.Password.get("password").value) {
      this.Same = true;
    } else {
      this.passwordAsking.nativeElement.classList.add("is-active");
      this.passwordChanging = true;
      this.Same = false;
    }
  }

  // verifyUser() {
  //   if (this.passwordChanging) {
  //     this.userService.login(this.account.get("pseudo").value, this.verify.get("password").value, false).then(response => {
  //     this.passwordAsking.nativeElement.classList.remove("is-active"); 
  //     }).then(() => {
  //     this.userService.updatePassword(this.Password.get("oldPassword").value).then(() => {
  //     this.passwordButton2.nativeElement.classList.add("is-loading");
  //       this.success.img = "assets/images/passwordCheck.png";
  //       this.success.text2 = "Mot de passe réinitialisé !";
  //       if (this.userType === "Enfant")
  //         this.success.text = "Ton mot de passe a bien été changé !";
  //       else
  //         this.success.text = "Votre mot de passe a bien été changé !";
  //       this.Success.nativeElement.classList.add("is-active");
  //     });
  //   });
  //   } else {
  //     this.Submit();
  //   }
  // }

  // Submit() {
  //   const body = {
  //     "pseudo": this.account.get("pseudo").value,
  //     "firstname": this.account.get("firstname").value,
  //     "lastname": this.account.get("lastname").value,
  //     "levelType": this.classDb.find(level => level.intValue === this.account.get("chosenClass").value).value
  //   };
  //   const type = this.userTypeList.find(t => t.fr === this.userType).en;
  //   if (this.account.get("email").dirty) {
  //     this.userService.isFullyAuthenticated().then(response => {
  //       if (response["body"].isFullyAuthenticated) {
  //         this.userService.sendUpdateEmail().then(() => {
  //           this.userService.updateAccount(type, body).then(() => {
  //             if (type !== "student")
  //               this.success.text = "Tes informations ont bien été modifiées ! Un email a été envoyé à ta nouvelle adresse.";
  //             else 
  //               this.success.text = "Vos informations ont bien été modifiées ! Un email a été envoyé à votre nouvelle adresse.";
  //             this.success.text2= "Paramètres changés";
  //             this.success.img = "../assets/images/settingsCheck.png";
  //             this.Success.nativeElement.classList.add("is-active");
  //           });
  //         }).catch(() => {
  //           this.Failure.nativeElement.classList.add("is-active");
  //         });
  //       } else {
  //         this.passwordAsking.nativeElement.classList.add("is-active");
  //         this.passwordChanging = false;
  //       }
  //     }).catch(() => {
  //       this.Failure.nativeElement.classList.add("is-active");
  //     });
  //   } else {
  //     this.userService.updateAccount(type, body).then(() => {
  //       if (type !== "student")
  //         this.success.text = "Tes informations ont bien été modifiées !";
  //       else 
  //         this.success.text = "Vos informations ont bien été modifiées !";
  //       this.success.text2= "Paramètres changés";
  //       this.success.img = "../assets/images/settingsCheck.png";
  //       this.Success.nativeElement.classList.add("is-active");
  //     }).catch(() => this.Failure.nativeElement.classList.add("is-active"));
  //   }
  // }

  tryAgain() {
    this.Failure.nativeElement.classList.remove("is-active");
  }

  cancel(child: string) {
    if (child === "success")
      this.Success.nativeElement.classList.remove("is-active");
    else if (child === "failure")
      this.Failure.nativeElement.classList.remove("is-active");
    else if (child === "passA"){
      this.passwordAsking.nativeElement.classList.remove("is-active");
    } else if (child === "email") {
      this.email.nativeElement.classList.remove("is-active");
    } else if (child === "warning") {
      this.warning.nativeElement.classList.remove("is-active");
    } else {
      this.newPass.nativeElement.classList.remove("is-active");
    }
  }
  
  redirect() {
    if (this.del)
      this.router.navigate(["/accueil"]).then(() => document.location.reload());
    else
      document.location.reload();
  }

  askDelete() {
    this.warning.nativeElement.classList.add("is-active");
  }

  del = false;
  removeAccount() {
    this.submitbutton3.nativeElement.classList.add("is-loading");
    this.userService.deleteAccount().then(() => {
      this.submitbutton3.nativeElement.classList.remove("is-loading");
      this.del = true;
      this.warning.nativeElement.classList.remove("is-active");
      this.success.text = "Ton compte a bien été supprimé. Nous sommes triste de te voir partir 😔. Bon courage !";
      this.success.text2 = "Suppression de compte réussie !";
      this.success.img = "assets/images/poubelle.png";
      this.Success.nativeElement.classList.add("is-active");
    }).catch(() => {
      this.submitbutton3.nativeElement.classList.remove("is-loading");
      this.warning.nativeElement.classList.remove("is-active");
      this.Failure.nativeElement.classList.add("is-active");
    });
  }

  isValid() {
    // const password = this.veri.get("password").value;
    const password = "totot#458";
    var regexp =/[@[ \]^_!"#$%&'()*+,-./:;{}<>=|~?]+/g;
    if (this.checkRegex('[0-9]', password) !== -1
      && this.checkRegex('[a-z]', password) !== -1
      && this.checkRegex('[A-Z]', password) !== -1
      && this.checkRegex(regexp, password) !== -1
      && password.length >= 5 && password.length <= 72)
      return true;
    else 
      return false;
  }
  
  checkRegex(regex, password: string) {
    return password.search(regex);
  }

  loginUser() {
    this.userService.getUserInfo().then(response => {
      const pseudo = response["body"]["user"].pseudo;
      // this.userService.login(pseudo, this.verify.get("password").value, false).then(() =>)
    });
  }
  verifyUser(log?: boolean, action?) {
    // if (log) {

    // }
    // else {
    //   this.userService.isFullyAuthenticated().then(response => {
    //     if (response["body"].isFullyAuthenticated) {
    //       this.doActions(action);
    //     } else {
    //       this.passwordAsking.nativeElement.classList.add("is-active");
    //     }
    //   }).catch(() => {this.Failure.nativeElement.classList.add("is-active");});
    // }

  }

  doActions(action) {
    if (action === "email") {
      this.submitbutton5.nativeElement.classList.add("is-loading");
      this.userService.sendUpdateEmail().then(() => {
        this.submitbutton5.nativeElement.classList.remove("is-loading");
        this.success.text = "Ton email a bien été changé. Un email de confirmation a été envoyé sur ton email : " + this.EmailForm.get("email").value;
        this.success.text2 =  "Changement de mail réussi !";
        this.success.img = "assets/images/send.png";
        this.Success.nativeElement.classList.add("is-active");
      }).catch(() => {
        this.submitbutton5.nativeElement.classList.remove("is-loading");
        this.Failure.nativeElement.classList.add("is-active");
      });
    }
    else {
      this.submitbutton4.nativeElement.classList.add("is-loading");
      this.userService.updatePassword(this.Password.get("password").value).then(() => {
        this.submitbutton4.nativeElement.classList.remove("is-loading");
        this.success.text = "Ton mot de passe a bien été changé.";
        this.success.text2 =  "Changement de mot de passe réussi !";
        this.success.img = "assets/images/passwordCheck.png";
        this.Success.nativeElement.classList.add("is-active");
      }).catch(() => {
        this.submitbutton4.nativeElement.classList.remove("is-loading");
        this.Failure.nativeElement.classList.add("is-active");
      });
    }
  }
}
