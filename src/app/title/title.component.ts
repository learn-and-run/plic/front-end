import { Component, Input, OnInit } from '@angular/core';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.sass']
})
export class TitleComponent implements OnInit {

  constructor(public globals: Globals) { }

  @Input() title: string;
  ngOnInit(): void {
  }

}
