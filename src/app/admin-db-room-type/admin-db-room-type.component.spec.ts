import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbRoomTypeComponent } from './admin-db-room-type.component';

describe('AdminDbRoomTypeComponent', () => {
  let component: AdminDbRoomTypeComponent;
  let fixture: ComponentFixture<AdminDbRoomTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDbRoomTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbRoomTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
