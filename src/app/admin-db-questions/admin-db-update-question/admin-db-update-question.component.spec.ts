import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbUpdateQuestionComponent } from './admin-db-update-question.component';

describe('AdminDbUpdateQuestionComponent', () => {
  let component: AdminDbUpdateQuestionComponent;
  let fixture: ComponentFixture<AdminDbUpdateQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbUpdateQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbUpdateQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
