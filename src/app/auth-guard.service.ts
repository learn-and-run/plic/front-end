import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './services/user.service';
import { server } from "../assets/config";
import { map, catchError } from 'rxjs/operators';
import { trigger } from '@angular/animations';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private router: Router, private http: HttpClient) {
    }
   
    // Check if the user is logged or not or if the user is fully logged or not
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | UrlTree {
        return Promise.resolve(this.http.get(`${server}/api/user/is_auth`, {
            observe: 'response', 
            withCredentials: true
          }).toPromise()).then(res => {
            if (res["body"]["isAuthenticated"])
            {
                return true;
            }
            else {
                alert("Vous n'êtes pas identifié. Identifiez-vous pour atteindre cette page.");
                this.router.navigate(["/accueil"]).then(() => document.location.reload());
                return false;
            }
          }).catch(() => {
            alert("Vous n'êtes pas identifié. Identifiez-vous pour atteindre cette page.");
            this.router.navigate(["/accueil"]).then(() => document.location.reload());
            return false;
          }) ||
          Promise.resolve(this.http.get(`${server}/api/user/is_fully_auth`, {
                observe: 'response',
                withCredentials: true
            }).toPromise()).then(res => {
                if (res["body"]["isFullyAthenticated"])
                    return true;
                else {
                    alert("Vous n'êtes pas identifié. Identifiez-vous pour atteindre cette page.");
                    this.router.navigate(["/accueil"]).then(() => document.location.reload());
                    return false;
                }
            }).catch(() => {
                alert("Vous n'êtes pas identifié. Identifiez-vous pour atteindre cette page.");
                this.router.navigate(["/accueil"]).then(() => document.location.reload());
                return false;
            });
    }
}