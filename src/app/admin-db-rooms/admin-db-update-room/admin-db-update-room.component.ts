import { Location } from "@angular/common";
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { adminLevels, adminModules} from 'src/app/app-constant';
import { AdminQuestionService } from 'src/app/services/admin-question.service';
import { AdminRoomService } from 'src/app/services/admin-room.service';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'update-room',
  templateUrl: './admin-db-update-room.component.html',
  styleUrls: ['./admin-db-update-room.component.sass']
})
export class AdminDbUpdateRoomComponent implements OnInit {

  minutes = [];
  seconds = [];
  listQuestions =  [];
  listQuestionsDisplayed = [];
  listTypeObjects = [];
  listObjects = [];
  listObjectsDisplayed = this.listQuestions;
  question = {};
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  listTypes = [];
  modules = adminModules;
  updateForm: FormGroup = new FormGroup({
    "module": new FormControl(null),
    "type": new FormControl(null),
    "expectedTimeMinutes": new FormControl("00"),
    "expectedTimeSeconds": new FormControl("30"),
    "question": new FormControl(null),
    "id": new FormControl(null)
  });
      
  objectForm = new FormGroup({
    "uuid": new FormControl(null, Validators.required),
    "posX": new FormControl(0, Validators.required),
    "posY": new FormControl(0, Validators.required),
    "posZ": new FormControl(0, Validators.required),
    "rotX": new FormControl(0, Validators.required),
    "rotY": new FormControl(0, Validators.required),
    "rotZ": new FormControl(0, Validators.required),
  });

  modForm = new FormGroup({
    "uuid": new FormControl(null),
    "posX": new FormControl(0),
    "posY": new FormControl(0),
    "posZ": new FormControl(0),
    "rotX": new FormControl(0),
    "rotY": new FormControl(0),
    "rotZ": new FormControl(0),
  });
  currentQuestion;
  currentIndex = -1;
  failure$ = new BehaviorSubject(false);
  textError = "Quelque chose s'est mal passé.";
  @ViewChild("Search") search: ElementRef<HTMLInputElement>;
  @ViewChild("Question") modalQuestion: ElementRef<HTMLInputElement>;
  @ViewChild("SubmitButton") SubmitButton: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton2") SubmitButton2: ElementRef<HTMLElement>;
  @ViewChild("Object") modalObject: ElementRef<HTMLDivElement>;
  @ViewChild("Delete") deleteModal: ElementRef<HTMLDivElement>;
  @ViewChild("Search2") Search2: ElementRef<HTMLInputElement>;

  constructor(private roomService: AdminRoomService, private questionService: AdminQuestionService, private location: Location, public globals: Globals) {
  }

  isMobile = false;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.isOpen)
        this.modalQuestion.nativeElement.classList.remove("is-active");
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      this.isMobile = true;
    }
  }
  ngOnInit() {
    for (var i = 0; i < 60; i+=1) {
      if (i < 10) {
        this.seconds.push("0" + i.toString());
        this.minutes.push("0" + i.toString());
      } else {
        this.minutes.push(i.toString());
        this.seconds.push(i.toString());
      }
    }
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
    const data = JSON.parse(localStorage.getItem("Salle"));
    this.listQuestions = JSON.parse(localStorage.getItem("QuestionsList"));
    this.roomService.getRoomsTypes().then(response => {
      this.listTypes = response["body"]["types"];
    });
    // this.questionService.GetQuestions("TOUS").then(response => {
      
    //   response["body"]["questions"].forEach(question => {
    //     this.listQuestions.push({
    //       "id": question.id,
    //       "name": question.name,
    //       "difficulty": question.difficulty,
    //       "level": question.level,
    //       "explanation": question.explanation,
    //       "question": question.question,
    //       "module": question.module,
    //       "background": "",
    //       "text": "black"
    //     });
    //   });
    // });
    this.listQuestionsDisplayed = this.listQuestions.filter(q => q.module === data.module);
      this.currentQuestion = this.listQuestionsDisplayed.find(qu => qu.id === data.questionId);
  
      this.currentIndex = this.listQuestionsDisplayed.indexOf(this.currentQuestion);
  
      this.listQuestionsDisplayed[this.currentIndex].background = "#5a5a5a";
    
      this.listQuestionsDisplayed[this.currentIndex].text = "white";
      this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_white.png";
    
      // this.question = {
      //   "explication": this.currentQuestion.explanation,
      //   "question": this.currentQuestion.question,
      //   "difficulty": this.currentQuestion.difficulty,
      //   "level": this.levels.find(l => l.value === this.currentQuestion.level).text
      // };
      this.questionInfo(this.currentQuestion);
      this.getObjects(data.id);
      const time = this.convertTime(data.expectedTime);
      let formatMinutes = "";
      let formatSeconds = "";
      if (time.minutes < 10) {
        formatMinutes += "0" + time.minutes.toString();
      } else {
        formatMinutes += time.minutes.toString();
      } 
      if (time.seconds < 10) {
        formatSeconds += "0" + time.seconds.toString();
      } else {
        formatSeconds += time.seconds.toString();
      }
      this.updateForm.patchValue({
        "module": data.module,
        "type": data.typeUuid,
        "expectedTimeMinutes": formatMinutes,
        "expectedTimeSeconds": formatSeconds,
        "question": this.currentQuestion.id,
        "id": data.id
      });
  }
  getObjects(id) {
    this.roomService.getObjects().then(response => {
      response["body"]["objects"].forEach(elt =>{
        if (elt.inGame) {
          this.listTypeObjects.push({
            "uuid": elt.uuid,
            "name": elt.name
          });
        }
      });
    }).then(() => {
      this.roomService.getObjectsByRoom(id).then(response => {
        console.log("HELLO", response);
        response["body"]["objects"].forEach(elt =>{ this.listObjects.push({
          "uuid": elt.uuid,
          "position": elt.position,
          "rotation": elt.rotation
        });
        this.listObjectsDisplayed = this.listObjects;
        }
        );
      })
    }).then(() => {
      this.loading = false;
      this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);
    });
  }

  levels= adminLevels;

  questionFromModule  = [];
  questionLinkByRoom  = [];

  convertTime(time: number) {
    let timer = time / 1000;
    let minutes = Math.trunc(timer / 60);
    let seconds = Math.trunc(timer % 60);
    return {"seconds": seconds, "minutes": minutes};
  }

  index = 0;
  isIn = false;
  TimeToMiliseconds(minutesString: number, secondsString: number) {
    const minutes = +minutesString;
    const seconds = +secondsString;
    if (minutes !== null) {
      const minutesToSeconds = minutes * 60;
      const addSeconds = minutesToSeconds + seconds;
      const convert = addSeconds * 1000;
      return convert;
    } else {
      return seconds * 1000;
    }
  }

  
  changeList() {
    this.question = {};
    this.isIn = false;

    if (this.currentIndex !== -1) {
      this.listQuestionsDisplayed[this.currentIndex].background = "";
      this.listQuestionsDisplayed[this.currentIndex].text = "black";
      this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_grey.png";
      this.currentIndex = -1;
    }
    this.listQuestionsDisplayed = this.listQuestions.filter(question => {
      return (question.module === this.updateForm.get("module").value);
    });
    if (this.listQuestionsDisplayed.find(x => x === this.currentQuestion)) {
      this.index = this.listQuestionsDisplayed.indexOf(this.currentQuestion);
      // this.question = {
      //   "explication": this.currentQuestion.explanation,
      //   "question": this.currentQuestion.question,
      //   "difficulty": this.currentQuestion.difficulty,
      //   "level": this.levels.find(l => l.value === this.currentQuestion.level).text
      // };
      this.questionInfo(this.currentQuestion);
      this.isIn = true;
      this.listQuestionsDisplayed[this.index].background = "#5a5a5a";
      this.listQuestionsDisplayed[this.index].text = "white";
      this.listQuestionsDisplayed[this.index].img = "assets/images/info_white.png";

    }
    this.refresh();
  }

  back() {
    this.location.back();
  }

  updateRoom() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const time = this.TimeToMiliseconds(this.updateForm.get("expectedTimeMinutes").value, this.updateForm.get("expectedTimeSeconds").value);
    const body = {
      "id": this.updateForm.get("id").value,
      "questionId": this.updateForm.get("question").value,
      "module": this.updateForm.get("module").value,
      "typeUuid": this.updateForm.get("type").value,
      "expectedTime": time
    };
    const onSuccess = (response) => {
     
      const body = {
        "roomId": response["body"]["id"],
        "objects": this.listObjects
      };
      this.roomService.updateObjectInRoom(body).then(() => {
        this.SubmitButton.nativeElement.classList.remove("is-loading");
        this.location.back();
      });
    };
    const onFailure = () => {
      this.failure$.next(true);
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      setTimeout(() => this.failure$.next(false), 500);
      clearTimeout();

    }
    this.roomService.updateRoom(body).then(onSuccess).catch(onFailure);
  }
  delete() {
    this.failure$.next(false);
    this.SubmitButton.nativeElement.classList.remove("is-loading");
  }

  chooseQuestion(id) {
    // Re init default color 
    if (this.currentIndex !==  -1) {
      this.listQuestionsDisplayed[this.currentIndex].background = "";
      this.listQuestionsDisplayed[this.currentIndex].text = "black";
      this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_grey.png";
    }
    if (this.isIn) {
      this.listQuestionsDisplayed[this.index].background = "";
      this.listQuestionsDisplayed[this.index].text = "black";
      this.listQuestionsDisplayed[this.index].img = "assets/images/info_grey.png";
    }
    this.currentIndex = id;
    this.listQuestionsDisplayed[this.currentIndex].text = "white";
    this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_white.png";
    this.listQuestionsDisplayed[this.currentIndex].background = "#5a5a5a";
    
    const info = this.listQuestionsDisplayed[id];

    this.updateForm.patchValue({
      "question": info.id
    });

    // this.question = {
    //   "explication": info.explanation,
    //   "question": info.question,
    //   "difficulty": info.difficulty,
    //   "level": this.levels.find(l => l.value === info.level).text
    // };
    this.questionInfo(info);
  }

  questionInfo(elt) {
    this.question = {
      "explication": elt.explanation,
      "question": elt.question,
      "difficulty": elt.difficulty,
      "level": this.levels.find(l => l.value === elt.level).text
    };
  }
  text = "";
  isOpen = false;
  openModal(elt) {
    this.isOpen = true;
    this.questionInfo(elt);
    this.text = elt.name;
    this.modalQuestion.nativeElement.classList.add("is-active");
  }

  close(modal) {
    if (modal === "question")
      this.modalQuestion.nativeElement.classList.remove("is-active");
    else if (modal === "object")
      this.modalObject.nativeElement.classList.remove("is-active");
    else
      this.deleteModal.nativeElement.classList.remove("is-active");
  }

  refresh() {
    const wordToLowerCase = this.search.nativeElement.value.toLowerCase();
    // check if the tabs clicked is null
    if (this.updateForm.get("module").value === null) {
      // Simple search
      this.listQuestionsDisplayed= this.listQuestions.filter(question=> {
        const questionLowerCase = question.name.toLowerCase();
        return questionLowerCase.includes(wordToLowerCase);
      });
    } else {
      // filtering room by the module clicked and the search word
      this.listQuestionsDisplayed = this.listQuestions.filter(question => {
        if (question.module === this.updateForm.get("module").value) {
          const roomLowerCase = question.name.toLowerCase();
          return roomLowerCase.includes(wordToLowerCase);
        }
      });
    }
  }

  refreshObj() {
    const wordToLowerCase = this.Search2.nativeElement.value.toLowerCase();
      this.listObjectsDisplayed = this.listObjects.filter(obj=> {
        const objLowerCase = obj.name.toLowerCase();
        return objLowerCase.includes(wordToLowerCase);
      });
  }

  objectToModify = 0;
  openModifyObject(elt, i) {
    this.objectToModify = i;
    this.modForm.patchValue({
      "uuid": elt.uuid,
      "posX": elt.position.x,
      "posY": elt.position.y,
      "posZ": elt.position.z,
      "rotX": elt.rotation.x,
      "rotY": elt.rotation.y,
      "rotZ": elt.rotation.z
    });
    this.modalObject.nativeElement.classList.add("is-active");
  }

  indexObj = 0;
  askDelete(i) {
    this.indexObj = i;
    this.deleteModal.nativeElement.classList.add("is-active");
  }

  addObject() {
    const object = {
      "uuid": this.objectForm.get("uuid").value,
      "position": {
        "x": this.objectForm.get("posX").value,
        "y": this.objectForm.get("posY").value,
        "z": this.objectForm.get("posZ").value
      },
      "rotation": {
        "x": this.objectForm.get("rotX").value,
        "y": this.objectForm.get("rotY").value,
        "z": this.objectForm.get("rotZ").value
      }
    };
    this.objectForm.patchValue({
      "uuid": null,
      "posX": 0,
      "posY": 0,
      "posZ": 0,
      "rotX": 0,
      "rotY": 0,
      "rotZ": 0,
    });

    this.listObjects.push(object);
    this.listObjectsDisplayed = this.listObjects;
  }

  modify() {
    this.listObjects[this.objectToModify].uuid = this.modForm.get("uuid").value;
    this.listObjects[this.objectToModify].position.x = this.modForm.get("posX").value;
    this.listObjects[this.objectToModify].position.y = this.modForm.get("posY").value;
    this.listObjects[this.objectToModify].position.z = this.modForm.get("posZ").value;
    this.listObjects[this.objectToModify].rotation.x = this.modForm.get("rotX").value;
    this.listObjects[this.objectToModify].rotation.y = this.modForm.get("rotY").value;
    this.listObjects[this.objectToModify].rotation.z = this.modForm.get("rotZ").value;
    this.listObjectsDisplayed = this.listObjects;
    this.modalObject.nativeElement.classList.remove("is-active");
  }

  getElt(elt) {
    const word = this.listTypeObjects.find(e => e.uuid === elt.uuid);
    return word.name;
  }

  cancel() {
    this.deleteModal.nativeElement.classList.remove("is-active");
  }

  deleteObject() {
    this.listObjects.splice(this.index, 1);
    this.listObjectsDisplayed = this.listObjects;
    this.deleteModal.nativeElement.classList.remove("is-active");
  }
}
