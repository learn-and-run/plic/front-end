import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { classItems, adminLevels } from '../app-constant';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.sass']
})

export class CreateAccountComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, public globals: Globals) { }

  isChild$ = new BehaviorSubject(true);
  text = "Création d'un compte";
  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild('Hola', {static: false}) hola: ElementRef<HTMLDivElement>;
  @ViewChild("SubmitButton") SubmitButton: ElementRef<HTMLElement>;

  private readonly imgPath = "../assets/images/";
  childImg = `${this.imgPath}checkChild.png`;
  parentImg = `${this.imgPath}parent.png`;
  eyeImg = `${this.imgPath}eye.png`;
  inputType = "password";
  type = "Enfant";
  currentClass = -1;
  valid$ = new BehaviorSubject(false);

  classItems = classItems;

  account = new FormGroup({
    email: new FormControl("", Validators.required),
    lastname: new FormControl("", Validators.required),
    firstname: new FormControl("", Validators.required),
    pseudo: new FormControl("", Validators.required),
    chosenClass: new FormControl(""),
    password: new FormControl("", Validators.required)
  });

  ngOnInit() {
  }

  typeEnfant = true;
  Change() {
    this.typeEnfant = !this.typeEnfant;
    this.isChild$.next(this.typeEnfant);
    if (this.typeEnfant) {
      this.childImg = `${this.imgPath}checkChild.png`;
      this.parentImg = `${this.imgPath}parent.png`;
      this.type = "Enfant";
    } else {
      this.childImg = `${this.imgPath}child.png`;
      this.parentImg = `${this.imgPath}checkParent.png`;
      this.type = "Parent";
    }
  }
  display = false;
  test() {
    this.display = !this.display;
    if (this.display) {
      this.hola.nativeElement.classList.add('is-active');
    } else {
      this.hola.nativeElement.classList.remove('is-active');
    }
  }

  isValid() {
    const password = this.account.get("password").value;
    var regexp =/[@[ \]^_!"#$%&'()*+,-./:;{}<>=|~?]+/g;
    if (this.checkRegex('[0-9]', password) !== -1
      && this.checkRegex('[a-z]', password) !== -1
      && this.checkRegex('[A-Z]', password) !== -1
      && this.checkRegex(regexp, password) !== -1
      && password.length >= 5 && password.length <= 72 && this.account.status === "VALID")
      this.valid$.next(true);
    else 
      this.valid$.next(false);
  }

  validForm() {
    this.isValid();
    if (this.typeEnfant && this.account.get("chosenClass").value !== "" && this.valid$.value) {
      return true;
    }
    if (!this.typeEnfant && this.account.valid && this.valid$.value)
    {
      return true;
    }
    return false;
  }
  ChangeClass(id) {
    if(this.currentClass !== -1) {
      this.classItems[this.currentClass].activeColor = "#FFFFFF";
    }
    this.currentClass = id;
    this.classItems[id].activeColor = this.classItems[id].color;
  }

  Enter(id) {
    if (id !== this.currentClass) {
      this.classItems[id].activeColor = this.classItems[id].color;
    }
  }

  Leave(id) {
    if (id !== this.currentClass) {
      this.classItems[id].activeColor = "#FFFFFF";
    }
  }

  cancel(child: string) {
    if (child === "success")
      this.Success.nativeElement.classList.remove("is-active");
    else
      this.Failure.nativeElement.classList.remove("is-active");
  }

  checkRegex(regex, password: string) {
    return password.search(regex);
  }

  redirect() {
    this.router.navigate(['/accueil']).then(() => document.location.reload());
  }

  Submit() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const onSuccess = () => {
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      this.Success.nativeElement.classList.add("is-active");
    };

    const onFailed = () => {
      this.SubmitButton.nativeElement.classList.add("is-loading");
      this.Failure.nativeElement.classList.add("is-active");
    };

    const body = {
      "pseudo": this.account.get("pseudo").value,
      "firstname": this.account.get("firstname").value,
      "lastname": this.account.get("lastname").value,
      "password": this.account.get("password").value,
      "email": this.account.get("email").value,
    };

    if (this.typeEnfant)
      body['levelType'] = adminLevels[this.currentClass].value;
    
    this.userService.createUser(body, this.typeEnfant ? 'student' : 'parent')
                    .then(onSuccess)
                    .catch(onFailed);      
  }
}
