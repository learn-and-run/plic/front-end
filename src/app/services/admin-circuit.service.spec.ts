import { TestBed } from '@angular/core/testing';
import { AdminCircuitService } from './admin-circuit.service';


describe('AdminCircuitService', () => {
  let service: AdminCircuitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminCircuitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
