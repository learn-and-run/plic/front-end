import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { adminModules } from 'src/app/app-constant';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminRoomService } from 'src/app/services/admin-room.service';
import { AdminQuestionService } from 'src/app/services/admin-question.service';
import { Location } from '@angular/common';
import { AdminCircuitService } from 'src/app/services/admin-circuit.service';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'modify-circuit',
  templateUrl: './admin-db-update-circuit.component.html',
  styleUrls: ['./admin-db-update-circuit.component.sass']
})
export class AdminDbUpdateCircuitComponent implements OnInit {

  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  listRooms = [];
  idCircuit = 0;
  display$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  failure = false;
  listRoomsDisplayed = [];
  scenes = [];
  modules = adminModules;
  updateForm = new FormGroup({
    "name": new FormControl("", Validators.required),
    "pictureName": new FormControl("", Validators.required),
    "module": new FormControl("GENERAL", Validators.required),
    "scene_uuid": new FormControl(null, Validators.required)
  });
  @ViewChild("SubmitButton") SubmitButton: ElementRef;
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("Rooms", {static: false}) rooms: ElementRef<HTMLDivElement>;
  preview = "";
  nameImg = "";
  constructor(private roomService: AdminRoomService, private circuitService: AdminCircuitService, private questionService: AdminQuestionService, private location: Location, public globals: Globals) { 
    this.modules.push({
      text: "Général",
      value: "GENERAL"
    }
    );
  }
  listRoomsChecked = [];
  errorText = "Chargement des questions impossible pour le moment."
  isMobile = false;

  questions = [];
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
  
    this.getGestions();
  }

  getGestions() {
    const data = JSON.parse(localStorage.getItem("Circuit"));
    this.questionService.GetQuestions("TOUS").then(response => {
      response["body"]["questions"].forEach(question => {
        this.questions.push({
          "id": question.id,
          "name": question.name,
        });
      });
    }).then(() =>  {
      this.roomService.getRooms("TOUS").then(response => {
      response["body"]["rooms"].forEach(room => {
        const question = this.questions.find(q => q.id === room.questionId);
        this.listRooms.push(
          {
            "roomId": room.id,
            "time": this.getFormatedTime(room.expectedTime),
            "module": room.module,
            "name": question.name,
            "background": "",
            "text": "black",
            "isCheck": false
          });
        this.listRoomsDisplayed = this.listRooms;
        // const questionInData = this.questions.find(q => q.id === )
      });
      this.checkRooms(data.id);
      this.idCircuit = data.id;
      this.updateForm.patchValue({
        "module": data.module,
        "name": data.name,
        "scene_uuid": data.scene_uuid
      });
      this.nameImg = data.pictureName + ".png";
    }).then(() => {
      this.circuitService.getScenesTypes().then(response => {
        response["body"]["scenes"].forEach(elt => {
          this.scenes.push({
            "uuid": elt.uuid,
            "name": elt.name
          });
        });
      }).catch(() => {
        this.errorText = "Chargement des scènes impossible pour le moment.";
        this.error$.next(true);
      });
    }).catch(() => {
      this.loading = false;
      this.errorText = "Chargement des salles impossible pour le moment."
      this.error$.next(true);
    });
  }).then(() => {
    this.loading = false;
    this.display$.next(true);
  }).catch(() => {
    this.loading = false;
    this.errorText = "Chargement des questions impossible pour le moment."

    this.error$.next(true);});
  }
  checkRooms(id: number) {
    this.roomService.associatedCircuit(id).then(response => {
      response["body"]["rooms"].forEach(room => {
        const r = this.listRoomsDisplayed.find(r => r.roomId === room.id);
        if (r !== undefined)
        {
          this.listRoomsChecked.push(r);
          r.text = "white";
          r.background = "#5a5a5a";
          r.isCheck = true;
        }
      });
    });
  }
  convertTime(time: number) {
    let timer = time / 1000;
    let minutes = Math.trunc(timer / 60);
    let seconds = Math.trunc(timer % 60);
    return {"seconds": seconds, "minutes": minutes};
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.isOpen){
        this.isOpen = false;
        this.rooms.nativeElement.classList.remove("is-active");       
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      this.isMobile = true;
    }
  }

  getFormatedTime(timeMiliseconds) {
    const time = this.convertTime(timeMiliseconds);
    let formatedTime = "";
    if (time.minutes < 10)
      formatedTime += "0" + time.minutes;
    else
      formatedTime += time.minutes;
    formatedTime += ":";
    if (time.seconds < 10) {
      formatedTime += "0" + time.seconds;
    } else {
      formatedTime += time.seconds;
    }
    return formatedTime;
  }
  previewImg = false;
  upload(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.preview = reader.result as string;
        this.previewImg = true;
      };
    }
    this.nameImg = this.updateForm.get("pictureName").value.substring(12);
  }

  chooseQuestion(id) {
    // Re init default color 
    if (this.listRoomsDisplayed[id].text !== "black") {
      this.listRoomsDisplayed[id].text = "black";
      this.listRoomsDisplayed[id].background = "";
      this.listRoomsDisplayed[id].isCheck = false;
    } else {
      this.listRoomsDisplayed[id].text = "white";
      this.listRoomsDisplayed[id].background = "#5a5a5a";
      this.listRoomsDisplayed[id].isCheck = true;
    }
  }

  refresh() {
    const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
    // check if the tabs clicked is null
    if (this.updateForm.get("module").value === "GENERAL") {
      // Simple search
      this.listRoomsDisplayed= this.listRooms.filter(question=> {
        const questionLowerCase = question.name.toLowerCase();
        return questionLowerCase.includes(wordToLowerCase);
      });
    } else {
      // filtering room by the module clicked and the search word
      this.listRoomsDisplayed = this.listRooms.filter(question => {
        if (question.module === this.updateForm.get("module").value) {
          const roomLowerCase = question.name.toLowerCase();
          return roomLowerCase.includes(wordToLowerCase);
        }
      });
    }
  }


  refactoList() {
    this.listRoomsDisplayed.forEach(room => {
      const r = this.listRoomsChecked.find(s => s.roomId === room.roomId);
      if (r === undefined) {
        room.text = "red";
        room.background = "";
        room.text = "black";
        room.isCheck = false;
      }
    });
    if (this.updateForm.get("module").value === "GENERAL") {
      this.listRoomsDisplayed = this.listRooms;
    } else {
      this.listRoomsDisplayed = this.listRooms.filter(question => {
        return (question.module === this.updateForm.get("module").value);
      });
    }
    this.refresh();
  }

  updateCircuit() {
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const old = this.nameImg.split('.');
    const body = {
      "id": this.idCircuit,
      "module": this.updateForm.get("module").value,
      "name": this.updateForm.get("name").value,
      "pictureName": old[0],
      "scene_uuid": this.updateForm.get("scene_uuid").value
    };
    if (this.updateForm.get("pictureName").value !== "") {
      const picture = this.updateForm.get("pictureName").value.substring(12);
      const namePicture = picture.split('.');
      body.pictureName = namePicture[0]
    };
    const link = this.listRoomsDisplayed.filter(r => r.isCheck);
    const listIds= [];

    link.forEach(link => {
      listIds.push(link.roomId)
    });
    const body2 = {
      "circuitId": this.idCircuit,
      "roomIds": listIds
    };

    this.circuitService.UpdateCircuit(body).then(() => {
      this.circuitService.updateRooms(body2).then(() => {
        this.location.back();
      }).catch(() => {
        this.failure = true;
        this.SubmitButton.nativeElement.classList.remove("is-loading");
        setTimeout(() => this.failure = false, 500);
        clearTimeout();
      });
    }).catch(() => {
      this.failure = true;
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      setTimeout(() => this.failure = false, 500);
      clearTimeout();
    });
  }

  fail() {
    this.failure = false;
  }
  back() {
    this.location.back();
  }
  isOpen = false;
  openModal() {
    this.isOpen = true;
    this.rooms.nativeElement.classList.add("is-active");
  }

  close() {
    this.isOpen = false;
    this.rooms.nativeElement.classList.remove("is-active");
  }
  error() {
    this.error$.next(false);
  }
}
