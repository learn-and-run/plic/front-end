import { Component, OnInit, Input, HostListener } from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.sass']
})
export class LoaderComponent implements OnInit {

  constructor() { }
  @Input() textLoader: string;
  isMobile = false;
  ngOnInit() {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
    else {
      this.isMobile = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
   
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      this.isMobile = true;
    }
  }

}
