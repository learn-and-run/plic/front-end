import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'drop-item',
  templateUrl: './drop-item.component.html',
  styleUrls: ['./drop-item.component.sass']
})
export class DropItemComponent implements OnInit {

  @Input() text: string;
  @Input() img: string;
  constructor() { }

  ngOnInit(): void {
  }

  getImg() {
    return `assets/images/HeaderImages/${this.img}`;
  }
}
