import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbCreateRoomComponent } from './admin-db-create-room.component';

describe('AdminDbCreateRoomComponent', () => {
  let component: AdminDbCreateRoomComponent;
  let fixture: ComponentFixture<AdminDbCreateRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbCreateRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbCreateRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
