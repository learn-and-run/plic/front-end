import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-validate-email',
  templateUrl: './validate-email.component.html',
  styleUrls: ['./validate-email.component.sass']
})
export class ValidateEmailComponent implements OnInit {
  @ViewChild("Success", {static: true}) Success: ElementRef;
  @ViewChild("Failure", {static: true}) Failure: ElementRef;
  @ViewChild("ActiveButton") ActiveButton: ElementRef<HTMLElement>;
  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, public globals: Globals) { }
  userId: string;

  ngOnInit() {
    this.userId = this.route.snapshot.params['id'];
  }

  validateEmail() {
    this.ActiveButton.nativeElement.classList.add("is-loading");
    this.userService.validateEmail(this.userId).then(() => {
      this.ActiveButton.nativeElement.classList.remove("is-loading");
      this.Success.nativeElement.classList.add("is-active");
    }).catch(() => {
      this.ActiveButton.nativeElement.classList.remove("is-loading");
      this.Failure.nativeElement.classList.add("is-active");
    });
  }

  cancel(child: string) {
    if (child === "success")
      this.Success.nativeElement.classList.remove("is-active");
    else
      this.Failure.nativeElement.classList.remove("is-active");
  }

  redirect() {
    this.router.navigate(['/accueil']).then(() => document.location.reload());
  }
}
