import {Component, OnInit} from '@angular/core';
import {Globals} from '../../assets/Globals';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  constructor(public globals: Globals) { }

  ngOnInit() { }

  public getImg(): string {
    return '../assets/images/HeaderImages/LogoLearnRunWhite671x671.png';
  }
}
