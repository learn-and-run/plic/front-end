import { Component, OnInit, ViewChild, ElementRef, HostListener, ViewChildren } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { timeStamp } from 'console';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.sass']
})
export class FriendsComponent implements OnInit {
  constructor(private userService: UserService) { }
  listFriends = [];
  hasFriends = false;
  hasNoFriends = false;
  isMobile =  false;
  @ViewChild("Add") add: ElementRef<HTMLElement>;
  @ViewChild("Success") success: ElementRef<HTMLElement>;
  @ViewChild("Failure") failure: ElementRef<HTMLElement>;
  @ViewChild("Warning") warning: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton") submitButton: ElementRef<HTMLElement>;
  @ViewChild("SubmitButton2") submitButton2: ElementRef<HTMLElement>;
  @ViewChild("Number") number: ElementRef<HTMLSpanElement>;
  @ViewChild("Bell") bell: ElementRef<HTMLImageElement>;
  @ViewChildren("Tpt") drop: ElementRef<HTMLDivElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  friendToDelete = {
    "relationId": "",
    "name": ""
  };
  listAsks = [];
  listTests = [
    {
      character: "toto",
      name: "titi",
    },
    {
      character: "toto",
      name: "titi",
    },
    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },    {
      character: "toto",
      name: "titi",
    },
  ];
  
  addForm = new FormGroup({
    "username": new FormControl(null, Validators.required)
  });
  numberNotif = 0;
  ngOnInit() {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
    this.userService.getRelation("STUDENT").then(response => {
      response["body"]["relations"].forEach(friend => {
        this.listFriends.push({
          "character": friend["user"]["character"].name + ".png",
          "name": friend["user"].pseudo,
          "relationId": friend.id
        });
      });
    }).then(() =>  {
      this.userService.pendingInvitation().then(response => {
        response["body"]["pendingInvitations"].forEach(demand => {
          if (demand.userType !== "PARENT") {
            this.listAsks.push({
              "relationId": demand.id,
              "name": demand["user"].pseudo,
              "character": demand["user"]["character"].name + ".png",
              "visibility": "visible",
            });
          }
        });
        if (this.listAsks.length !== 0) {
          this.numberNotif = this.listAsks.length;
          if (this.isMobile) {
            this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile has-animation-bell";
            this.number.nativeElement.className = "notif has-left-margin notif-size-mobile has-animation";
          } else {
            this.bell.nativeElement.className = "has-left-margin is-bell has-animation-bell";
            this.number.nativeElement.className = "notif notif-size-desktop has-left-margin has-animation";
          }
          this.number.nativeElement.style.visibility = "visible";
        } else {
          if(this.isMobile)  {
            this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          } else {
            this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed";
          }
          this.number.nativeElement.style.visibility = "hidden";
        }
      })
      if (this.listFriends.length === 0) {
        this.hasNoFriends = true;
      } else {
        this.hasFriends = true;
      }
    });
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      this.canDisplay = true;
      if (this.numberNotif !== 0 && !this.notif) {
        this.bell.nativeElement.className = "has-left-margin is-bell has-animation-bell";
        this.number.nativeElement.className = "notif notif-size-desktop has-left-margin has-animation";
      }
      if (this.notif) {
        this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed";
        this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if(this.notif) {
        this.canDisplay = false;
        this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
      }
      if(this.numberNotif !== 0 && !this.notif) {
        this.number.nativeElement.className = "notif has-left-margin notif-size-mobile has-animation";
        this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile has-animation-bell";
      }
      this.isMobile = true;
    }
  }

  goPerso(perso){
    return `assets/images/HeaderImages/${perso}`;
  }

  goStatsfriend(name) {
    return `/statistiques/${name}`;
  }
  notif = false;
  display = true;
  img = "assets/images/bell.png";
  canDisplay = true;
  openNotif() {
    if (this.listAsks.length !== 0) {
      if(!this.isMobile) {
        this.canDisplay = true;
      }
      if (this.display) {
        this.notif = true;
        if (this.isMobile)
          this.canDisplay = false;
        this.bell.nativeElement.classList.remove("has-animation-bell");
        this.number.nativeElement.classList.remove("has-animation");
        this.img = "assets/images/croix2.png";
        this.number.nativeElement.style.visibility = "hidden";
        this.display = false;
      } else {
        this.notif = false;
        if (this.isMobile)
          this.canDisplay = true;
        this.bell.nativeElement.classList.add("has-animation-bell");
        this.number.nativeElement.classList.add("has-animation");
        this.img = "assets/images/bell.png";
        this.number.nativeElement.style.visibility = "visible";
        this.display = true;
      }
    }
  }

  cancel(friend: string) {
    if(friend === "add") {
      this.add.nativeElement.classList.remove("is-active");
    } else if (friend === "success")
   { this.success.nativeElement.classList.remove("is-active");
    document.location.reload();}
    else if (friend === "warning")
      this.warning.nativeElement.classList.remove("is-active");
    else
      this.failure.nativeElement.classList.remove("is-active");
  }

  // askDelete(event) {
  //   this.friendToDelete = {
  //     "relationId": event.relationId,
  //     "name": event.name
  //   }
  //   this.warning.nativeElement.classList.add("is-active");
  // }
  askDelete(friend) {
    this.friendToDelete = {
      "relationId": friend.relationId,
      "name": friend.name
    };
    this.warning.nativeElement.classList.add("is-active");
  }

  oldIndex = -1;
  open(i){
    if (this.oldIndex === -1) {
      this.oldIndex = i;
      this.drop["_results"][i].nativeElement.classList.add("is-active");
    } else
    if (i !== this.oldIndex) {
      this.drop["_results"][this.oldIndex].nativeElement.classList.remove("is-active");
      this.drop["_results"][i].nativeElement.classList.add("is-active");
      this.oldIndex = i;
    } else {
      this.oldIndexA = -1;
      this.drop["_results"][i].nativeElement.classList.remove("is-active");
    }
  }


  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else
    if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
     this.oldIndexA = i;
    } else {
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }
  delete() {
    this.submitButton2.nativeElement.classList.add("is-loading");
    this.userService.removeRelation(this.friendToDelete.relationId).then(() => {
      this.submitButton2.nativeElement.classList.remove()
      this.listFriends = this.listFriends.filter(friend => friend.relationId !== this.friendToDelete.relationId);
      this.warning.nativeElement.classList.remove("is-active");
      if (this.listFriends.length === 0) {
        this.hasNoFriends = true;
        this.hasFriends = false;
      }
    }).catch(() => {
      this.submitButton2.nativeElement.classList.remove("is-loading");
      this.warning.nativeElement.classList.remove("is-active");
      this.failure.nativeElement.classList.add("is-active");
    });
  }
  openAddFriend() {
    this.add.nativeElement.classList.add("is-active");
  }

  acceptFriend(friend) {
    const body = {
      "id": friend.relationId
    };
    this.userService.acceptInvitation(body).then(() => {
      if (this.hasNoFriends) {
        this.hasFriends = true;
        this.hasNoFriends = false;
      }
      this.listFriends.push({
        "character": friend.character,
        "name": friend.name,
        "relationId": friend.relationId
      });
      this.listAsks = this.listAsks.filter(ask => ask !== friend);
      if (this.listAsks.length === 0) {
        this.img = "assets/images/bell.png";
        this.notif = false;
        if(this.isMobile)  {
          this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        } else {
          this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
          this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed opacity";
        }
        this.number.nativeElement.style.visibility = "hidden";
      } else {
        this.numberNotif--;
      }
    });
  }


  declineFriend(friend) {
    this.userService.declineInvitation(friend.relationId).then(() => {
      if (this.hasNoFriends) {
        this.hasFriends = true;
        this.hasNoFriends = false;
      }
      this.listAsks = this.listAsks.filter(ask => ask !== friend);
      if (this.listAsks.length === 0) {
        this.img = "assets/images/bell.png";
        this.notif = false;
        if(this.isMobile)  {
          this.bell.nativeElement.className = "has-left-margin is-bell size-bell-mobile opacity";
          this.number.nativeElement.className = "notif has-left-margin notif-size-mobile";
        } else {
          this.number.nativeElement.className = "notif notif-size-desktop has-left-margin";
          this.bell.nativeElement.className = "has-left-margin is-bell cursor-not-allowed opacity";
        }
      } else {
        this.numberNotif--;
      }
    });
  }

  username = "";
  addFriend() {
    this.submitButton.nativeElement.classList.add("is-loading");
    this.userService.sendInvitation(this.addForm.value).then(() => {
      this.submitButton.nativeElement.classList.remove("is-loading");
      this.add.nativeElement.classList.remove("is-active");
      this.username = this.addForm.get("username").value;
      this.success.nativeElement.classList.add("is-active");
    }).catch(() => {
      this.submitButton.nativeElement.classList.remove("is-loading");
      this.failure.nativeElement.classList.add("is-active");
    });
  }

  redirect() {
    document.location.reload();
  }

  Submit() {
    this.addForm.patchValue({
      "username": null
    });
    this.failure.nativeElement.classList.remove("is-active");
  }
}
