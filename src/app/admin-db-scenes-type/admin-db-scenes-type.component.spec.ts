import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbScenesTypeComponent } from './admin-db-scenes-type.component';

describe('AdminDbScenesTypeComponent', () => {
  let component: AdminDbScenesTypeComponent;
  let fixture: ComponentFixture<AdminDbScenesTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDbScenesTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbScenesTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
