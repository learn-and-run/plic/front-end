import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbCreateQuestionComponent } from './admin-db-create-question.component';

describe('AdminDbCreateQuestionComponent', () => {
  let component: AdminDbCreateQuestionComponent;
  let fixture: ComponentFixture<AdminDbCreateQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDbCreateQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbCreateQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
