import { Location } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { adminLevels, adminModules } from 'src/app/app-constant';
import { AdminRoomService } from 'src/app/services/admin-room.service';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'create-room',
  templateUrl: './admin-db-create-room.component.html',
  styleUrls: ['./admin-db-create-room.component.sass']
})
export class AdminDbCreateRoomComponent implements OnInit {
  
  // Questions
  listQuestions =  [];
  listQuestionsDisplayed = [];
  listTypeObjects = [];
  listObjects = [];
  listObjectsDisplayed = [];
  failure$ = new BehaviorSubject(false);
  textError = "Quelque chose s'est mal passé.";
  
  levels= adminLevels;
  listTypes = [];
  question = {
  }
  
  minutes = [];
  seconds = [];
  
  modules = adminModules;
  currentIndex = -1;
  @ViewChild("SubmitButton") SubmitButton: ElementRef<HTMLElement>;
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  @ViewChild("Search2") Search2: ElementRef<HTMLInputElement>;
  @ViewChild("Question") modalQuestion: ElementRef<HTMLDivElement>;
  @ViewChild("Object") modalObject: ElementRef<HTMLDivElement>;
  @ViewChild("Delete") deleteModal: ElementRef<HTMLDivElement>;

  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  createForm = new FormGroup({
    "module": new FormControl(null, Validators.required),
    "type": new FormControl(null, Validators.required),
    "questionId": new FormControl(null, Validators.required),
    "expectedTimeSeconds": new FormControl("30"),
    "expectedTimeMinutes": new FormControl("00")
  });
  
  objectForm = new FormGroup({
    "uuid": new FormControl(null, Validators.required),
    "posX": new FormControl(0, Validators.required),
    "posY": new FormControl(0, Validators.required),
    "posZ": new FormControl(0, Validators.required),
    "rotX": new FormControl(0, Validators.required),
    "rotY": new FormControl(0, Validators.required),
    "rotZ": new FormControl(0, Validators.required),
  });

  modForm = new FormGroup({
    "uuid": new FormControl(null),
    "posX": new FormControl(0),
    "posY": new FormControl(0),
    "posZ": new FormControl(0),
    "rotX": new FormControl(0),
    "rotY": new FormControl(0),
    "rotZ": new FormControl(0),
  });

  constructor(private roomService: AdminRoomService, private location: Location, public globals: Globals) {
    this.getObjects();
    // this.questionService.GetQuestions("TOUS").then(response => {
    //   response["body"]["questions"].forEach(question => {
    //     this.listQuestions.push({
    //       "id": question.id,
    //       "name": question.name,
    //       "difficulty": question.difficulty,
    //       "level": question.level,
    //       "explanation": question.explanation,
    //       "question": question.question,
    //       "module": question.module,
    //       "background": "",
    //       "text": "black"
    //     });
    //   });
    //   this.listQuestionsDisplayed = this.listQuestions;
    // });
    this.listQuestions = JSON.parse(localStorage.getItem("QuestionsList"));
  }
  isMobile = false;
  isOpen = false;

  getObjects() {
    this.roomService.getObjects().then(response => {
      response["body"]["objects"].forEach(elt =>{
        if (elt.inGame) {
          this.listTypeObjects.push({
            "uuid": elt.uuid,
            "name": elt.name
          });
        }
      });
    }).then(() => {
      this.roomService.getRoomsTypes().then(response => {
        this.listTypes = response["body"]["types"];
      });
    }).then(() => {
      this.loading = false;
      this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);
    });
  }

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   if (window.innerWidth >= 1024 && this.isMobile) {
  //     if (this.isOpen)
  //       this.modalQuestion.nativeElement.classList.remove("is-active");
  //     this.isMobile = false;
  //   } else if (window.innerWidth < 1024 && !this.isMobile) {
  //     this.isMobile = true;
  //   }
  // }

  ngOnInit() {
    for (var i = 0; i < 60; i+=1) {
      if (i < 10) {
        this.seconds.push("0" + i.toString());
        this.minutes.push("0" + i.toString());
      } else {
        this.minutes.push(i.toString());
        this.seconds.push(i.toString());
      }
    }
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
  }

  changeList() {

    this.question = {};

    if (this.currentIndex !== -1) {
      this.listQuestionsDisplayed[this.currentIndex].background = "";
      this.listQuestionsDisplayed[this.currentIndex].text = "black";
      this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_grey.png";
      this.currentIndex = -1;
    }

    this.listQuestionsDisplayed = this.listQuestions.filter(question => {
      return (question.module === this.createForm.get("module").value);
    });

    this.refresh();
  }

  back() {
    this.location.back();
  }

  createRoom() {
    const miliseconds = this.TimeToMiliseconds(this.createForm.get("expectedTimeMinutes").value, this.createForm.get("expectedTimeSeconds").value);
    this.SubmitButton.nativeElement.classList.add("is-loading");
    const body = {
      "questionId": this.createForm.get("questionId").value,
      "expectedTime": miliseconds,
      "module": this.createForm.get("module").value,
      "typeUuid": this.createForm.get("type").value
    };

    this.roomService.CreateRoom(body).then(response => {
      if (this.listObjects.length !== 0) {
        const body = {
          "roomId": response["body"].id,
          "objects": this.listObjects
        }
        console.log("body", body);
        this.roomService.updateObjectInRoom(body).then(() => {
          this.SubmitButton.nativeElement.classList.remove("is-loading");
          this.location.back();
        }).catch(() => {
          this.textError = "Quelque chose s'est mal passé.";
          this.failure$.next(true);
          setTimeout(() => this.failure$.next(false), 500);
          clearTimeout();
        });
      }
    }).catch(response => {
      this.SubmitButton.nativeElement.classList.remove("is-loading");
      if (response["error"].status === 409) {
        this.textError = "La salle est déjà créée."
      } else {
        this.textError = "Quelque chose s'est mal passé.";
      }
      this.failure$.next(true);
      setTimeout(() => this.failure$.next(false), 500);
      clearTimeout();
    });
  }

  delete() {
    this.failure$.next(false);
    this.SubmitButton.nativeElement.classList.remove("is-loading");
  }

  chooseQuestion(id) {
    // Re init default color 
    if (this.currentIndex !==  -1) {
      this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_grey.png";
      this.listQuestionsDisplayed[this.currentIndex].background = "";
      this.listQuestionsDisplayed[this.currentIndex].text = "black";
    }

    this.currentIndex = id;
    this.listQuestionsDisplayed[this.currentIndex].img = "assets/images/info_white.png";
    this.listQuestionsDisplayed[this.currentIndex].text = "white";
    this.listQuestionsDisplayed[this.currentIndex].background = "#5a5a5a";
    
    const info = this.listQuestionsDisplayed[id];

    this.createForm.patchValue({
      "questionId": info.id
    });

    this.question = {
      "explication": info.explanation,
      "question": info.question,
      "difficulty": info.difficulty,
      "level": this.levels.find(l => l.value === info.level).text
    };
  }

  refresh() {
    const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
    // check if the tabs clicked is null
    if (this.createForm.get("module").value === null) {
      // Simple search
      this.listQuestionsDisplayed= this.listQuestions.filter(question=> {
        const questionLowerCase = question.name.toLowerCase();
        return questionLowerCase.includes(wordToLowerCase);
      });
    } else {
      // filtering room by the module clicked and the search word
      this.listQuestionsDisplayed = this.listQuestions.filter(question => {
        if (question.module === this.createForm.get("module").value) {
          const roomLowerCase = question.name.toLowerCase();
          return roomLowerCase.includes(wordToLowerCase);
        }
      });
    }
  }

  refreshObj() {
    const wordToLowerCase = this.Search2.nativeElement.value.toLowerCase();
      this.listObjectsDisplayed = this.listObjects.filter(obj=> {
        const objLowerCase = obj.name.toLowerCase();
        return objLowerCase.includes(wordToLowerCase);
      });
  }

  TimeToMiliseconds(minutesString: string, secondsString: string) {
    const minutes = +minutesString;
    const seconds = +secondsString;
    if (minutes !== 0) {
      const minutesToSeconds = minutes * 60;
      const addSeconds = minutesToSeconds + seconds;
      const convert = addSeconds * 1000;
      return convert;
    } else {
      return seconds * 1000;
    }
  }

  text = "";
  openModal(elt) {
    // this.isOpen = true;
    this.question = {
      "explication": elt.explanation,
      "question": elt.question,
      "difficulty": elt.difficulty,
      "level": this.levels.find(l => l.value === elt.level).text
    };
    this.text = elt.name;
    this.modalQuestion.nativeElement.classList.add("is-active");
  }

  objectToModify = 0;
  openModifyObject(elt, i) {
    this.objectToModify = i;
    this.modForm.patchValue({
      "uuid": elt.uuid,
      "posX": elt.position.x,
      "posY": elt.position.y,
      "posZ": elt.position.z,
      "rotX": elt.rotation.x,
      "rotY": elt.rotation.y,
      "rotZ": elt.rotation.z
    });
    this.modalObject.nativeElement.classList.add("is-active");
  }

  close(modal) {
    if (modal === "question")
      this.modalQuestion.nativeElement.classList.remove("is-active");
    else if (modal === "object")
      this.modalObject.nativeElement.classList.remove("is-active");
    else
      this.deleteModal.nativeElement.classList.remove("is-active");
  }

  index = 0;
  askDelete(i) {
    this.index = i;
    this.deleteModal.nativeElement.classList.add("is-active");
  }

  addObject() {
    const object = {
      "uuid": this.objectForm.get("uuid").value,
      "position": {
        "x": this.objectForm.get("posX").value,
        "y": this.objectForm.get("posY").value,
        "z": this.objectForm.get("posZ").value
      },
      "rotation": {
        "x": this.objectForm.get("rotX").value,
        "y": this.objectForm.get("rotY").value,
        "z": this.objectForm.get("rotZ").value
      }
    };
    this.objectForm.patchValue({
      "uuid": null,
      "posX": 0,
      "posY": 0,
      "posZ": 0,
      "rotX": 0,
      "rotY": 0,
      "rotZ": 0,
    });

    this.listObjects.push(object);
    this.listObjectsDisplayed = this.listObjects;
  }

  modify() {
    this.listObjects[this.objectToModify].uuid = this.modForm.get("uuid").value;
    this.listObjects[this.objectToModify].position.x = this.modForm.get("posX").value;
    this.listObjects[this.objectToModify].position.y = this.modForm.get("posY").value;
    this.listObjects[this.objectToModify].position.z = this.modForm.get("posZ").value;
    this.listObjects[this.objectToModify].rotation.x = this.modForm.get("rotX").value;
    this.listObjects[this.objectToModify].rotation.y = this.modForm.get("rotY").value;
    this.listObjects[this.objectToModify].rotation.z = this.modForm.get("rotZ").value;
    this.listObjectsDisplayed = this.listObjects;
    this.modalObject.nativeElement.classList.remove("is-active");
  }

  getElt(elt) {
    const word = this.listTypeObjects.find(e => e.uuid === elt.uuid);
    return word.name;
  }

  cancel() {
    this.deleteModal.nativeElement.classList.remove("is-active");
  }

  deleteObject() {
    this.listObjects.splice(this.index, 1);
    this.listObjectsDisplayed = this.listObjects;
    this.deleteModal.nativeElement.classList.remove("is-active");
  }
}
