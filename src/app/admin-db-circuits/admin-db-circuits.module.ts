import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AdminDbCircuitsComponent } from './admin-db-circuits.component';
import { StatsService } from '../services/stats.service';
import { TabItemComponent } from '../tab-item/tab-item.component';
import { AdminDbUpdateCircuitComponent } from './admin-db-update-circuit/admin-db-update-circuit.component';
import { AdminDbCreateCircuitComponent } from './admin-db-create-circuit/admin-db-create-circuit.component';
import { AdminCircuitService } from '../services/admin-circuit.service';
@NgModule({
    imports:
    [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        AdminDbCircuitsComponent
    ],
    declarations:
    [
        TabItemComponent,
        AdminDbCircuitsComponent,
        AdminDbUpdateCircuitComponent,
        AdminDbCreateCircuitComponent,
    ],
    providers: [StatsService, AdminCircuitService]
})
export class AdminDBCircuitsModule {}