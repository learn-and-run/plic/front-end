import { HttpClient, HttpHeaders } from '@angular/common/http';
import { server } from "../../assets/config";
import { Injectable, resolveForwardRef } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})

export class AdminRoomService {
  constructor(private http: HttpService) { }

  urlEncodedHeader = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
  jsonHeader = new HttpHeaders({'Content-Type': 'application/json'});

  /**
   * Create room
   */
  public CreateRoom(dataToSend): Promise<Object> {
    return this.http.post(`${server}/api/room/create`, dataToSend);
  }

  /**
   * Get rooms by module
   */
  public getRooms(module): Promise<Object> {
    return this.http.get(`${server}/api/room/all/${module}`);
  }

  /**
   * Update room
   */
  public updateRoom(object): Promise<Object> {
    return this.http.patch(`${server}/api/room/update`, object);
  }

  /**
   * Delete room
   */
  public deleteRoom(id): Promise<Object> {
    return this.http.delete(`${server}/api/room/delete/${id}`);
  }

  public associatedCircuit(id: number): Promise<Object> { 
    return this.http.get(`${server}/api/room/circuit/${id}`);
  }

  /**
   * Get roomTypes
   */
  public getRoomsTypes(): Promise<Object> {
    return this.http.get(`${server}/api/roomType/all`);
  }

  /**
   * Create room type
   */
  public createRoomType(body): Promise<Object> {
    return this.http.post(`${server}/api/roomType/create`, body);
  }

  /**
   * Update room type
   */
  public updateRoomType(body): Promise<Object> {
    return this.http.patch(`${server}/api/roomType/update`, body);
  }

  /**
   * Delete room type
   */
  public deleteRoomType(uuid): Promise<Object> {
    return this.http.delete(`${server}/api/roomType/delete/${uuid}`);
  }

  /**
   * Get objects
   */
  public getObjects(): Promise<Object> {
    return this.http.get(`${server}/api/object/all`);
  }

  /**
   * Create object
   */
  public createObject(bodyToSend): Promise<Object> {
    return this.http.post(`${server}/api/object/create`, bodyToSend);
  }

  /**
   * Update object
   */
  public updateObject(bodyToSend): Promise<Object> {
    return this.http.patch(`${server}/api/object/update`, bodyToSend);
  }

  /**
   * Delete object
   */
  public deleteObject(uuid): Promise<Object> {
    return this.http.delete(`${server}/api/object/delete/${uuid}`);
  }

  /**
   * Get object in the room
   */
  public getObjectsByRoom(roomId): Promise<Object> {
    return this.http.get(`${server}/api/room/object/all/${roomId}`);
  }

  /**
   * update object in the room
   */
  public updateObjectInRoom(bodyToSend): Promise<Object> {
    return this.http.patch(`${server}/api/room/object/update`, bodyToSend);
  }
}