import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DifferentAComponent } from './different-a.component';

describe('DifferentAComponent', () => {
  let component: DifferentAComponent;
  let fixture: ComponentFixture<DifferentAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DifferentAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DifferentAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
