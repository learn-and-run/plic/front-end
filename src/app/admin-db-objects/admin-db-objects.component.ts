import { Component, ElementRef, HostListener, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Globals } from 'src/assets/Globals';
import { AdminRoomService } from '../services/admin-room.service';

@Component({
  selector: 'app-admin-db-objects',
  templateUrl: './admin-db-objects.component.html',
  styleUrls: ['./admin-db-objects.component.sass']
})
export class AdminDbObjectsComponent implements OnInit {
  @ViewChild("Delete", {static: false}) deleteModal: ElementRef<HTMLDivElement>;
  @ViewChild("Modify", {static: false}) modifyModal: ElementRef<HTMLDivElement>;
  @ViewChild("Add", {static: false}) addModal: ElementRef<HTMLDivElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  @ViewChild("SubmitButton") modifyButton: ElementRef<HTMLButtonElement>;
  @ViewChild("SubmitButton2") addButton: ElementRef<HTMLButtonElement>;
  listObjects = [];
  listObjectsDisplayed = this.listObjects;
  canDisplay$ = new BehaviorSubject(true);
  error$ = new BehaviorSubject(false);
  loading = true;
  failure = false;
  createForm = new FormGroup({
    "name": new FormControl(null, Validators.required),
    "folder": new FormControl(null, Validators.required),
    "inGame": new FormControl(false, Validators.required)
  });

  modifyForm = new FormGroup({
    "uuid": new FormControl(null),
    "name": new FormControl(null, Validators.required),
    "folder": new FormControl(null, Validators.required),
    "inGame": new FormControl(false, Validators.required)
  });

  constructor(public globals: Globals, private roomService: AdminRoomService) { }

  isMobile = false;
  ngOnInit() {
    console.log("init");
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
    this.getObjects();
  }

  fail() {
    this.error$.next(false);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.ask)
        this.deleteModal.nativeElement.classList.remove("is-active");
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.ask)
        this.deleteModal.nativeElement.classList.add("is-active");      
      this.isMobile = true;
    }
  }

  getObjects() {
    console.log("this");
    this.roomService.getObjects().then(response => {
      response["body"]["objects"].forEach(elt => {this.listObjects.push(elt)});
      this.loading = false;
     this.canDisplay$.next(true);
    }).catch(() => {
      this.loading = false;
      this.error$.next(true);
    });
  }

  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
      this.oldIndexA = i;
    } else {
      this.oldIndexA = -1;
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }
  text = "";
  openModal() {
    this.addModal.nativeElement.classList.add("is-active");
  }
  goModify(elt) {
    this.text = elt.name;
    this.modifyForm.patchValue({
      "uuid": elt.uuid,
      "name": elt.name,
      "folder": elt.folder,
      "inGame": elt.inGame
    });
    this.modifyModal.nativeElement.classList.add("is-active");
  }
  refresh() {
  const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
  this.listObjectsDisplayed=  this.listObjects.filter(elt => {
    const name = elt.name.toLowerCase();
    return name.includes(wordToLowerCase);
  });
  }

  ask = false;
  currentIndex = 0;
  askDelete(id)
  {
    this.ask = true;
    this.currentIndex = id;
    if (this.globals.isMobile)
      this.deleteModal.nativeElement.classList.add("is-active");
    this.text = this.listObjectsDisplayed[id].name;
    this.listObjectsDisplayed[id].confirm = true;
    this.listObjectsDisplayed[id].error = false;
    this.listObjectsDisplayed[id].cancel = true;
  }

  close(modal) {
    if (modal === "delete") {
      this.deleteModal.nativeElement.classList.remove("is-active");
       this.listObjectsDisplayed[this.currentIndex].confirm = false;
       this.listObjectsDisplayed[this.currentIndex].cancel = false;
    } else if (modal === "add") {
      this.addModal.nativeElement.classList.remove("is-active");
      this.createForm.patchValue({
        "name": null,
        "folder": null,
        "inGame": false
      });
    } else {
      this.modifyModal.nativeElement.classList.remove("is-active");
      this.modifyForm.patchValue({
        "uuid": null,
        "name": null,
        "folder": null,
        "inGame": false
      });
    }
  }

  delete(id) {
    this.ask = false;
    const objectToRemove = this.listObjectsDisplayed[id];
    this.roomService.deleteObject(objectToRemove.uuid).then(() => {
      this.listObjects = this.listObjects.filter(type => type.uuid !== objectToRemove.uuid);
      this.deleteModal.nativeElement.classList.remove("is-active");
      this.refresh();
    }).catch(err => {
      console.log(err);
      this.listObjectsDisplayed[id].error = true;
      this.deleteModal.nativeElement.classList.remove("is-active");
      this.listObjectsDisplayed[id].confirm = false;
      this.listObjectsDisplayed[id].cancel = false;
    });
  }

  cancel(id) {
    this.ask = false;
    if (this.globals.isMobile) {
      this.deleteModal.nativeElement.classList.remove("is-active");
    }
    this.listObjectsDisplayed[id].confirm = false;
    this.listObjectsDisplayed[id].cancel = false;
  }
  private regex = '^([a-zA-Z]+/)+$';
  isValid(modal) {
    if (modal === "create") {
      const type = this.createForm.get("folder").value;
      if (type !== null)
        return type.search(this.regex) !== -1 ? true : false;
      return false;
    } else {
      const type = this.modifyForm.get("folder").value;
      if (type !== null)
        return type.search(this.regex) !== -1 ? true : false;
      return false;
    }
  }
  submitAddForm() {
    this.addButton.nativeElement.classList.add("is-loading");
      const body = {
        "name": this.createForm.get("name").value,
        "folder": this.createForm.get("folder").value,
        "inGame": this.createForm.get("inGame").value
      };
      this.roomService.createObject(body).then(response => {
        this.listObjects.push(response["body"]);
        this.listObjectsDisplayed = this.listObjects;
        this.createForm.patchValue({
          "name": null,
          "folder": null,
          "inGame": false
        });
        this.addModal.nativeElement.classList.remove("is-active");
        this.addButton.nativeElement.classList.remove("is-loading");

      }).catch(() => {this.failure = true; 
        this.addButton.nativeElement.classList.remove("is-loading");
        setTimeout(() => this.failure = false, 500);
        clearTimeout();
      });
        
  }
  submitUpdateForm() {
    this.modifyButton.nativeElement.classList.add("is-loading");
    const body = {
      "uuid": this.modifyForm.get("uuid").value,
      "name": this.modifyForm.get("name").value,
      "folder": this.modifyForm.get("folder").value,
      "inGame": this.modifyForm.get("inGame").value
    };
    this.roomService.updateObject(body).then(response => {
      const index = this.listObjects.findIndex(elt => elt.uuid === response["body"].uuid);
      this.listObjects[index].name = response["body"].name;
      this.listObjects[index].folder = response["body"].folder;
      this.listObjects[index].inGame = response["body"].inGame;
      this.modifyForm.patchValue({
        "uuid": null,
        "name": null,
        "folder": null,
        "inGame": false
      });
      this.modifyModal.nativeElement.classList.remove("is-active");
      this.modifyButton.nativeElement.classList.remove("is-loading");
    }).catch(() => { this.failure = true; 
      this.modifyButton.nativeElement.classList.remove("is-loading");
      setTimeout(() => this.failure = false, 500);});
  }
}
