import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDbObjectsComponent } from './admin-db-objects.component';

describe('AdminDbObjectsComponent', () => {
  let component: AdminDbObjectsComponent;
  let fixture: ComponentFixture<AdminDbObjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDbObjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDbObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
