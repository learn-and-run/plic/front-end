import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { adminLevels, adminModules } from 'src/app/app-constant';
import { AdminQuestionService } from 'src/app/services/admin-question.service';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'update-question',
  templateUrl: './admin-db-update-question.component.html',
  styleUrls: ['./admin-db-update-question.component.sass']
})
export class AdminDbUpdateQuestionComponent implements OnInit {

  @ViewChild("SubmitButton") button: ElementRef<HTMLButtonElement>;
  adminForm  = new FormGroup({
    "name": new FormControl(null),
    "question": new FormControl(null),
    "answer": new FormControl(null),
    "badAnswer": new FormControl(null),
    "level": new FormControl(null),
    "module": new FormControl(null),
    "clue1": new FormControl(""),
    "clue2": new FormControl(""),
    "clue3": new FormControl(""),
    "difficulty": new FormControl(0),
    "explanation": new FormControl("")
  });
  id= "";
  clue1 = false;
  clue2 = false;
  clue3 = false;
  failure= new BehaviorSubject(false);
  levels = adminLevels;
  counter = 0;
  difficulty = [0, 1, 2, 3, 4, 5];
  modules = adminModules;
  constructor(private questionsService: AdminQuestionService, private location: Location,  private route: ActivatedRoute, public globals: Globals) { }

  ngOnInit() {
    const questionInfo = JSON.parse(localStorage.getItem("Question"));
    this.id = questionInfo.id;
    this.adminForm.patchValue({
      "name": questionInfo.name,
      "question": questionInfo.question,
      "answer": questionInfo.response,
      "badAnswer": questionInfo.badAnswer,
      "level": this.levels.find(l => l.value === questionInfo.level).value,
      "module": this.modules.find(m => m.value === questionInfo.module).value,
      "clue1": questionInfo.clue1,
      "clue2": questionInfo.clue2,
      "clue3": questionInfo.clue3,
      "difficulty": questionInfo.difficulty,
      "explanation": questionInfo.explanation
    });
    if (this.adminForm.get("clue1").value !== "" && this.adminForm.get("clue1").value !== null) {
      this.clue1 = true;
      this.counter += 1;
    }
    if (this.adminForm.get("clue2").value !== ""  && this.adminForm.get("clue2").value !== null) { 
      this.clue2 = true;
      this.counter += 1;
    }
    if (this.adminForm.get("clue3").value !== "" && this.adminForm.get("clue3").value !== null) {
      this.clue3 = true;
      this.counter += 1;
    }
    if (this.counter === 3) {
      this.display = true;
    }
  }

  display = false;
  addClue() {
    if (this.counter < 3) {
      this.counter++;
      if (this.counter === 1)
        this.clue1 = true;
      if (this.counter === 2)
        this.clue2 = true;
      if (this.counter === 3)
       { this.clue3 = true; 
      this.display = true;}

    }
  }
  removeClue() {
    if (this.counter > 0) {
      if (this.counter === 3)
      {
        this.adminForm.patchValue({
          "clue3": ""
        });
        this.clue3 =false;}
      if (this.counter === 2){
        this.adminForm.patchValue({
          "clue2": ""
        });
        this.clue2 = false;}
      if (this.counter === 1){
        this.adminForm.patchValue({
          "clue1": ""
        });
        this.clue1 = false;
      }
      this.counter--;
      this.display =false;
    }
  }

  back() {
    this.location.back();
  }

  submitForm() {
    this.button.nativeElement.classList.add("is-loading");
    const body = {
      "id": this.id,
      "name": this.adminForm.get("name").value,
      "question": this.adminForm.get("question").value,
      "response": this.adminForm.get("answer").value,
      "badAnswer": this.adminForm.get("badAnswer").value,
      "level": this.adminForm.get("level").value,
      "module": this.adminForm.get("module").value,
      "clue1": this.adminForm.get("clue1").value,
      "clue2": this.adminForm.get("clue2").value,
      "clue3": this.adminForm.get("clue3").value,
      "difficulty": this.adminForm.get("difficulty").value,
      "explanation": this.adminForm.get("explanation").value
    };
    this.questionsService.UpdateQuestion(body).then(() => {
      this.button.nativeElement.classList.remove("is-loading");
      this.location.back();
    }).catch(() => {
      this.button.nativeElement.classList.remove("is-loading");
      this.failure.next(true);
      setTimeout(() => this.failure.next(false), 500);
      clearTimeout();
    });
  }

  fail() {
    this.failure.next(false);
  }
}
