import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../services/user.service';
import { TabItemComponent } from '../tab-item/tab-item.component';
import { LoaderComponent } from '../loader/loader.component';
import { StatsService } from '../services/stats.service';

@NgModule({
    imports:
    [
        FormsModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [
    ],
    declarations:
    [
        TabItemComponent,
        LoaderComponent
    ],
    providers: [UserService, StatsService]
})
export class StatsModule {}