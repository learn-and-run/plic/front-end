import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tab-item',
  templateUrl: './tab-item.component.html',
  styleUrls: ['./tab-item.component.sass']
})
export class TabItemComponent implements OnInit {

  constructor() { }

  @Input() backgroundColor: string;
  @Input() matiere: string;
  @Input() opacity: number;
  @Input() icon: string;
  @Input() idModule: number;
  @Input() currentModule: number;
  @Input() path: string;

  ngOnInit() {
  }

  getImg() {
    return this.path + this.icon;
  }
  
  Enter() {
    this.opacity = 1;
  }

  Leave() {
    if (this.idModule !== this.currentModule) {
      this.opacity = 0.5;
    }
  }
}
