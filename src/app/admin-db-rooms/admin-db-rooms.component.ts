import { Component, OnInit, ElementRef, ViewChild, HostListener, ViewChildren } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AdminRoomService } from '../services/admin-room.service';
import { adminModules } from '../app-constant';
import { AdminQuestionService } from '../services/admin-question.service';
import { timeStamp } from 'console';
import { Globals } from 'src/assets/Globals';

@Component({
  selector: 'app-admin-db-rooms',
  templateUrl: './admin-db-rooms.component.html',
  styleUrls: ['./admin-db-rooms.component.sass']
})
export class AdminDbRoomsComponent implements OnInit {

  listRooms = [];
  textError = "Chargement des salles impossible pour le moment.";
  listRoomsDisplayed = this.listRooms.sort();
  @ViewChild("Rooms", {static: false}) rooms: ElementRef<HTMLDivElement>;
  @ViewChild('Hola', {static: false}) hola: ElementRef<HTMLDivElement>;
  @ViewChildren("DropNotif") dropNotif: ElementRef<HTMLDivElement>;
  @ViewChild("Search") Search: ElementRef<HTMLInputElement>;
  currentModule = 0;
  textColor = "#b63a6b";
  borderColor = "#b63a6b";
  testModules = [
    {
      item: "Tous",
      itemValue: "TOUS",
      textColor: "#b63a6b",
      current: "#b63a6b"
    },
    {
      item: "Mathématiques",
      itemValue: "MATHS",
      textColor: "#51c47f",
      current: "#dbdbdb"
    },
    {
      item: "Français",
      itemValue: "FRANCAIS",
      textColor: "#1c7cac",
      current: "#dbdbdb",
    },
    {
      item: "Physique",
      itemValue: "PHYSIQUE",
      textColor: "#d37f50",
      current: "#dbdbdb"

    },
    {
      item: "Chimie",
      itemValue: "CHIMIE",
      textColor: "#db983d",
      current: "#dbdbdb"

    },
    {
      item: "Anglais",
      itemValue: "ANGLAIS",
      textColor: "#cd444b",
      current: "#dbdbdb"

    },
    {
      item: "Histoire",
      itemValue: "HISTOIRE",
      textColor: "#ce80b2",
      current: "#dbdbdb"

    },
    {
      item: "Géographie",
      itemValue: "GEOGRAPHIE",
      textColor: "#a5619e",
      current: "#dbdbdb"
    }
  ];
  modules = adminModules;
  questions = [];
  canDisplay$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  loading = true;
  constructor(private roomService: AdminRoomService, private questionService: AdminQuestionService, public globals: Globals) {
    this.getRooms();
  }
  isMobile = false;
  ngOnInit() {
    if (window.innerWidth >= 1024) {
      this.isMobile = false;
    } else if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
  }

  fail() {
    this.error$.next(false);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.ask)
        this.rooms.nativeElement.classList.remove("is-active");
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.ask)
        this.rooms.nativeElement.classList.add("is-active");      
      this.isMobile = true;
    }
  }
  convertTime(time: number) {
    let timer = time / 1000;
    let minutes = Math.trunc(timer / 60);
    let seconds = Math.trunc(timer % 60);
    return {"seconds": seconds, "minutes": minutes};
  }
  getRooms() {
    this.questionService.GetQuestions("TOUS").then(response => {
      response["body"]["questions"].forEach(question => {
        this.questions.push({
          "id": question.id,
          "name": question.name,
          "difficulty": question.difficulty,
          "level": question.level,
          "explanation": question.explanation,
          "question": question.question,
          "module": question.module,
          "background": "",
          "text": "black",
          "img": "assets/images/info_grey.png"
        });
      });
    }).then(() =>  {this.roomService.getRooms("TOUS").then(response => {
      response["body"]["rooms"].forEach(room => {
        const question = this.questions.find(q => q.id === room.questionId);
     
        this.listRooms.push(
          {
            room,
            question,
            "confirm": false,
            "cancel": false,
            "error": false
          });
      });
      localStorage.setItem("QuestionsList", JSON.stringify(this.questions));
    }).catch(() => {
      this.loading = false;
      this.textError = "Chargement des questions impossible pour le moment.";
      this.error$.next(true);
    })
  }).then(() => {
    this.loading = false;
    this.canDisplay$.next(true);
  }).catch(() => {
    this.loading = false;
    this.textError = "Chargement des salles impossible pour le moment.";

    this.error$.next(true);
  })
  }

  oldIndexA = -1;
  openActions(i){
    if (this.oldIndexA === -1) {
      this.oldIndexA = i;
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
    } else if (i !== this.oldIndexA) {
      this.dropNotif["_results"][this.oldIndexA].nativeElement.classList.remove("is-active");
      this.dropNotif["_results"][i].nativeElement.classList.add("is-active");
      this.oldIndexA = i;
    } else {
      this.oldIndexA = -1;
      this.dropNotif["_results"][i].nativeElement.classList.remove("is-active");
    }
  }

  getFormatedTime(timeMiliseconds) {
    const time = this.convertTime(timeMiliseconds);
    let formatedTime = "";
    if (time.minutes < 10)
      formatedTime += "0" + time.minutes;
    else
      formatedTime += time.minutes;
    formatedTime += ":";
    if (time.seconds < 10) {
      formatedTime += "0" + time.seconds;
    } else {
      formatedTime += time.seconds;
    }
    return formatedTime;
  }

  currentColor = "#b63a6b";
  change(element, id) {
    if (this.currentModule !== id) {
      this.testModules[this.currentModule].current = "#dbdbdb";
      this.currentModule = id;
      this.currentColor = this.testModules[id].textColor;
      this.testModules[id].current = this.testModules[id].textColor;
      this.refresh();
    }
  }

  refresh() {
      const wordToLowerCase = this.Search.nativeElement.value.toLowerCase();
      // check if the tabs clicked is TOUS
      if (this.testModules[this.currentModule].itemValue === "TOUS") {
        // Simple search
        this.listRoomsDisplayed= this.listRooms.filter(room => {
          const roomLowerCase = room.question.name.toLowerCase();
          return roomLowerCase.includes(wordToLowerCase);
        });
      } else {
        // filtering room by the module clicked and the search word
        this.listRoomsDisplayed = this.listRooms.filter(room => {
          if (room.room.module === this.testModules[this.currentModule].itemValue) {
            const roomLowerCase = room.question.name.toLowerCase();
            return roomLowerCase.includes(wordToLowerCase);
          }
        });
      }
  }

  goModify(room) {
    localStorage.setItem("Salle", JSON.stringify(room.room));
  }
  ask = false;
  text = "";
  currentIndex = 0;
  askDelete(id)
  {
    this.ask = true;
    this.currentIndex = id;
    if (this.globals.isMobile)
      this.rooms.nativeElement.classList.add("is-active");
    this.text = this.listRoomsDisplayed[id].question.name;
    this.listRoomsDisplayed[id].confirm = true;
    this.listRoomsDisplayed[id].error = false;
    this.listRoomsDisplayed[id].cancel = true;
  }

  close() {
    this.rooms.nativeElement.classList.remove("is-active");
    this.listRoomsDisplayed[this.currentIndex].confirm = false;
    this.listRoomsDisplayed[this.currentIndex].cancel = false;
  }

  delete(id) {
    this.ask = false;
    const roomToRemove = this.listRoomsDisplayed[id];
    this.roomService.deleteRoom(roomToRemove.room.id).then(() => {
      this.listRooms = this.listRooms.filter(room => room.room.id !== roomToRemove.room.id);
      this.refresh();
    }).catch(() => {
      this.listRoomsDisplayed[id].error = true;
      this.listRoomsDisplayed[id].confirm = false;
      this.listRoomsDisplayed[id].cancel = false;
    })
  }

  cancel(id) {
    this.ask = false;
    console.log("ID: ", id);
    if (this.globals.isMobile) {
      this.rooms.nativeElement.classList.remove("is-active");
    }
    this.listRoomsDisplayed[id].confirm = false;
    this.listRoomsDisplayed[id].cancel = false;
  }


  display = false;
  test() {
    this.display = !this.display;
    if (this.display) {
      this.hola.nativeElement.classList.add('is-active');
    } else {
      this.hola.nativeElement.classList.remove('is-active');
    }
  }

}
