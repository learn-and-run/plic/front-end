import { Component, ElementRef, HostListener, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Location } from '@angular/common';
import { modules } from '../app-constant';
import { StatsService } from '../services/stats.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.sass']
})
export class RankingComponent implements OnInit {

  constructor(private location: Location, private statService: StatsService) { }
  modules = modules;
  infos = [];
  currentModule = 0;
  currentColor = 'rgb(41, 47, 51, 1)';
  circuits = [];
  @ViewChildren('TR') tr: ElementRef<HTMLElement>;
  listCircuits = [];
  pseudo = '';
  isLoading$ = new BehaviorSubject(false);
  error$ = new BehaviorSubject(false);
  canDisplay$ = new BehaviorSubject(false);
  click = 'hidden';
  @ViewChild('Stats', {static: false}) mobileStats: ElementRef<HTMLDivElement>;
  @ViewChild('Hola', {static: false}) hola: ElementRef<HTMLDivElement>;

  isMobile = false;

  display = false;
  ngOnInit(): void {
    if (window.innerWidth < 1024) {
      this.isMobile = true;
    }
    this.CallBack();
  }
  back() {
    this.location.back();
  }
  letter = "";
  onClick(id) {
    if (this.currentModule !== id) {
      this.modules[this.currentModule].opacity = 0.5;
      this.currentModule = id;
      this.currentColor = this.modules[id].color;
      this.modules[id].opacity = 1;
      this.CallBack();
    }
  }

  CallBack() {
    this.infos = [];
    this.circuits = [];
    this.listCircuits = [];
    this.click = 'hidden';
    this.letter = this.getLetter();
    this.isLoading$.next(true);
    const onSuccess = response => {
      response.body.friends.forEach(elt => {
        this.infos.push({
          ladder: elt.ladder,
          pseudo: elt.pseudo,
          average: elt.average + " %",
          isSelected: false,
          img: `../assets/images/CircuitsIcon/arrow_${this.letter}.png`
        });
        this.currentIndex = -1;
        this.circuits.push(elt.circuits);
      });
      setTimeout(() => {
        this.canDisplay$.next(true);
        this.isLoading$.next(false);
      }, 1000);
      clearTimeout();
    };

    const onFailed = err => {
      this.isLoading$.next(false);
      this.error$.next(true);
    };
    this.statService.getRanking(this.modules[this.currentModule].db).then(onSuccess).catch(onFailed);
  }

  getLetter() {
    if (this.modules[this.currentModule].db === 'GENERAL') {
      return 'T';
    } else {
     return this.modules[this.currentModule].db.substring(0, 1);
    }
  }

  // getImg(selected?: boolean) {
  //   if(selected) {
  //     return "../assets/images/CircuitsIcon/arrow.png";
  //   }
  //   let letter = '';
  //   if (this.modules[this.currentModule].db === 'GENERAL') {
  //     letter = 'T';
  //   } else {
  //     letter = this.modules[this.currentModule].db.substring(0, 1);
  //   }
  //   return '../assets/images/CircuitsIcon/arrow_' + letter + '.png';
  // }

  close() {
    this.click = 'hidden';
  }

  displayStats(i, elt) {
    this.pseudo = elt.pseudo;
    this.listCircuits = this.circuits[i];
    this.changeSelection(i);
    if (this.isMobile) {
      this.mobileStats.nativeElement.classList.add('is-active');
    } else {
      this.click = 'visible';
    }
  }

  currentIndex = -1;
  thirdCase = false;
  index = -1;
  changeSelection(i) {
    if (this.currentIndex === -1) {
      if (this.thirdCase) {
        this.infos[this.index].isSelected = false;
        this.infos[this.index].img = `../assets/images/CircuitsIcon/arrow_${this.letter}.png`;
        this.thirdCase = false;
      }
      this.currentIndex = i;
      this.infos[i].isSelected = true;
      this.infos[i].img = `../assets/images/CircuitsIcon/arrow.png`;
    } else {
      if(this.currentIndex !== i) {
        this.infos[this.currentIndex].isSelected = false;
        this.infos[this.currentIndex].img = `../assets/images/CircuitsIcon/arrow_${this.letter}.png`;
        this.infos[i].img = `../assets/images/CircuitsIcon/arrow.png`;
        this.infos[i].isSelected = true;
        this.currentIndex = i;
      } else {
        this.currentIndex = -1;
        this.thirdCase = true;
        this.index = i;
        this.infos[i].img = `../assets/images/CircuitsIcon/arrow.png`;
        this.infos[i].isSelected = true;
      }
    }
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >= 1024 && this.isMobile) {
      if (this.mobileStats.nativeElement.classList.contains('is-active')) {
        this.mobileStats.nativeElement.classList.remove('is-active');
        this.click = 'visible';
      }
      this.isMobile = false;
    } else if (window.innerWidth < 1024 && !this.isMobile) {
      if (this.click === 'visible') {
        this.mobileStats.nativeElement.classList.add('is-active');
        this.click = 'hidden';
      }
      this.isMobile = true;
    }
  }

  Cancel() {
    this.mobileStats.nativeElement.classList.remove('is-active');
  }
  getImgColor() {
    // let letter = '';
    // if (this.modules[this.currentModule].db === 'GENERAL') {
    //   letter = 'T';
    // } else {
    //   letter = this.modules[this.currentModule].db.substring(0, 1);
    // }
    return '../assets/images/score_time/time_' + this.letter + '.png';
  }
  getImgScore() {
    // let letter = '';
    // if (this.modules[this.currentModule].db === 'GENERAL') {
    //   letter = 'T';
    // } else {
    //   letter = this.modules[this.currentModule].db.substring(0, 1);
    // }
    return '../assets/images/score_time/medaille_' + this.letter + '.png';
  }
  test() {
    this.display = !this.display;
    if (this.display) {
      this.hola.nativeElement.classList.add('is-active');
    } else {
      this.hola.nativeElement.classList.remove('is-active');
    }
  }

  getPath(name) {
    return `../assets/images/matières/${name}`;
  }

  SecondsToTime(given_seconds: number) {
    const dateObj = new Date(given_seconds * 1000);
    const hours = dateObj.getUTCHours();
    const minutes = dateObj.getUTCMinutes();
    const seconds = dateObj.getSeconds();
    let time = '';

    if (hours === 0 && minutes === 0 && seconds !== 0) {
      time = seconds.toString().padStart(2, '0') + ' s ';
    } else if (hours === 0 && minutes === 0 && seconds === 0) {
      time = '--';
    } else if (hours === 0 && minutes !== 0 && seconds === 0) {
      time = minutes.toString().padStart(2, '0') + ' min ';
    } else if (hours === 0 && minutes !== 0 && seconds !== 0) {
      time = minutes.toString().padStart(2, '0') + ' min ' + seconds.toString().padStart(2, '0') + ' s ';
    } else if (hours !== 0 && minutes === 0 && seconds === 0) {
      time = hours.toString().padStart(2, '0') + ' h ';
    } else if (hours !== 0 && minutes !== 0 && seconds === 0) {
      time = hours.toString().padStart(2, '0') + ' h ' + minutes.toString().padStart(2, '0') + ' min ';
    } else {
      time = hours.toString().padStart(2, '0') + ' h ' +
        minutes.toString().padStart(2, '0') + ' min ' +
        seconds.toString().padStart(2, '0') + ' s ';
    }
    return time;
  }
}
